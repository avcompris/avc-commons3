package net.avcompris.commons3.triggered.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import javax.annotation.Nullable;

import net.avcompris.commons3.triggered.ErrorTriggered;
import net.avcompris.commons3.triggered.ErrorTriggeredAction;

public abstract class AbstractErrorTriggeredImpl implements ErrorTriggered {

	private ErrorTriggeredAction action;

	@Override
	public final void register(final ErrorTriggeredAction action) {

		checkNotNull(action, "action");

		checkState(this.action == null, "Action already registered");

		this.action = action;
	}

	@Nullable
	protected final ErrorTriggeredAction getAction() {

		return action;
	}
}
