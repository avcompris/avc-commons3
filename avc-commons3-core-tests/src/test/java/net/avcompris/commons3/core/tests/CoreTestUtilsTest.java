package net.avcompris.commons3.core.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolder;
import static net.avcompris.commons3.core.tests.CoreTestUtils.assertAreSorted;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.api.Entities;
import net.avcompris.commons3.api.Entity;
import net.avcompris.commons3.types.DateTimeHolder;

public class CoreTestUtilsTest {

	private class MyCustomer implements Entity {

		private final String name;
		private final int age;
		private DateTimeHolder createdAt;
		private DateTimeHolder updatedAt;

		MyCustomer(final String name, final int age) {

			this.name = checkNotNull(name, "name");
			this.age = age;

			createdAt = toDateTimeHolder(new DateTime());
			updatedAt = createdAt;
		}

		@SuppressWarnings("unused")
		public String getName() {

			return name;
		}

		@SuppressWarnings("unused")
		public int getAge() {

			return age;
		}

		@Override
		public int getRevision() {

			return 1;
		}

		@Override
		public DateTimeHolder getCreatedAt() {

			return createdAt;
		}

		@Override
		public DateTimeHolder getUpdatedAt() {

			return updatedAt;
		}
	}

	private interface MyCustomers extends Entities<MyCustomer> {

		MyCustomers setStart(int start);

		MyCustomers setLimit(int limit);

		MyCustomers setSize(int size);

		MyCustomers setTotal(int total);

		MyCustomers setTookMs(int tookMs);

		MyCustomers addToResults(MyCustomer customer);
	}

	@Test
	public void test_assertEmptyAreSorted() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(0) //
				.setTotal(0) //
				.setTookMs(28);

		assertAreSorted("name", customers);
		assertAreSorted("name ASC", customers);
		assertAreSorted("name DESC", customers);

		assertAreSorted("age", customers);
		assertAreSorted("age ASC", customers);
		assertAreSorted("age DESC", customers);
	}

	@Test
	public void test_invalidDataBean_crash() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0);

		assertThrows(IllegalStateException.class, ()

		-> assertAreSorted("name", customers));
	}

	@Test
	public void test_assertSortedAreSorted() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(4) //
				.setTotal(4) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 16)) //
				.addToResults(new MyCustomer("ursula", 18)) //
				.addToResults(new MyCustomer("vincent", 40)) //
				.addToResults(new MyCustomer("xavier", 42));

		assertAreSorted("name", customers);
		assertAreSorted("name ASC", customers);

		assertAreSorted("age", customers);
		assertAreSorted("age ASC", customers);
	}

	@Test
	public void test_assertSingleIsSorted() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(1) //
				.setTotal(1) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 16));

		assertAreSorted("name", customers);
		assertAreSorted("name ASC", customers);
		assertAreSorted("name DESC", customers);

		assertAreSorted("age", customers);
		assertAreSorted("age ASC", customers);
		assertAreSorted("age DESC", customers);
	}

	@Test
	public void test_assertSortedAreNotSortedDESC_name() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(4) //
				.setTotal(4) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 16)) //
				.addToResults(new MyCustomer("ursula", 18)) //
				.addToResults(new MyCustomer("vincent", 40)) //
				.addToResults(new MyCustomer("xavier", 42));

		assertThrows(AssertionError.class, ()

		-> assertAreSorted("name DESC", customers));
	}

	@Test
	public void test_assertSortedAreNotSortedDESC_age() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(4) //
				.setTotal(4) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 16)) //
				.addToResults(new MyCustomer("ursula", 18)) //
				.addToResults(new MyCustomer("vincent", 40)) //
				.addToResults(new MyCustomer("xavier", 42));

		assertThrows(AssertionError.class, ()

		-> assertAreSorted("age DESC", customers));
	}

	@Test
	public void test_assertSortedResultsDESCAreSortedDESC() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(4) //
				.setTotal(4) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 46)) //
				.addToResults(new MyCustomer("christophe", 38)) //
				.addToResults(new MyCustomer("bernard", 20)) //
				.addToResults(new MyCustomer("anatole", 12));

		assertAreSorted("name DESC", customers);

		assertAreSorted("age DESC", customers);
	}

	@Test
	public void test_assertSortedResultsDESCAreSortedASC_name() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(4) //
				.setTotal(4) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 46)) //
				.addToResults(new MyCustomer("christophe", 38)) //
				.addToResults(new MyCustomer("bernard", 20)) //
				.addToResults(new MyCustomer("anatole", 12));

		assertThrows(AssertionError.class, ()

		-> assertAreSorted("name", customers));
	}

	@Test
	public void test_assertSortedResultsDESCAreSortedASC_age() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(4) //
				.setTotal(4) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 46)) //
				.addToResults(new MyCustomer("christophe", 38)) //
				.addToResults(new MyCustomer("bernard", 20)) //
				.addToResults(new MyCustomer("anatole", 12));

		assertThrows(AssertionError.class, ()

		-> assertAreSorted("age", customers));
	}

	@Test
	public void test_assertUnsortedAreNotSorted_name() throws Exception {

		final MyCustomers customers = instantiate(MyCustomers.class) //
				.setStart(0) //
				.setLimit(10) //
				.setSize(4) //
				.setTotal(4) //
				.setTookMs(28) //
				.addToResults(new MyCustomer("toto", 12)) //
				.addToResults(new MyCustomer("christophe", 40));

		assertThrows(AssertionError.class, ()

		-> assertAreSorted("name", customers));
	}
}
