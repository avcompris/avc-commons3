package net.avcompris.commons3.core.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static net.avcompris.commons3.databeans.DataBeans.validate;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.uncapitalize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.tests.AbstractApiTest;
import net.avcompris.commons3.api.tests.ControllerContext;
import net.avcompris.commons3.api.tests.ControllerContextUtils;
import net.avcompris.commons3.api.tests.TestsSpec.Data;
import net.avcompris.commons3.api.tests.TestsSpec.HttpMethod;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.ClockImpl;
import net.avcompris.commons3.web.AbstractController;

public abstract class AbstractServiceApiTest<T> extends AbstractApiTest {

	private final ClassToInstanceMap<Object> implementations = MutableClassToInstanceMap.create();

	private final List<Class<? extends AbstractController>> controllerClasses = new ArrayList<>();

	protected AbstractServiceApiTest(final TestSpec spec, //
			@Nullable final String superadminAuthorization, //
			@SuppressWarnings("unchecked") final Class<? extends AbstractController>... controllerClasses) {

		super(spec, superadminAuthorization);

		for (final Class<? extends AbstractController> controllerClass : controllerClasses) {

			this.controllerClasses.add(controllerClass);
		}
	}

	protected abstract T getBeans(Clock clock) throws Exception;

	protected final <U> void inject(final Class<U> serviceClass, final U serviceImpl) {

		checkNotNull(serviceClass, "serviceClass");
		checkNotNull(serviceImpl, "serviceImpl");

		implementations.put(serviceClass, serviceImpl);
	}

	@Override
	protected final StepExecutionResult execute(final int stepIndex, final StepExecution step) throws Exception {

		@Nullable
		final ControllerContext context = extractControllerContext(step.getHttpMethod(), step.getPath());

		int statusCode = -1;

		Object result = null;

		if (context == null) {

			statusCode = 404;

			result = null;

		} else {

			final Class<? extends AbstractController> controllerClass = context.controllerClass;
			final Method controllerMethod = context.controllerMethod;

			final Constructor<?> constructor = extractConstructor(controllerClass);

			final Class<?>[] constructorParameterTypes = constructor.getParameterTypes();

			final Object[] constructorArgs = new Object[constructorParameterTypes.length];

			for (int i = 0; i < constructorArgs.length; ++i) {

				constructorArgs[i] = instantiate(context, step, constructorParameterTypes[i]);
			}

			final Object controller = constructor.newInstance(constructorArgs);

			final Class<?>[] parameterTypes = controllerMethod.getParameterTypes();

			final Object[] args = new Object[parameterTypes.length];

			try {

				for (int i = 0; i < args.length; ++i) {

					args[i] = instantiate(context, step, parameterTypes[i],
							controllerMethod.getParameterAnnotations()[i]);
				}

			} catch (final MalformedDataBeanException e) {

				e.printStackTrace();

				statusCode = e.getHttpErrorCode();

				result = null;
			}

			try {

				if (statusCode == -1) {

					result = controllerMethod.invoke(controller, args);

					statusCode = 200;
				}

			} catch (final InvocationTargetException e) {

				if (e.getTargetException() instanceof ServiceException) {

					final ServiceException serviceException = (ServiceException) e.getTargetException();

					statusCode = serviceException.getHttpErrorCode();

				} else {

					throw e;
				}
			}

			if (result != null && result instanceof ResponseEntity) {

				final ResponseEntity<?> responseEntity = (ResponseEntity<?>) result;

				statusCode = responseEntity.getStatusCodeValue();

				result = responseEntity.getBody();
			}
		}

		if (result != null) {

			try {

				validate(result);

			} catch (final IllegalStateException e) {

				throw e;

			} catch (final IllegalArgumentException e) {

				// swallow the typing exception; do nothing
			}
		}

		return new StepExecutionResult(statusCode, result);
	}

	@Nullable
	private ControllerContext extractControllerContext(final HttpMethod httpMethod, final String path) {

		// 1. Exact matching

		for (final Class<? extends AbstractController> controllerClass : controllerClasses) {

			for (final Method method : controllerClass.getMethods()) {

				final RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);

				if (requestMapping == null) {
					continue;
				} else if (!httpMethod.name().contentEquals(requestMapping.method()[0].name())) {
					continue;
				}

				final ControllerContext context = ControllerContextUtils //
						.extractControllerContext(httpMethod, path, controllerClass);

				if (context != null) {

					return context;
				}
			}
		}

		return null;
	}

	private static Constructor<?> extractConstructor(final Class<? extends AbstractController> controllerClass) {

		for (final Constructor<?> constructor : controllerClass.getConstructors()) {

			return constructor;
		}

		throw new IllegalStateException();
	}

	@Nullable
	private static <T> T extractAnnotation(final Annotation[] annotations, final Class<T> annotationClass) {

		for (final Annotation annotation : annotations) {

			if (annotationClass.isInstance(annotation)) {

				return annotationClass.cast(annotation);
			}
		}

		return null;
	}

	private Object instantiate(final ControllerContext context, final StepExecution step, final Class<?> type,
			final Annotation... annotations) throws MalformedDataBeanException {

		checkNotNull(context, "context");
		checkNotNull(type, "type");

		@Nullable
		final PathVariable pathVariable = extractAnnotation(annotations, PathVariable.class);

		@Nullable
		final RequestParam requestParam = extractAnnotation(annotations, RequestParam.class);

		@Nullable
		final RequestBody requestBody = extractAnnotation(annotations, RequestBody.class);

		if (implementations.containsKey(type)) {

			return implementations.get(type);

		} else if (CorrelationService.class.equals(type)) {

			return new CorrelationService() {

				@Override
				public String getCorrelationId(@Nullable final String correlationIdParam,
						@Nullable final String correlationIdHeader) throws ServiceException {

					return randomAlphanumeric(20);
				}

				@Override
				public void purgeOlderThanSec(final String correlationId, final User user, final int seconds)
						throws ServiceException {

					throw new NotImplementedException("");
				}
			};

		} else if (SessionPropagator.class.equals(type)) {

			return new SessionPropagator();

		} else if (Clock.class.equals(type)) {

			return new ClockImpl();

		} else if (String.class.equals(type)) {

			if (pathVariable != null) {

				return context.getVariable(pathVariable.name());

			} else if (requestParam != null) {

				if (requestParam.required() || context.hasVariable(requestParam.name())) {

					return context.getVariable(requestParam.name());
				}
			}

			return null;

		} else if (Integer.class.equals(type)) {

			return null;

		} else if (HttpServletRequest.class.equals(type)) {

			final HttpServletRequest request = mock(HttpServletRequest.class);

			if (spec.getAuthentication() != null) {

				when(request.getHeader("Authorization")).thenReturn(superadminAuthorization);
			}

			return request;

		} else if (HttpServletResponse.class.equals(type)) {

			final HttpServletResponse response = mock(HttpServletResponse.class);

			return response;

		} else if (requestBody != null) {

			final Object dataBean = mock(type);

			final Set<String> nonNullableGetterNames = newHashSet();

			for (final Method method : type.getMethods()) {

				if (method.getParameterCount() != 0 //
						|| method.getReturnType() == null //
						|| method.getAnnotation(Nullable.class) != null) {
					continue;
				}

				final String methodName = method.getName();

				if ("getClass".contentEquals(methodName) //
						|| "hashCode".contentEquals(methodName) //
						|| "toString".contentEquals(methodName)) {
					continue;
				}

				nonNullableGetterNames.add(methodName);
			}

			for (final Data data : step.getData()) {

				final Method getter = extractGetter(type, data.getName());

				nonNullableGetterNames.remove(getter.getName());

				final Class<?> propertyType = getter.getReturnType();

				final Class<?> wrapperType = propertyType.isPrimitive() //
						? ClassUtils.primitiveToWrapper(propertyType) //
						: propertyType;

				when(invoke(getter, dataBean)).thenReturn(cast(wrapperType, data.getValue()));
			}

			for (final String getterName : newHashSet(nonNullableGetterNames)) {

				final Method getter = extractGetter(type, extractPropertyName(getterName));

				final Class<?> propertyType = getter.getReturnType();

				if (propertyType.isArray()) {

					nonNullableGetterNames.remove(getter.getName());

					when(invoke(getter, dataBean)).thenReturn(Array.newInstance(propertyType.getComponentType(), 0));
				}
			}

			if (!nonNullableGetterNames.isEmpty()) {
				throw new MalformedDataBeanException(
						"There are non-nullable getters that were not invoked: " + join(nonNullableGetterNames, ", "));
			}

			return dataBean;

		} else if (ApplicationContext.class.equals(type)) {

			return mock(type);

		} else if (type.isInterface()) { // e.g. UsersDao

			return mock(type);

		} else {

			throw new NotImplementedException("type: " + type.getName());
		}
	}

	private static String extractPropertyName(final String methodName) {

		checkNotNull(methodName, "methodName");

		if (methodName.startsWith("get")) {

			return uncapitalize(methodName.substring(3));

		} else if (methodName.startsWith("is")) {

			return uncapitalize(methodName.substring(2));

		} else {

			throw new NotImplementedException("methodName: " + methodName);
		}

	}

	private static <T> T cast(final Class<T> propertyType, final Object propertyValue) {

		if (String.class.equals(propertyType)) {

			return propertyType.cast(propertyValue);

		} else if (Integer.class.equals(propertyType)) {

			return propertyType.cast(Integer.valueOf(propertyValue.toString()));

		} else if (Long.class.equals(propertyType)) {

			return propertyType.cast(Long.valueOf(propertyValue.toString()));

		} else if (Boolean.class.equals(propertyType)) {

			return propertyType.cast(Boolean.valueOf(propertyValue.toString()));

		} else if (propertyType.isEnum()) {

			for (final T enumConstant : propertyType.getEnumConstants()) {

				if (((Enum<?>) enumConstant).name().contentEquals(propertyValue.toString())) {

					return enumConstant;
				}
			}

			throw new IllegalArgumentException(
					"propertyValue: " + propertyValue + ", for enum class: " + propertyType.getName());

		} else if (String[].class.equals(propertyType)) {

			return propertyType.cast(propertyValue);

		} else {

			throw new NotImplementedException("propertyType: " + propertyType);
		}
	}
}
