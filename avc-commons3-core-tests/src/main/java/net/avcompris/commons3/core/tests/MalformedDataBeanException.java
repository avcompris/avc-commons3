package net.avcompris.commons3.core.tests;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.exception.ServiceException;

public class MalformedDataBeanException extends ServiceException {

	private static final long serialVersionUID = -2963961609449468220L;

	public MalformedDataBeanException(@Nullable final String message) {

		super(400, message);
	}
}
