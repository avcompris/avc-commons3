package net.avcompris.commons3.core.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Locale.ENGLISH;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.StringUtils.normalizeSpace;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.apache.commons.lang3.StringUtils.uncapitalize;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons3.api.Entities;
import net.avcompris.commons3.api.EntitiesQuery.EnumSortBy;
import net.avcompris.commons3.api.Entity;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthorizedException;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.databeans.DataBeans;
import net.avcompris.commons3.types.DateTimeHolder;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.ClockImpl;
import net.avcompris.commons3.utils.LogFactory;

public abstract class CoreTestUtils {

	private static final Set<String> randoms = newHashSet();

	public static String newCorrelationId() {

		final String correlationId = "C-" //
				+ System.currentTimeMillis() + "-" //
				+ randomAlphanumeric(20);

		LogFactory.setCorrelationId(correlationId);

		return correlationId;
	}

	public static Permissions grantAll() {

		return new Permissions() {

			@Override
			public void assertAuthorized(final String correlationId, final User user,
					final Object... contextNameValuePairs) throws UnauthorizedException {

				return; // do nothing
			}
		};
	}

	public static Clock defaultClock() {

		return new ClockImpl();
	}

	private static String randomNoDuplicate(final Callable<String> c) {

		for (final long startMs = System.currentTimeMillis(); System.currentTimeMillis() < startMs + 1000;) {

			final String s;

			try {

				s = c.call();

			} catch (final RuntimeException e) {

				throw e;

			} catch (final Exception e) {

				throw new RuntimeException(e);
			}

			synchronized (randoms) {

				if (!randoms.contains(s)) {

					randoms.add(s);

					return s;
				}
			}
		}

		throw new IllegalStateException("Timeout after 1000 ms.");
	}

	public static String random8() {

		return randomNoDuplicate(() -> randomAlphanumeric(8));
	}

	public static String random20() {

		return randomNoDuplicate(() -> randomAlphanumeric(20));
	}

	public static String random20(@Nullable final String prefix) {

		return randomNoDuplicate(() -> random(20, prefix));
	}

	public static String random32(@Nullable final String prefix) {

		return randomNoDuplicate(() -> random(32, prefix));
	}

	public static String random40(@Nullable final String prefix) {

		return randomNoDuplicate(() -> random(40, prefix));
	}

	public static String randomLowercase40(@Nullable final String prefix) {

		return randomNoDuplicate(() -> randomLowercase(40, prefix));
	}

	public static String randomEmail20() {

		return randomNoDuplicate(()

		-> randomAlphanumeric(8) + "@" + randomAlphanumeric(7) + ".avcompris.com");
	}

	private static String random(final int length, @Nullable final String prefix) {

		return (prefix //
				+ System.currentTimeMillis() + "-" //
				+ randomAlphanumeric(length) //
		).substring(0, length);
	}

	private static String randomLowercase(final int length, @Nullable final String prefix) {

		return random(length, prefix).toLowerCase(ENGLISH);
	}

	public static <T extends Entity> void assertAreSorted(final EnumSortBy sortBy, final Entities<T> entities) {

		checkNotNull(entities, "entities");
		checkNotNull(sortBy, "sortBy");

		DataBeans.validate(entities);

		final StringBuilder sb = new StringBuilder();

		final String name = sortBy.name();

		final boolean isDesc = name.endsWith("_DESC");

		for (int i = 8; i < (isDesc ? name.length() - 5 : name.length()); ++i) {

			final char c = name.charAt(i);

			if (c == '_') {

				++i;

				sb.append(Character.toUpperCase(name.charAt(i)));

			} else {

				sb.append(Character.toLowerCase(c));
			}
		}

		if (isDesc) {
			sb.append(" DESC");
		}

		assertAreSorted(sb.toString(), entities.getResults());
	}

	public static <T extends Entity> void assertAreSorted(final String sortBy, final Entities<T> entities) {

		checkNotNull(entities, "entities");
		checkNotNull(sortBy, "sortBy");

		DataBeans.validate(entities);

		assertAreSorted(sortBy, entities.getResults());
	}

	@SafeVarargs
	public static <T extends Entity> void assertAreSorted(final String sortBy, final T... results) {

		checkNotNull(results, "results");
		checkNotNull(sortBy, "sortBy");

		if (results.length == 0 || results.length == 1) {
			return;
		}

		final boolean isDesc;
		final String propertyName;

		final String trim = normalizeSpace(sortBy);

		if (trim.endsWith(" DESC")) {

			isDesc = true;
			propertyName = substringBefore(trim, " ");

		} else if (trim.endsWith(" ASC")) {

			isDesc = false;
			propertyName = substringBefore(trim, " ");

		} else {

			isDesc = false;
			propertyName = trim;
		}

		final FieldExtractor extractor = extractFieldExtractor(results.getClass().getComponentType(), propertyName);

		final Comparator<Object> comparator = getComparator(extractor.getReturnType(), isDesc);

		Object current = extractor.invoke(results[0]);

		for (int i = 1; i < results.length; ++i) {

			final Object next = extractor.invoke(results[i]);

			if (!isDesc) {

				if (next == null) {

					current = null;

					continue;

				} else if (current == null) {

					fail("Values should be sorted: [" + (i - 1) + "]." + propertyName + ": " + current //
							+ ", [" + i + "]." + propertyName + ": " + next);

				}
			}

			if (isDesc) {

				if (current == null) {

					current = next;

					continue;

				} else if (next == null) {

					fail("Values should be sorted: [" + (i - 1) + "]." + propertyName + ": " + current //
							+ ", [" + i + "]." + propertyName + ": " + next);

				}
			}

			if (comparator.compare(next, current) < 0) {

				fail("Values should be sorted: [" + (i - 1) + "]." + propertyName + ": " + current //
						+ ", [" + i + "]." + propertyName + ": " + next);
			}

			current = next;
		}
	}

	private static Comparator<Object> getComparator(final Class<?> type, final boolean isDesc) {

		checkNotNull(type, "type");

		final int sign = isDesc ? -1 : 1;

		if (String.class.equals(type)) {

			return new Comparator<Object>() {

				@Override
				public int compare(final Object o1, final Object o2) {

					return sign * ((String) o1).compareToIgnoreCase((String) o2);

					// return sign * ((String) o1).compareTo((String) o2);
				}
			};

		} else if (int.class.equals(type)) {

			return new Comparator<Object>() {

				@Override
				public int compare(final Object o1, final Object o2) {

					return sign * ((Integer) o1).compareTo((Integer) o2);
				}
			};

		} else if (boolean.class.equals(type)) {

			return new Comparator<Object>() {

				@Override
				public int compare(final Object o1, final Object o2) {

					return sign * ((Boolean) o1).compareTo((Boolean) o2);
				}
			};

		} else if (DateTimeHolder.class.equals(type)) {

			return new Comparator<Object>() {

				@Override
				public int compare(final Object o1, final Object o2) {

					return sign * ((DateTimeHolder) o1).compareTo((DateTimeHolder) o2);
				}
			};

		} else if (type.isEnum()) {

			return new Comparator<Object>() {

				@Override
				public int compare(final Object o1, final Object o2) {

					return sign * o1.toString().compareTo(o2.toString());
				}
			};

		} else {

			throw new NotImplementedException("type: " + type.getName());
		}
	}

	private interface FieldExtractor {

		Class<?> getReturnType();

		@Nullable
		Object invoke(Object object);
	}

	private static final class FieldExtractorFromGetter implements FieldExtractor {

		private final Method getter;

		@Nullable
		private final FieldExtractor next;

		public FieldExtractorFromGetter(final Method getter) {

			this.getter = checkNotNull(getter, "getter");
			this.next = null;
		}

		public FieldExtractorFromGetter(final Method getter, final FieldExtractor next) {

			this.getter = checkNotNull(getter, "getter");
			this.next = checkNotNull(next, "next");
		}

		@Override
		public Class<?> getReturnType() {

			return next == null

					? getter.getReturnType()

					: next.getReturnType();
		}

		@Override
		public Object invoke(final Object object) {

			checkNotNull(object, "object");

			final Object value;

			try {

				value = getter.invoke(object);

			} catch (final IllegalAccessException e) {

				throw new RuntimeException(e);

			} catch (final InvocationTargetException e) {

				throw new RuntimeException(e.getTargetException());
			}

			if (value == null) {

				return null;
			}

			return next == null

					? value

					: next.invoke(value);
		}
	}

	@Nullable
	private static Method extractGetter(final Class<?> clazz, final String propertyName) {

		for (final Method method : clazz.getMethods()) {

			if (method.getParameterTypes().length != 0) {
				continue;
			}

			final String methodName = method.getName();

			final String suffix;

			if (methodName.startsWith("get")) {

				suffix = methodName.substring(3);

			} else if (methodName.startsWith("is")) {

				suffix = methodName.substring(2);

			} else {

				continue;
			}

			if (uncapitalize(suffix).contentEquals(propertyName)) {

				return method;
			}
		}

		return null;
	}

	private static FieldExtractor extractFieldExtractor(final Class<?> clazz, final String propertyName) {

		checkNotNull(clazz, "clazz");
		checkNotNull(propertyName, "propertyName");

		final Method getter = extractGetter(clazz, propertyName);

		if (getter != null) {

			return new FieldExtractorFromGetter(getter);
		}

		final String subName = extractSubName(propertyName);

		if (subName != null) {

			final Method subGetter = extractGetter(clazz, subName);

			if (subGetter != null) {

				return new FieldExtractorFromGetter(subGetter, //
						extractFieldExtractor(subGetter.getReturnType(),
								uncapitalize(propertyName.substring(subName.length()))));
			}
		}

		throw new IllegalArgumentException("Cannot find getter for: " + propertyName + ", in: " + clazz.getName());
	}

	private static String extractSubName(final String propertyName) {

		final StringBuilder sb = new StringBuilder();

		for (final char c : propertyName.toCharArray()) {

			if (!Character.isLowerCase(c)) {

				break;
			}

			sb.append(c);
		}

		return sb.toString();
	}

	@FunctionalInterface
	public interface Action<T> {

		T action() throws ServiceException;
	}

	@FunctionalInterface
	public interface Predicate {

		boolean test() throws ServiceException;
	}

	public static void retryUntil(final int timeoutMs, final Predicate predicate) throws ServiceException {

		checkNotNull(predicate, "predicate");

		retryUntil(timeoutMs, 1_000, () -> predicate.test() ? "OK" : null);
	}

	public static <T> T retryUntil(final int timeoutMs, final Action<T> action) throws ServiceException {

		return retryUntil(timeoutMs, 1_000, action);
	}

	public static <T> T retryUntil(final int timeoutMs, final int delayMs, final Action<T> action)
			throws ServiceException {

		checkNotNull(action, "action");

		for (final long startMs = System.currentTimeMillis(); System.currentTimeMillis() < startMs + timeoutMs;) {

			final T result = action.action();

			if (result != null) {

				return result;
			}

			try {

				Thread.sleep(delayMs);

			} catch (final InterruptedException e) {

				e.printStackTrace();

				// do nothing
			}
		}

		System.err.println("A timeout occurs (" + timeoutMs + " ms)");

		throw new RuntimeException("Timeout after: " + timeoutMs + " ms");
	}
}
