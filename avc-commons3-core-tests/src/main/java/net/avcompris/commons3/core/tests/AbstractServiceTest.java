package net.avcompris.commons3.core.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.joda.time.DateTimeZone.UTC;

import java.io.IOException;
import java.sql.SQLException;

import org.joda.time.DateTime;

import net.avcompris.commons3.utils.Clock;

public abstract class AbstractServiceTest<T> {

	protected static DateTime now() {

		return new DateTime().withZone(UTC);
	}

	protected abstract T getBeans(Clock clock) throws Exception;

	protected void dumpDbTable(final String dbTableName) throws SQLException, IOException {

		checkNotNull(dbTableName, "dbTableName");

		// do nothing
	}
}
