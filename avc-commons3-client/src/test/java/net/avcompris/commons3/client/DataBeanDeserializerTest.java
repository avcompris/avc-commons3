package net.avcompris.commons3.client;

import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import javax.annotation.Nullable;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import net.avcompris.commons3.types.DateTimeHolder;

public class DataBeanDeserializerTest {

	private interface DummyCustomer {

		int getId();

		String getName();

		@Nullable
		String getAddress();

		DateTimeHolder getStartedAt();
	}

	private interface MutableDummyCustomer extends DummyCustomer {

		MutableDummyCustomer setId(int id);

		MutableDummyCustomer setName(String name);

		MutableDummyCustomer setAddress(@Nullable String address);

		MutableDummyCustomer setStartedAt(DateTimeHolder startedAt);
	}

	@Test
	public void testDeserializeCustomer_noAddress() throws Exception {

		final DummyCustomer customer = deserialize(MutableDummyCustomer.class,

				"{\"id\": 123, " //
						+ "\"startedAt\": {\"dateTime\": \"2018-03-22T19:20:08.483+01:00\"}, " //
						+ "\"name\": \"xyz-123\"}");

		assertEquals(123, customer.getId());
		assertEquals("xyz-123", customer.getName());
		assertEquals(new DateTime(2018, 3, 22, 18, 20, 8, 483, DateTimeZone.UTC).toString(),
				customer.getStartedAt().getDateTime());
		assertEquals(new DateTime(2018, 3, 22, 19, 20, 8, 483, DateTimeZone.forOffsetHours(1)),
				toDateTime(customer.getStartedAt()));
		assertEquals(null, customer.getAddress());

		assertEquals("{address: null, id: 123, name: \"xyz-123\", " //
				+ "startedAt: {dateTime: \"2018-03-22T18:20:08.483Z\", timestamp: 1521742808483}}",
				customer.toString());
		assertTrue(customer.equals(customer));
		assertNotEquals(0, customer.hashCode());
		assertNotNull(customer.getClass());
	}

	@Test
	public void testDeserializeCustomer_nullAddress() throws Exception {

		final DummyCustomer customer = deserialize(MutableDummyCustomer.class,

				"{\"id\": 123, \"address\": null, " //
						+ "\"startedAt\": {\"dateTime\": \"2018-03-22T19:20:08.483+01:00\"}, " //
						+ "\"name\": \"xyz-123\"}");

		assertEquals(123, customer.getId());
		assertEquals("xyz-123", customer.getName());
		assertEquals(new DateTime(2018, 3, 22, 18, 20, 8, 483, DateTimeZone.UTC).toString(),
				customer.getStartedAt().getDateTime());
		assertEquals(new DateTime(2018, 3, 22, 19, 20, 8, 483, DateTimeZone.forOffsetHours(1)),
				toDateTime(customer.getStartedAt()));
		assertEquals(null, customer.getAddress());

		assertEquals("{address: null, id: 123, name: \"xyz-123\", " //
				+ "startedAt: {dateTime: \"2018-03-22T18:20:08.483Z\", timestamp: 1521742808483}}",
				customer.toString());
		assertTrue(customer.equals(customer));
		assertNotEquals(0, customer.hashCode());
		assertNotNull(customer.getClass());
	}

	@Test
	public void testDeserializerError_crash() throws Exception {

		assertThrows(IOException.class, ()

		-> deserialize(DummyError.class,

				"{\n" + //
						"    \"type\": \"UnauthenticatedException\",\n" + //
						"    \"description\": null,\n" + //
						"    \"correlationId\": null,\n" + //
						"    \"statusCode\": 401\n" + //
						"}"));
	}

	@Test
	public void testDeserializerError_OK() throws Exception {

		final DummyError error = deserialize(DummyError.class,

				"{\n" + //
						"    \"type\": \"UnauthenticatedException\",\n" + //
						"    \"description\": null,\n" + //
						"    \"correlationId\": \"xxx\",\n" + //
						"    \"statusCode\": 401\n" + //
						"}");

		assertEquals(401, error.getStatusCode());
		assertEquals("UnauthenticatedException", error.getType());
		assertNull(error.getDescription());
		assertEquals("xxx", error.getCorrelationId());
	}

	private interface DummyError {

		String getCorrelationId();

		int getStatusCode();

		String getType();

		@Nullable
		String getDescription();

		DummyError setCorrelationId(String correlationId);

		DummyError setStatusCode(int statusCode);

		DummyError setType(String type);

		DummyError setDescription(@Nullable String description);
	}

	@Test
	public void testDeserialize_roles() throws Exception {

		final DummyRolesInfo roles = deserialize(DummyRolesInfo.class,

				"{\n" + //
						"  \"roles\" : [ {\n" + //
						"    \"permissions\" : [ \"ANY\", \"GET_APPINFO\", \"CREATE_REGULAR_USER\" ],\n" + //
						"    \"role\" : \"ANONYMOUS\",\n" + //
						"    \"superRoles\" : [ ]\n" + //
						"  }, {\n" + //
						"    \"permissions\" : [ \"GET_USER_ME\", \"UPDATE_USER_ME\", \"CREATE_CALORIES_ENTRY\", \"QUERY_MY_CALORIES_ENTRIES\", \"GET_MY_CALORIES_ENTRY\", \"UPDATE_MY_CALORIES_ENTRY\", \"DELETE_MY_CALORIES_ENTRY\" ],\n"
						+ //
						"    \"role\" : \"REGULAR\",\n" + //
						"    \"superRoles\" : [ \"ANONYMOUS\" ]\n" + //
						"  }, {\n" + //
						"    \"permissions\" : [ \"GET_ROLES\", \"QUERY_USERS\", \"CREATE_FULL_USER\", \"GET_USER\", \"UPDATE_USER\", \"DELETE_USER\", \"UPDATE_USER_ROLE\" ],\n"
						+ //
						"    \"role\" : \"USERMGR\",\n" + //
						"    \"superRoles\" : [ \"REGULAR\" ]\n" + //
						"  }, {\n" + //
						"    \"permissions\" : [ \"QUERY_USERS\", \"GET_USER\", \"QUERY_CALORIES_ENTRIES\", \"GET_CALORIES_ENTRY\" ],\n"
						+ //
						"    \"role\" : \"SUPERVISOR\",\n" + //
						"    \"superRoles\" : [ \"REGULAR\" ]\n" + //
						"  }, {\n" + //
						"    \"permissions\" : [ \"UPDATE_CALORIES_ENTRY\", \"DELETE_CALORIES_ENTRY\" ],\n" + //
						"    \"role\" : \"ADMIN\",\n" + //
						"    \"superRoles\" : [ \"USERMGR\", \"SUPERVISOR\" ]\n" + //
						"  }, {\n" + //
						"    \"permissions\" : [ \"SUPERADMIN\" ],\n" + //
						"    \"role\" : \"SUPERADMIN\",\n" + //
						"    \"superRoles\" : [ ]\n" + //
						"  } ]\n" + //
						"}",

				DummyRoleInfo.class);

		assertEquals(6, roles.getRoles().length);

		assertSame(DummyRole.REGULAR, roles.getRoles()[1].getRole());

		assertEquals(1, roles.getRoles()[1].getSuperRoles().length);

		assertSame(DummyRole.ANONYMOUS, roles.getRoles()[1].getSuperRoles()[0]);

		assertEquals(7, roles.getRoles()[1].getPermissions().length);

		assertSame(DummyPermissionType.GET_USER_ME, roles.getRoles()[1].getPermissions()[0]);
		assertSame(DummyPermissionType.UPDATE_USER_ME, roles.getRoles()[1].getPermissions()[1]);
	}

	private interface DummyRolesInfo {

		DummyRoleInfo[] getRoles();

		DummyRolesInfo addToRoles(DummyRoleInfo role);

		DummyRolesInfo setRoles(DummyRoleInfo[] roles);
	}

	private interface DummyRoleInfo {

		DummyRole getRole();

		DummyRole[] getSuperRoles();

		DummyPermissionType[] getPermissions();

		DummyRoleInfo setRole(DummyRole role);

		DummyRoleInfo addToSuperRoles(DummyRole superRole);

		DummyRoleInfo setSuperRoles(DummyRole[] superRoles);

		DummyRoleInfo addToPermissions(DummyPermissionType permission);

		DummyRoleInfo setPermissions(DummyPermissionType[] permissions);
	}

	private static <T> T deserialize(final Class<T> clazz, final String json, final Class<?>... otherClasses)
			throws IOException {

		final ObjectMapper objectMapper = new ObjectMapper();

		final SimpleModule module = new SimpleModule();

		module.addDeserializer(clazz, new DataBeanDeserializer<T>(clazz));

		objectMapper.registerModule(module);

		final T bean = objectMapper.readValue(json, clazz);

		return bean;
	}

	private static <T, U> T deserialize(final Class<T> clazz, final String json, final Class<U> otherClass)
			throws IOException {

		final ObjectMapper objectMapper = new ObjectMapper();

		final SimpleModule module = new SimpleModule();

		module.addDeserializer(clazz, new DataBeanDeserializer<T>(clazz));

		module.addDeserializer(otherClass, DataBeanDeserializer.createDeserializer(otherClass));

		objectMapper.registerModule(module);

		final T bean = objectMapper.readValue(json, clazz);

		return bean;
	}

	@Test
	public void testDateTime_IllegalArgumentException() {

		assertThrows(IllegalArgumentException.class, ()

		-> DateTime.parse("globally"));
	}

	private interface MyBase {

		@Nullable
		String getSql();

		MyBase setSql(@Nullable String sql);
	}

	private interface MyAdvanced extends MyBase {

		@Override
		MyAdvanced setSql(@Nullable String sql);
	}

	@Test
	public void testDefaultMethodWithNullableParameter() throws Exception {

		final MyBase bean = deserialize(MyAdvanced.class, "{}");

		assertNull(bean.getSql());
	}
}
