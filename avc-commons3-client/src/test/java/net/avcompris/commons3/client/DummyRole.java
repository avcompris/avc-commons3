package net.avcompris.commons3.client;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.client.DummyPermissionType.ANY;
import static net.avcompris.commons3.client.DummyPermissionType.CREATE_CALORIES_ENTRY;
import static net.avcompris.commons3.client.DummyPermissionType.CREATE_FULL_USER;
import static net.avcompris.commons3.client.DummyPermissionType.CREATE_REGULAR_USER;
import static net.avcompris.commons3.client.DummyPermissionType.DELETE_CALORIES_ENTRY;
import static net.avcompris.commons3.client.DummyPermissionType.DELETE_MY_CALORIES_ENTRY;
import static net.avcompris.commons3.client.DummyPermissionType.DELETE_USER;
import static net.avcompris.commons3.client.DummyPermissionType.GET_APPINFO;
import static net.avcompris.commons3.client.DummyPermissionType.GET_CALORIES_ENTRY;
import static net.avcompris.commons3.client.DummyPermissionType.GET_MY_CALORIES_ENTRY;
import static net.avcompris.commons3.client.DummyPermissionType.GET_ROLES;
import static net.avcompris.commons3.client.DummyPermissionType.GET_USER;
import static net.avcompris.commons3.client.DummyPermissionType.GET_USER_ME;
import static net.avcompris.commons3.client.DummyPermissionType.QUERY_CALORIES_ENTRIES;
import static net.avcompris.commons3.client.DummyPermissionType.QUERY_MY_CALORIES_ENTRIES;
import static net.avcompris.commons3.client.DummyPermissionType.QUERY_USERS;
import static net.avcompris.commons3.client.DummyPermissionType.UPDATE_CALORIES_ENTRY;
import static net.avcompris.commons3.client.DummyPermissionType.UPDATE_MY_CALORIES_ENTRY;
import static net.avcompris.commons3.client.DummyPermissionType.UPDATE_USER;
import static net.avcompris.commons3.client.DummyPermissionType.UPDATE_USER_ME;
import static net.avcompris.commons3.client.DummyPermissionType.UPDATE_USER_ROLE;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;

enum DummyRole {

	ANONYMOUS(null,

			ANY, //
			GET_APPINFO, //
			CREATE_REGULAR_USER //
	),

	REGULAR(new DummyRole[] { ANONYMOUS },

			GET_USER_ME, UPDATE_USER_ME, //
			CREATE_CALORIES_ENTRY, //
			QUERY_MY_CALORIES_ENTRIES, //
			GET_MY_CALORIES_ENTRY, UPDATE_MY_CALORIES_ENTRY, DELETE_MY_CALORIES_ENTRY //
	),

	USERMGR(new DummyRole[] { REGULAR },

			GET_ROLES, //
			QUERY_USERS, //
			CREATE_FULL_USER, GET_USER, UPDATE_USER, DELETE_USER, //
			UPDATE_USER_ROLE //
	),

	SUPERVISOR(new DummyRole[] { REGULAR },

			QUERY_USERS, //
			GET_USER, //
			QUERY_CALORIES_ENTRIES, //
			GET_CALORIES_ENTRY),

	ADMIN(new DummyRole[] { USERMGR, SUPERVISOR },

			UPDATE_CALORIES_ENTRY, DELETE_CALORIES_ENTRY //
	),

	SUPERADMIN(null,

			DummyPermissionType.SUPERADMIN //
	);

	private DummyRole(@Nullable final DummyRole[] superRoles, final DummyPermissionType... permissions) {

		this.superRoles = (superRoles != null) ? superRoles : new DummyRole[0];
		this.permissions = checkNotNull(permissions, "permissions");
	}

	private final DummyRole[] superRoles;
	private final DummyPermissionType[] permissions;

	public String getRolename() {

		return name();
	}

	public DummyRole[] getSuperRoles() {

		return ArrayUtils.clone(superRoles);
	}

	public DummyPermissionType[] getPermissions() {

		return ArrayUtils.clone(permissions);
	}
}