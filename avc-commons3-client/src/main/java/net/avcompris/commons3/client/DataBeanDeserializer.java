package net.avcompris.commons3.client;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolder;
import static org.apache.commons.lang3.StringUtils.uncapitalize;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.NullNode;

import net.avcompris.commons3.databeans.DataBeans;
import net.avcompris.commons3.types.DateTimeHolder;

public final class DataBeanDeserializer<T> extends StdDeserializer<T> {

	private static final long serialVersionUID = 8617288345903422915L;

	private final Class<T> dataBeanClass;

	public DataBeanDeserializer(final Class<T> dataBeanClass) {

		super(checkNotNull(dataBeanClass, "dataBeanClass"));

		checkArgument(dataBeanClass.isInterface(), //
				"dataBeanClass should be an interface, but was: %s", dataBeanClass.getName());

		this.dataBeanClass = dataBeanClass;
	}

	public static <U> DataBeanDeserializer<U> createDeserializer(final Class<U> clazz) {

		return new DataBeanDeserializer<U>(clazz);
	}

	private static boolean hasParameterAnnotation(final Method method, final int paramIndex,
			final Class<? extends Annotation> annotationClass) {

		// Circumvent the "default" method mechanism: Find the outermost corresponding
		// method
		//
		if (method.isDefault()) {

			final Method outermostMethod;

			try {

				outermostMethod = method.getDeclaringClass().getMethod(method.getName(), method.getParameterTypes());

			} catch (final NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (!outermostMethod.isDefault()) {

				return hasParameterAnnotation(outermostMethod, paramIndex, annotationClass);
			}
		}

		for (final Annotation annotation : method.getParameterAnnotations()[paramIndex]) {

			if (annotationClass.isInstance(annotation)) {

				return true;
			}
		}

		return false;
	}

	@Override
	public T deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {

		checkNotNull(parser, "parser");
		checkNotNull(context, "context");

		final T dataBean = DataBeans.instantiate(dataBeanClass);

		final ObjectCodec codec = parser.getCodec();

		final JsonNode node = codec.readTree(parser);

		for (final Method method : dataBeanClass.getMethods()) {

			final String methodName = method.getName();

			if (!methodName.startsWith("set")) {
				continue;
			}

			final Class<?>[] paramTypes = method.getParameterTypes();

			if (paramTypes == null || paramTypes.length != 1) {
				continue;
			}

			final Class<?> paramType = paramTypes[0];

			final boolean isNullable;

			if (hasParameterAnnotation(method, 0, Nullable.class)) {

				isNullable = true;

			} else {

				isNullable = false;
			}

			final String fieldName = uncapitalize(methodName.substring(3)); // Skip "set"

			final JsonNode fieldNode = node.get(fieldName);

			if (fieldNode == null || fieldNode instanceof NullNode) {

				if (isNullable) {
					continue;
				}

				throw new IOException("Non-@Nullable field is missing for DataBean: " + dataBeanClass.getName() + "."
						+ fieldName + ": " + fieldNode);
			}

			final Object value;

			if (Boolean.class.equals(paramType) || boolean.class.equals(paramType)) {

				try {

					value = Boolean.parseBoolean(fieldNode.asText());

				} catch (final NumberFormatException e) {
					throw new IOException("Cannot parse boolean field for DataBean: " + dataBeanClass.getName() + "."
							+ fieldName + ": " + fieldNode);
				}

			} else if (Integer.class.equals(paramType) || int.class.equals(paramType)) {

				try {

					value = Integer.parseInt(fieldNode.asText());

				} catch (final NumberFormatException e) {
					throw new IOException("Cannot parse int field for DataBean: " + dataBeanClass.getName() + "."
							+ fieldName + ": " + fieldNode);
				}

			} else if (Short.class.equals(paramType) || short.class.equals(paramType)) {

				try {

					value = Short.parseShort(fieldNode.asText());

				} catch (final NumberFormatException e) {
					throw new IOException("Cannot parse short field for DataBean: " + dataBeanClass.getName() + "."
							+ fieldName + ": " + fieldNode);
				}

			} else if (Long.class.equals(paramType) || long.class.equals(paramType)) {

				try {

					value = Long.parseLong(fieldNode.asText());

				} catch (final NumberFormatException e) {
					throw new IOException("Cannot parse long field for DataBean: " + dataBeanClass.getName() + "."
							+ fieldName + ": " + fieldNode);
				}

			} else if (String.class.equals(paramType)) {

				value = fieldNode.asText();

			} else if (DateTimeHolder.class.equals(paramType)) {

				final DateTime dateTime;

				try {

					dateTime = DateTime.parse(fieldNode.get("dateTime").asText());

				} catch (final IllegalArgumentException e) {
					throw new IOException("Cannot parse DateTime field for DataBean: " + dataBeanClass.getName() + "."
							+ fieldName + ": " + fieldNode);
				}

				// final long timestamp = fieldNode.has("timestamp") //
				// ? fieldNode.get("timestamp").asLong() //
				// : dateTime.getMillis();

				value = toDateTimeHolder(dateTime);

			} else {

				final JsonParser subParser = fieldNode.traverse();

				final JsonToken valueToken = subParser.nextToken();

				if (valueToken == JsonToken.VALUE_NULL) {

					if (isNullable) {

						value = null;

					} else {

						throw new IOException("Non-@Nullable field is missing for DataBean: " + dataBeanClass.getName()
								+ "." + fieldName + ": " + valueToken);
					}

				} else {

					subParser.setCodec(codec);

					value = context.readValue(subParser, paramType);
				}
			}

			method.setAccessible(true);

			try {

				method.invoke(dataBean, value);

			} catch (final InvocationTargetException e) {

				e.printStackTrace();

				final Throwable targetException = e.getTargetException();

				if (targetException == null) {
					throw new RuntimeException(e);
				} else if (targetException instanceof Error) {
					throw (Error) targetException;
				} else if (targetException instanceof RuntimeException) {
					throw (RuntimeException) targetException;
				} else {
					throw new RuntimeException(targetException);
				}

			} catch (final IllegalAccessException e) {

				e.printStackTrace();

				throw new RuntimeException(e);
			}
		}

		return dataBean;
	}
}
