package net.avcompris.commons3.client.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.COOKIE;

import java.nio.charset.Charset;

import javax.annotation.Nullable;

import org.apache.commons.logging.Log;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.Lists;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnexpectedException;
import net.avcompris.commons3.client.DataBeanDeserializer;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.utils.LogFactory;

public abstract class AbstractClient {

	protected final String baseURL;
	protected final SessionPropagator sessionPropagator;

	private static final Log logger = LogFactory.getLog(AbstractClient.class);

	protected AbstractClient(final String baseURL, @Nullable final SessionPropagator sessionPropagator) {

		this.baseURL = checkNotNull(baseURL, "baseURL");
		this.sessionPropagator = sessionPropagator;

		checkArgument(!isBlank(baseURL), "baseURL should not be empty");
	}

	protected static <U extends V, V> RestTemplate createRestTemplate() {

		final SimpleModule module = new SimpleModule();

		return createRestTemplate(module);
	}

	protected static <U extends V, V> RestTemplate createRestTemplate(final Class<?> responseClass) {

		checkNotNull(responseClass, "responseClass");

		final SimpleModule module = new SimpleModule();

		addResponseDeserializer(module, responseClass);

		return createRestTemplate(module);
	}

	protected static <U, V extends U> RestTemplate createRestTemplate(final Class<?> responseClass, //
			final Class<U> subClass1, final Class<V> subImplClass1 //
	) {

		checkNotNull(responseClass, "responseClass");
		checkNotNull(subClass1, "subClass1");
		checkNotNull(subImplClass1, "subImplClass1");

		final SimpleModule module = new SimpleModule();

		addResponseDeserializer(module, responseClass);

		final DataBeanDeserializer<V> deserializer = addResponseDeserializer(module, subImplClass1);

		module.addDeserializer(subClass1, deserializer);

		return createRestTemplate(module);
	}

	private static RestTemplate createRestTemplate(final SimpleModule module) {

		final RestTemplate restTemplate = new RestTemplate();

		final ObjectMapper objectMapper = new ObjectMapper();

		objectMapper.registerModule(module);

		final MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter(
				objectMapper);

		mappingJackson2HttpMessageConverter.setDefaultCharset(Charset.forName(UTF_8));

		mappingJackson2HttpMessageConverter.setSupportedMediaTypes(
				Lists.newArrayList(MediaType.APPLICATION_JSON, MediaType.parseMediaType("application/*+json")));

		restTemplate.getMessageConverters()
				.removeIf(messageConverter -> messageConverter.getClass() == MappingJackson2HttpMessageConverter.class);

		restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

		return restTemplate;
	}

	private static <T> DataBeanDeserializer<T> addResponseDeserializer(final SimpleModule module,
			final Class<T> responseClass) {

		final DataBeanDeserializer<T> deserializer = new DataBeanDeserializer<T>(responseClass);

		module.addDeserializer(responseClass, deserializer);

		return deserializer;
	}

	protected static HttpHeaders headers(final String correlationId, final SessionPropagator sessionPropagator) {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(sessionPropagator, "sessionPropagator");

		return headers(correlationId, sessionPropagator.getAuthorizationHeader(), sessionPropagator.getUserSessionId());
	}

	protected static HttpHeaders headers(@Nullable final String correlationId,
			@Nullable final String authorizationHeader, @Nullable final String userSessionId) {

//		if (logger.isDebugEnabled()) {
//			logger.debug("headers(): " + authorizationHeader + ", " + userSessionId);
//		}

		final HttpHeaders headers = new HttpHeaders();

		if (correlationId != null) {
			headers.add("Correlation-ID", correlationId);
		}

		if (authorizationHeader != null) {
			headers.add(AUTHORIZATION, authorizationHeader);
		}

		if (userSessionId != null) {
			headers.add(COOKIE, "user_session_id=" + userSessionId);
		}

		return headers;
	}

	@FunctionalInterface
	protected static interface Action<T> {

		T execute();
	}

	@FunctionalInterface
	protected static interface ActionVoid {

		void execute();
	}

	protected static <T> T wrap(final Action<T> action) throws ServiceException {

		checkNotNull(action, "action");

		try {

			return action.execute();

		} catch (final HttpServerErrorException | HttpClientErrorException e) {

			final String body = e.getResponseBodyAsString();

			logger.error(body, e);

			throw new UnexpectedException(e);
		}
	}

	protected static void wrap(final ActionVoid action) throws ServiceException {

		checkNotNull(action, "action");

		try {

			action.execute();

		} catch (final HttpServerErrorException | HttpClientErrorException e) {

			final String body = e.getResponseBodyAsString();

			logger.error(body, e);

			throw new UnexpectedException(e);
		}
	}

	protected final <T, U> ResponseEntity<T> exchange( //
			final RestTemplate restTemplate, //
			final String uri, //
			final HttpMethod method, //
			final HttpEntity<U> request, //
			final Class<T> clazz) {

		checkNotNull(restTemplate, "restTemplate");
		checkNotNull(uri, "uri");
		checkNotNull(method, "method");
		checkNotNull(request, "request");
		checkNotNull(clazz, "clazz");

		if (logger.isInfoEnabled()) {
			logger.info("exchange(), method: " + method + ", uri: " + uri + ", class: " + clazz.getSimpleName());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("exchange(), request.body: " + request.getBody());
		}

		return restTemplate.exchange(uri, method, request, clazz);
	}

	protected final <U> void postForLocation( //
			final RestTemplate restTemplate, //
			final String uri, //
			final HttpEntity<U> request) {

		checkNotNull(restTemplate, "restTemplate");
		checkNotNull(uri, "uri");
		checkNotNull(request, "request");

		if (logger.isInfoEnabled()) {
			logger.info("postForLocation(), uri: " + uri);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("postForLocation(), request.body: " + request.getBody());
		}

		restTemplate.postForLocation(uri, request);
	}
}
