package net.avcompris.commons3.client;

import javax.annotation.Nullable;

import org.apache.commons.logging.Log;
import org.springframework.stereotype.Component;

import net.avcompris.commons3.utils.LogFactory;

@Component
public final class SessionPropagator {

	private static final ThreadLocal<String> USER_SESSION_ID = new ThreadLocal<>();
	private static final ThreadLocal<String> AUTHORIZATION_HEADER = new ThreadLocal<>();

	private static final Log logger = LogFactory.getLog(SessionPropagator.class);

	public SessionPropagator setUserSessionId(@Nullable final String userSessionId) {

		if (logger.isDebugEnabled()) {
			logger.debug("setUserSessionId(): " + userSessionId);
		}

		if (userSessionId == null) {

			USER_SESSION_ID.remove();

		} else {

			USER_SESSION_ID.set(userSessionId);
		}

		return this;
	}

	public SessionPropagator setAuthorizationHeader(@Nullable final String authorizationHeader) {

		if (logger.isDebugEnabled()) {
			logger.debug("setAuthorizationHeader(): " + authorizationHeader);
		}

		if (authorizationHeader == null) {

			AUTHORIZATION_HEADER.remove();

		} else {

			AUTHORIZATION_HEADER.set(authorizationHeader);
		}

		return this;
	}

	@Nullable
	public String getUserSessionId() {

		return USER_SESSION_ID.get();
	}

	@Nullable
	public String getAuthorizationHeader() {

		return AUTHORIZATION_HEADER.get();
	}
}
