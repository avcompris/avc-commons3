package net.avcompris.commons3.dao.impl;

import org.joda.time.DateTime;

import net.avcompris.commons3.dao.EntityDto;

public interface MutableEntityDto extends EntityDto {

	MutableEntityDto setRevision(int revision);

	MutableEntityDto setCreatedAt(DateTime createdAt);

	MutableEntityDto setUpdatedAt(DateTime updatedAt);
}
