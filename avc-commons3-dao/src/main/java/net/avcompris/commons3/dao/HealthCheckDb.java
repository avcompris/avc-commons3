package net.avcompris.commons3.dao;

import javax.annotation.Nullable;

public interface HealthCheckDb {

	boolean isOk();

	@Nullable
	String[] getErrors();

	@Nullable
	RuntimeDbStatus getRuntimeDbStatus();

	interface RuntimeDbStatus {

		boolean isOk();

		RuntimeDbTable[] getTables();
	}

	interface RuntimeDbTable {

		boolean isOk();

		String getRuntimeName();

		String getCompileName();

		boolean getExistsInRuntimeDb();

		@Nullable
		RuntimeDbColumn[] getColumns();
	}

	interface RuntimeDbColumn {

		boolean isOk();

		String getName();

		@Nullable
		String getRuntimeLiteral();

		@Nullable
		String getCompileLiteral();
	}
}
