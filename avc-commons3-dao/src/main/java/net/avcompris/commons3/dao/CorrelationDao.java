package net.avcompris.commons3.dao;

import java.io.IOException;
import java.sql.SQLException;

import net.avcompris.commons3.dao.exception.DuplicateEntityException;

public interface CorrelationDao {

	boolean isCorrelationIdValid(String correlationId) throws SQLException, IOException;

	void addCorrelationId(String correlationId) throws SQLException, IOException, DuplicateEntityException;

	void purgeOlderThanSec(int seconds) throws SQLException, IOException;
}
