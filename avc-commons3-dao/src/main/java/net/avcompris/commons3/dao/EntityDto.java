package net.avcompris.commons3.dao;

import org.joda.time.DateTime;

public interface EntityDto {

	DateTime getCreatedAt();

	DateTime getUpdatedAt();

	int getRevision();
}
