package net.avcompris.commons3.dao.impl;

import javax.annotation.Nullable;

import net.avcompris.commons3.dao.EntityDto;

public interface MutableEntitiesDto<T extends EntityDto> {

	MutableEntitiesDto<T> setTotal(int total);

	MutableEntitiesDto<T> addToResults(T item);

	MutableEntitiesDto<T> setSqlWhereClause(@Nullable String sqlWhereClause);
}
