package net.avcompris.commons3.dao.exception;

import javax.annotation.Nullable;

public final class EntityNotFoundException extends Exception {

	private static final long serialVersionUID = -7715140428919594334L;

	public EntityNotFoundException(@Nullable final String message) {

		super(message);
	}

	public EntityNotFoundException(@Nullable final String message, @Nullable final Throwable cause) {

		super(message, cause);
	}
}
