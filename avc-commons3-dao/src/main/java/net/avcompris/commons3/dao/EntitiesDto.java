package net.avcompris.commons3.dao;

import javax.annotation.Nullable;

public interface EntitiesDto<T extends EntityDto> {

	int getTotal();

	T[] getResults();

	@Nullable
	String getSqlWhereClause();
}
