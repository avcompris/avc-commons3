package net.avcompris.commons3.dao.exception;

import javax.annotation.Nullable;

public final class ForeignEntityViolationException extends Exception {

	private static final long serialVersionUID = -3345304541568843680L;

	public ForeignEntityViolationException(@Nullable final String message) {

		super(message);
	}

	public ForeignEntityViolationException(@Nullable final String message, @Nullable final Throwable cause) {

		super(message, cause);
	}
}
