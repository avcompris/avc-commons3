package net.avcompris.commons3.dao.exception;

import javax.annotation.Nullable;

public final class DuplicateEntityException extends Exception {

	private static final long serialVersionUID = 758161139315445390L;

	public DuplicateEntityException(@Nullable final String message) {

		super(message);
	}

	public DuplicateEntityException(@Nullable final String message, @Nullable final Throwable cause) {

		super(message, cause);
	}

	public DuplicateEntityException(@Nullable final Throwable cause) {

		super(cause);
	}
}
