package net.avcompris.commons3.dao;

import static java.util.Locale.ENGLISH;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;

public interface EntitiesDtoQuery<T extends Filtering<U>, U extends Filtering.Field> {

	@Nullable
	T getFiltering();

	SortBy[] getSortBys();

	Expand[] getExpands();

	int getStart();

	int getLimit();

	EntitiesDtoQuery<T, U> setFiltering(@Nullable T filtering);

	EntitiesDtoQuery<T, U> setStart(int start);

	EntitiesDtoQuery<T, U> setLimit(int limit);

	interface SortBy {

		String name();

		String toSqlField();

		boolean isDesc();

		static String toSqlField(final String name) {

			return name.endsWith("_DESC")

					? substringBetween(name, "SORT_BY_", "_DESC").toLowerCase(ENGLISH)

					: substringAfter(name, "SORT_BY_").toLowerCase(ENGLISH);
		}

		static boolean isDesc(final String name) {

			return name.endsWith("_DESC");
		}
	}

	interface Expand {

		String name();
	}
}
