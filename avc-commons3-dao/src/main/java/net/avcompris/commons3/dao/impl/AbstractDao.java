package net.avcompris.commons3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.avcompris.commons3.utils.Clock;

public abstract class AbstractDao {

	private static final Log logger = LogFactory.getLog(AbstractDao.class);

	protected final Clock clock;

	protected AbstractDao(final Clock clock) {

		this.clock = checkNotNull(clock, "clock");
	}

	@FunctionalInterface
	protected interface Action<T> {

		T action() throws SQLException, IOException;
	}

	protected static final <T> T retryUntil(final long timeoutMs, final long delayMs, final Action<T> action)
			throws SQLException, IOException {

		return retryUntil(timeoutMs, delayMs, true, action);
	}

	protected static final <T> T retryUntil(final long timeoutMs, final long delayMs,
			final boolean throwExceptionInCaseOfTimeout, final Action<T> action) throws SQLException, IOException {

		checkNotNull(action, "action");

		for (final long startMs = System.currentTimeMillis(); System.currentTimeMillis() < startMs + timeoutMs;) {

			T result = null;

			try {

				result = action.action();

			} catch (final SQLException | IOException | RuntimeException | Error e) {

				throw e;

			} catch (final Exception e) {

				throw new RuntimeException(e);
			}

			if (result != null) {

				return result;
			}

			try {

				Thread.sleep(delayMs);

			} catch (final InterruptedException e) {

				e.printStackTrace();

				// do nothing
			}
		}

		logger.error("A timeout occurs (" + timeoutMs + " ms)");

		if (throwExceptionInCaseOfTimeout) {

			throw new RuntimeException("Timeout after: " + timeoutMs + " ms");

		} else {

			return null;
		}
	}
}
