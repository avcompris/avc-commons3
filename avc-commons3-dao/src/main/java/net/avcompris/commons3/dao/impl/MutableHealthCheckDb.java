package net.avcompris.commons3.dao.impl;

import javax.annotation.Nullable;

import net.avcompris.commons3.dao.HealthCheckDb;

public interface MutableHealthCheckDb extends HealthCheckDb {

	MutableHealthCheckDb setOk(boolean ok);

	MutableHealthCheckDb setComponentName(String componentName);

	MutableHealthCheckDb addToErrors(String errorMessage);

	MutableHealthCheckDb setRuntimeDbStatus(@Nullable HealthCheckDb.RuntimeDbStatus runtimeDbStatus);

	interface RuntimeDbStatus extends HealthCheckDb.RuntimeDbStatus {

		RuntimeDbStatus setOk(boolean ok);

		RuntimeDbStatus addToTables(HealthCheckDb.RuntimeDbTable table);
	}

	interface RuntimeDbTable extends HealthCheckDb.RuntimeDbTable {

		RuntimeDbTable setOk(boolean ok);

		RuntimeDbTable setRuntimeName(String runtimeName);

		RuntimeDbTable setCompileName(String compileName);

		RuntimeDbTable setExistsInRuntimeDb(boolean ok);

		RuntimeDbTable addToColumns(HealthCheckDb.RuntimeDbColumn column);
	}

	interface RuntimeDbColumn extends HealthCheckDb.RuntimeDbColumn {

		RuntimeDbColumn setOk(boolean ok);

		RuntimeDbColumn setName(String name);

		RuntimeDbColumn setRuntimeLiteral(@Nullable String runtimeLiteral);

		RuntimeDbColumn setCompileLiteral(@Nullable String compileLiteral);
	}
}
