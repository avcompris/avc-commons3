package net.avcompris.commons3.notifier.api;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

public interface ErrorNotification {

	DateTime getDateTime();

	String getClassName();

	@Nullable
	String getMessage();

	@Nullable
	ErrorNotification getCause();

	StackTraceElement[] getStackTrace();

	interface StackTraceElement {

		@Nullable
		String getClassName();

		@Nullable
		String getMethodName();

		@Nullable
		String getFileName();

		int getLineNumber();

		StackTraceElement setClassName(@Nullable String className);

		StackTraceElement setMethodName(@Nullable String methodName);

		StackTraceElement setFileName(@Nullable String fileName);

		StackTraceElement setLineNumber(int lineNumber);
	}

	ErrorNotification setDateTime(DateTime dateTime);

	ErrorNotification setClassName(String className);

	ErrorNotification setMessage(@Nullable String message);

	ErrorNotification setCause(@Nullable ErrorNotification cause);

	ErrorNotification addToStackTrace(StackTraceElement ste);
}
