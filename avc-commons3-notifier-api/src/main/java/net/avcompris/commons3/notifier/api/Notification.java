package net.avcompris.commons3.notifier.api;

import javax.annotation.Nullable;

public interface Notification {

	ActionType getActionType();

	interface ActionType {

		String name();
	}

	@Nullable
	String getUsername();

	Notification setActionType(ActionType actionType);

	Notification setUsername(@Nullable String username);
}
