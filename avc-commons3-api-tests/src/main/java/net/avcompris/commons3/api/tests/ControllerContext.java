package net.avcompris.commons3.api.tests;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.lang.reflect.Method;
import java.util.Map;

import net.avcompris.commons3.web.AbstractController;

public final class ControllerContext {

	public final Class<? extends AbstractController> controllerClass;
	public final Method controllerMethod;
	private final Map<String, String> variables = newHashMap();

	public ControllerContext( //
			final Class<? extends AbstractController> controllerClass, //
			final Method controllerMethod) {

		this.controllerClass = checkNotNull(controllerClass, "controllerClass");
		this.controllerMethod = checkNotNull(controllerMethod, "controllerMethod");
	}

	public ControllerContext addVariable(final String variableName, final String variableValue) {

		variables.put(variableName, variableValue);

		return this;
	}

	public boolean hasVariable(final String variableName) {

		return variables.containsKey(variableName);
	}

	public String getVariable(final String variableName) {

		final String variableValue = variables.get(variableName);

		checkArgument(variableValue != null, //
				"variableName: %s", variableName);

		return variableValue;
	}
}
