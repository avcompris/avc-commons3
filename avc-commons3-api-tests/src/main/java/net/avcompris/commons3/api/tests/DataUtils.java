package net.avcompris.commons3.api.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.api.tests.AbstractApiTest.extractGetter;
import static net.avcompris.commons3.api.tests.AbstractApiTest.invoke;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import java.lang.reflect.Array;
import java.lang.reflect.Method;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

abstract class DataUtils {

	@Nullable
	public static String getPropertyAsString(final Object dataBean, final String propertyName) {

		checkNotNull(dataBean, "dataBean");
		checkNotNull(propertyName, "propertyName");

		final Object value;

		if (dataBean instanceof JSONObject) {

			final JSONObject jsonObject = (JSONObject) dataBean;

			if (propertyName.endsWith(".length")) {

				final JSONArray array = (JSONArray) jsonObject.get(substringBefore(propertyName, ".length"));

				value = array.size();

			} else if (propertyName.endsWith("]")) {

				final String indexAsString = substringBetween(propertyName, "[", "]");

				final int index;

				try {

					index = Integer.parseInt(indexAsString);

				} catch (final NumberFormatException e) {

					throw new NotImplementedException("propertyName: " + propertyName);
				}

				final JSONArray array = (JSONArray) jsonObject.get(substringBefore(propertyName, "["));

				value = array.get(index);

			} else if (propertyName.contains("].")) {

				final String indexAsString = substringBetween(propertyName, "[", "]");

				final int index;

				try {

					index = Integer.parseInt(indexAsString);

				} catch (final NumberFormatException e) {

					throw new NotImplementedException("propertyName: " + propertyName);
				}

				final JSONArray array = (JSONArray) jsonObject.get(substringBefore(propertyName, "["));

				final Object sub = array.get(index);

				return getPropertyAsString(sub, substringAfter(propertyName, "]."));

			} else if (propertyName.contains(".")) {

				throw new NotImplementedException("propertyName: " + propertyName);

			} else {

				value = jsonObject.get(propertyName);
			}

		} else if (propertyName.endsWith(".length")) {

			final Method getter = extractGetter(dataBean.getClass(), substringBefore(propertyName, ".length"));

			value = Array.getLength(invoke(getter, dataBean));

		} else if (propertyName.endsWith("]")) {

			final String indexAsString = substringBetween(propertyName, "[", "]");

			final int index;

			try {

				index = Integer.parseInt(indexAsString);

			} catch (final NumberFormatException e) {

				throw new NotImplementedException("propertyName: " + propertyName);
			}

			final Method getter = extractGetter(dataBean.getClass(), substringBefore(propertyName, "["));

			value = Array.get(invoke(getter, dataBean), index);

		} else if (propertyName.contains("].")) {

			final String indexAsString = substringBetween(propertyName, "[", "]");

			final int index;

			try {

				index = Integer.parseInt(indexAsString);

			} catch (final NumberFormatException e) {

				throw new NotImplementedException("propertyName: " + propertyName);
			}

			final Method getter = extractGetter(dataBean.getClass(), substringBefore(propertyName, "["));

			final Object sub = Array.get(invoke(getter, dataBean), index);

			return getPropertyAsString(sub, substringAfter(propertyName, "]."));

		} else if (propertyName.contains(".")) {

			throw new NotImplementedException("propertyName: " + propertyName);

		} else {

			final Method getter = extractGetter(dataBean.getClass(), propertyName);

			value = invoke(getter, dataBean);
		}

		if (value == null) {

			return null;

		} else {

			return value.toString();
		}
	}
}
