package net.avcompris.commons3.api.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;

import com.google.common.collect.Iterables;

import net.avcompris.commons3.yaml.Yaml;
import net.avcompris.commons3.yaml.YamlUtils;

public final class TestsSpec {

	private final List<TestSpec> tests = new ArrayList<>();

	private TestsSpec(final Yaml yaml, final Class<?>[] classes) {

		checkNotNull(yaml, "yaml");
		checkNotNull(classes, "classes");

		final Yaml tests = yaml.get("tests");

		for (final Object key : tests.keys()) {

			final String testName = (String) key;

			final Yaml test = tests.get(testName);

			final AuthenticationSpec authenticationSpec;

			if (test.has("authentified")) {

				final String authentified = test.get("authentified").asString();

				if ("false".contentEquals(authentified)) {

					authenticationSpec = null;

				} else if ("true".contentEquals(authentified)) {

					authenticationSpec = new AuthenticationSpec() {

					};

				} else {

					throw new NotImplementedException("authentified: " + authentified);
				}

			} else {

				throw new NotImplementedException("YAML has no \"authentified\" section");
			}

			final List<Step> steps = new ArrayList<>();

			if (test.has("request")) {

				steps.add(loadStep(authenticationSpec, test, classes));

			} else if (test.has("steps")) {

				for (final Yaml step : test.get("steps").items()) {

					steps.add(loadStep(authenticationSpec, step, classes));
				}

			} else {

				throw new NotImplementedException("");
			}

			this.tests.add(new TestSpec() {

				@Override
				public String getName() {
					return testName;
				}

				@Override
				@Nullable
				public AuthenticationSpec getAuthentication() {
					return authenticationSpec;
				}

				@Override
				public Step[] getSteps() {
					return Iterables.toArray(steps, Step.class);
				}

				@Override
				public String toString() {
					return testName;
				}
			});
		}
	}

	public static TestsSpec load(final File yamlFile, final Class<?>... classes) throws IOException {

		checkNotNull(yamlFile, "yamlFile");

		try (InputStream is = new FileInputStream(yamlFile)) {

			return load(is, classes);
		}
	}

	public static TestsSpec load(final InputStream is, final Class<?>... classes) throws IOException {

		checkNotNull(is, "is");

		final Yaml yaml = YamlUtils.loadYaml(is);

		return new TestsSpec(yaml, classes);
	}

	public TestSpec[] getTests() {

		return Iterables.toArray(tests, TestSpec.class);
	}

	public interface TestSpec {

		String getName();

		@Nullable
		AuthenticationSpec getAuthentication();

		Step[] getSteps();
	}

	public interface AuthenticationSpec {

	}

	public enum HttpMethod {

		GET, POST, PUT, DELETE,
	}

	public interface Step {

		@Nullable
		AuthenticationSpec getAuthentication();

		HttpMethod getHttpMethod();

		String getPath();

		Data[] getData();

		@Nullable
		Class<?> getReturnType();

		Assertion[] getAssertions();

		Let[] getLets();
	}

	public interface Data {

		String getName();

		Object getValue();
	}

	public enum AssertionType {

		EQUALS,
	}

	public interface Assertion {

		AssertionType getType();

		String getLeft();

		String getRight();
	}

	public interface Let {

		String getVariableName();

		String getExpression();
	}

	private static Class<?> extractClass(final String classSimpleName, final Class<?>[] classes) {

		for (final Class<?> clazz : classes) {

			if (clazz.getSimpleName().contentEquals(classSimpleName)) {

				return clazz;
			}
		}

		throw new RuntimeException("Unknown class references in YAML: " + classSimpleName);
	}

	private static Step loadStep(@Nullable final AuthenticationSpec authenticationSpec, final Yaml step,
			final Class<?>[] classes) {

		final String request = step.get("request").asString();

		final Yaml response = step.get("response");

		final String responseStatus;
		final Class<?> returnType;

		if (response.has("status")) {

			responseStatus = response.get("status").asString();

		} else {

			responseStatus = response.asString();
		}

		if (response.has("class")) {

			returnType = extractClass(response.get("class").asString(), classes);

		} else {

			returnType = null;
		}

		final HttpMethod method = HttpMethod.valueOf(substringBefore(request, " ").trim());

		final String path = substringAfter(request, " ").trim();

		final List<Data> data = new ArrayList<>();

		if (step.has("data")) {

			for (final Object key : step.get("data").keys()) {

				final String name = (String) key;

				final Object value;

				final Yaml yaml = step.get("data").get(name);

				if (yaml.isArray()) {

					final List<String> strings = new ArrayList<>();

					for (final Yaml item : yaml.items()) {

						strings.add(item.asString());
					}

					value = Iterables.toArray(strings, String.class);

				} else {

					value = yaml.asString();
				}

				data.add(new Data() {

					@Override
					public String getName() {
						return name;
					}

					@Override
					public Object getValue() {
						return value;
					}
				});
			}
		}

		final List<Assertion> assertions = new ArrayList<>();

		assertions.add(new Assertion() {

			@Override
			public AssertionType getType() {
				return AssertionType.EQUALS;
			}

			@Override
			public String getLeft() {
				return "response.status";
			}

			@Override
			public String getRight() {
				return responseStatus;
			}
		});

		if (step.has("assertions")) {

			for (final Yaml item : step.get("assertions").items()) {

				final String expression = item.asString();

				if (!expression.contains("==")) {

					throw new NotImplementedException("expession: " + expression);
				}

				final String left = substringBefore(expression, "==").trim();
				final String right = substringAfter(expression, "==").trim();

				assertions.add(new Assertion() {

					@Override
					public AssertionType getType() {
						return AssertionType.EQUALS;
					}

					@Override
					public String getLeft() {
						return left;
					}

					@Override
					public String getRight() {
						return right;
					}
				});
			}
		}

		final List<Let> lets = new ArrayList<>();

		if (step.has("let")) {

			final Yaml let = step.get("let");

			for (final Object key : let.keys()) {

				final String variableName = (String) key;

				final String expression = let.get(variableName).asString();

				lets.add(new Let() {

					@Override
					public String getVariableName() {
						return variableName;
					}

					@Override
					public String getExpression() {
						return expression;
					}
				});
			}
		}

		return new Step() {

			@Override
			@Nullable
			public AuthenticationSpec getAuthentication() {
				return authenticationSpec;
			}

			@Override
			public HttpMethod getHttpMethod() {
				return method;
			}

			@Override
			public String getPath() {
				return path;
			}

			@Override
			public Data[] getData() {
				return Iterables.toArray(data, Data.class);
			}

			@Override
			@Nullable
			public Class<?> getReturnType() {
				return returnType;
			}

			@Override
			public Assertion[] getAssertions() {
				return Iterables.toArray(assertions, Assertion.class);
			}

			@Override
			public Let[] getLets() {
				return Iterables.toArray(lets, Let.class);
			}
		};
	}
}
