package net.avcompris.commons3.api.tests;

import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import com.google.common.collect.ImmutableMap;

public abstract class ExpressionUtils {

	public static String compute(final String expression) {

		return compute(expression, ImmutableMap.of());
	}

	public static String compute(final String expression, final ImmutableMap<String, String> variables) {

		try {

			return Integer.toString(Integer.parseInt(expression));

		} catch (final NumberFormatException e) {

			// do nothing
		}

		if (variables.containsKey(expression)) {

			return variables.get(expression);

		} else if (expression.startsWith("$") && variables.containsKey(expression.substring(1))) {

			return variables.get(expression.substring(1));
		}

		if (expression.endsWith(" + 1")) {

			final String left = substringBeforeLast(expression, " + 1").trim();

			if (variables.containsKey(left)) {

				return Integer.toString(Integer.parseInt(variables.get(left)) + 1);

			} else if (left.startsWith("$") && variables.containsKey(left.substring(1))) {

				return Integer.toString(Integer.parseInt(variables.get(left.substring(1))) + 1);
			}
		}

		return expression;

		// throw new NotImplementedException("expression: " + expression);
	}
}
