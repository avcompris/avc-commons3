package net.avcompris.commons3.api.tests;

import static com.google.common.base.Preconditions.checkState;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.util.Iterator;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;

public final class TestSpecParametersExtension implements ParameterResolver {

	@Override
	public boolean supportsParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
			throws ParameterResolutionException {

		final Executable declaringExecutable = parameterContext.getParameter().getDeclaringExecutable();

		return Constructor.class.isAssignableFrom(declaringExecutable.getClass())
				&& TestSpec.class.equals(parameterContext.getParameter().getType());
	}

	@Override
	public TestSpec resolveParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
			throws ParameterResolutionException {

		checkState(iterator != null, "iterator should not be null");

		final TestSpec testSpec = iterator.next();

		if (!iterator.hasNext()) {

			iterator = null;
		}

		return testSpec;
	}

	private static Iterator<TestSpec> iterator = null;

	public static void inject(final Iterable<TestSpec> testSpecs) {

		checkState(iterator == null, "iterator should be null");

		iterator = testSpecs.iterator();

		if (!iterator.hasNext()) {

			throw new IllegalArgumentException("iterable should not be empty");
		}
	}
}
