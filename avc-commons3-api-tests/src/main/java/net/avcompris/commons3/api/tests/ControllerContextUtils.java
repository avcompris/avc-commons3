package net.avcompris.commons3.api.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;

import java.lang.reflect.Method;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.web.bind.annotation.RequestMapping;

import net.avcompris.commons3.api.tests.TestsSpec.HttpMethod;
import net.avcompris.commons3.web.AbstractController;

public abstract class ControllerContextUtils {

	public static ControllerContext extractControllerContext( //
			final HttpMethod httpMethod, final String uri, //
			final Class<? extends AbstractController> controllerClass) {

		checkNotNull(httpMethod, "httpMethod");
		checkNotNull(uri, "uri");
		checkNotNull(controllerClass, "controllerClass");

		final String path;
		final String query;

		if (uri.contains("?")) {

			path = substringBefore(uri, "?");
			query = substringAfter(uri, "?");

		} else {

			path = uri;
			query = null;
		}

		// 1. Exact matching

		for (final Method method : controllerClass.getMethods()) {

			final RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);

			if (requestMapping == null) {
				continue;
			} else if (!httpMethod.name().contentEquals(requestMapping.method()[0].name())) {
				continue;
			}

			final String requestMappingPath = requestMapping.value()[0];

			if (requestMappingPath.contains("{")) {
				continue;
			}

			if (path.contentEquals(requestMappingPath)) {

				final ControllerContext context = new ControllerContext( //
						controllerClass, //
						method);

				return populateWithQuery(context, query);
			}
		}

		// 2. Substitutions

		methods: for (final Method method : controllerClass.getMethods()) {

			final RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);

			if (requestMapping == null) {
				continue;
			} else if (!httpMethod.name().contentEquals(requestMapping.method()[0].name())) {
				continue;
			}

			final String requestMappingPath = requestMapping.value()[0];

			if (!requestMappingPath.contains("{")) {
				continue;
			}

			final int start = requestMappingPath.indexOf("{");

			if (!path.startsWith(requestMappingPath.substring(0, start))) {
				continue methods;
			}

			int pathVariableOffset = start;

			final ControllerContext context = new ControllerContext( //
					controllerClass, //
					method);

			for (int variableOffset = start;;) {

				final int variableEnd = requestMappingPath.indexOf("}", variableOffset) + 1;

				final String suffix = requestMappingPath.substring(variableEnd);

				final String variableName = requestMappingPath.substring(variableOffset + 1, variableEnd - 1);

				if (!suffix.contains("{")) {

					final String variableValue;

					if (suffix.isEmpty()) {

						variableValue = path.substring(pathVariableOffset);

					} else if (path.length() >= variableOffset + suffix.length() && path.endsWith(suffix)) {

						variableValue = path.substring(pathVariableOffset, path.length() - suffix.length());

					} else {

						continue methods;
					}

					if (variableValue.contains("/")) {

						continue methods;
					}

					context.addVariable(variableName, variableValue);

					break;
				}

				variableOffset = requestMappingPath.indexOf("{", variableEnd);

				final String fragment = requestMappingPath.substring(variableEnd, variableOffset);

				if (fragment.isEmpty() //
						|| fragment.charAt(0) != '/' //
						|| fragment.charAt(fragment.length() - 1) != '/') {

					throw new NotImplementedException("requestMappingPath: " + requestMappingPath);
				}

				final String variableValue;

				final int pathVariableEnd = path.indexOf("/", pathVariableOffset);

				if (pathVariableEnd == -1) {
					continue methods;
				}

				variableValue = path.substring(pathVariableOffset, pathVariableEnd);

				pathVariableOffset = pathVariableEnd + variableOffset - variableEnd;

				final String pathFragment = path.substring(pathVariableEnd, pathVariableOffset);

				context.addVariable(variableName, variableValue);

				if (!pathFragment.contentEquals(fragment)) {

					continue methods;
				}
			}

			return context;
		}

		// 9. Nothing matches

		return null;
	}

	private static ControllerContext populateWithQuery(final ControllerContext context, @Nullable final String query) {

		if (query == null) {

			return context;
		}

		if (query.contains("&")) {
			throw new NotImplementedException("query: " + query);
		}

		final String variableName = substringBefore(query, "=");
		final String variableValue = substringAfter(query, "=").replace('+', ' ');

		context.addVariable(variableName, variableValue);

		return context;
	}
}