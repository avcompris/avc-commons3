package net.avcompris.commons3.api.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Locale.ENGLISH;
import static net.avcompris.commons3.api.tests.DataUtils.getPropertyAsString;
import static net.avcompris.commons3.api.tests.ExpressionUtils.compute;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBetween;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.collect.ImmutableMap;

import net.avcompris.commons3.api.tests.TestsSpec.Assertion;
import net.avcompris.commons3.api.tests.TestsSpec.AssertionType;
import net.avcompris.commons3.api.tests.TestsSpec.AuthenticationSpec;
import net.avcompris.commons3.api.tests.TestsSpec.Data;
import net.avcompris.commons3.api.tests.TestsSpec.HttpMethod;
import net.avcompris.commons3.api.tests.TestsSpec.Let;
import net.avcompris.commons3.api.tests.TestsSpec.Step;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;

public abstract class AbstractApiTest {

	protected final TestSpec spec;

	@Nullable
	protected final String superadminAuthorization;

	protected AbstractApiTest(final TestSpec spec, //
			@Nullable final String superadminAuthorization) {

		this.spec = checkNotNull(spec, "spec");

		this.superadminAuthorization = superadminAuthorization;
	}

	protected interface StepExecution {

		@Nullable
		AuthenticationSpec getAuthentication();

		HttpMethod getHttpMethod();

		String getPath();

		Data[] getData();

		@Nullable
		Class<?> getReturnType();
	}

	protected static final class StepExecutionResult {

		public final int statusCode;

		@Nullable
		public final Object result;

		public StepExecutionResult(final int statusCode, @Nullable final Object result) {

			this.statusCode = statusCode;
			this.result = result;
		}
	}

	protected abstract StepExecutionResult execute(int stepIndex, StepExecution stepExecution) throws Exception;

	private static final class StepExecutionImpl implements StepExecution {

		private final Step step;
		private final String path;
		private final Map<String, String> variables;

		public StepExecutionImpl(final Step step, final Map<String, String> variables) {

			this(step, step.getPath(), variables);
		}

		public StepExecutionImpl(final Step step, final String path, final Map<String, String> variables) {

			this.step = checkNotNull(step, "step");
			this.path = checkNotNull(path, "path");
			this.variables = checkNotNull(variables, "variables");
		}

		@Override
		public AuthenticationSpec getAuthentication() {
			return step.getAuthentication();
		}

		@Override
		public HttpMethod getHttpMethod() {
			return step.getHttpMethod();
		}

		@Override
		public String getPath() {
			return path;
		}

		@Override
		public Data[] getData() {

			final Data[] data = Arrays.copyOf(step.getData(), step.getData().length);

			for (int i = 0; i < data.length; ++i) {

				final Data dataItem = data[i];

				final String dataValue = dataItem.getValue().toString();

				if (dataValue.startsWith("$")) {

					final String variableName = substringAfter(dataValue, "$");

					final String variableValue = variables.get(variableName);

					checkState(variableValue != null, "variable should not be null: %s", dataValue);

					data[i] = new Data() {

						@Override
						public String getName() {
							return dataItem.getName();
						}

						@Override
						public Object getValue() {
							return variableValue;
						}
					};
				}
			}

			return data;
		}

		@Override
		@Nullable
		public Class<?> getReturnType() {
			return step.getReturnType();
		}
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("testSpecs")
	public final void test(final TestSpec testSpec) throws Exception {

		final Map<String, String> variables = newHashMap();

		int stepIndex = 0;

		for (final Step step : spec.getSteps()) {

			final String path = step.getPath();

			final StepExecution stepExecution;

			if (path.contains("$")) {

				String newPath = path;

				for (final Let let : step.getLets()) {

					final String variableName = let.getVariableName();

					final String expression = let.getExpression();

					final String processedValue;

					if (expression.startsWith("$random_alpha(") && expression.endsWith(")")) {

						processedValue = randomAlphabetic(Integer.parseInt(substringBetween(expression, "(", ")"))) //
								.toLowerCase(ENGLISH);

					} else if (expression.startsWith("$random_email(") && expression.endsWith(")")) {

						processedValue = (randomAlphabetic(8) + "@" + randomAlphabetic(8) + ".com") //
								.toLowerCase(ENGLISH);

					} else if (expression.startsWith("$")) {

						throw new NotImplementedException(
								"expression: " + expression + ", for variable: " + variableName);

					} else {

						continue; // do nothing
					}

					System.out.println("let " + variableName + " = " + processedValue);

					variables.put(variableName, processedValue);
				}

				for (final String variableName : variables.keySet()) {

					while (newPath.contains("$" + variableName)) {

						newPath = newPath.replace("$" + variableName, variables.get(variableName));
					}
				}

				stepExecution = new StepExecutionImpl(step, newPath, variables);

			} else {

				stepExecution = new StepExecutionImpl(step, variables);
			}

			final StepExecutionResult stepExecutionResult = execute(stepIndex, stepExecution);

			final int statusCode = stepExecutionResult.statusCode;

			@Nullable
			final Object result = stepExecutionResult.result;

			for (final Let let : step.getLets()) {

				final String variableName = let.getVariableName();

				final String expression = let.getExpression();

				if (expression.startsWith("response.data.")) {

					assertNotNull(result, "Step #" + stepIndex + ": result should not be null");

					final String propertyName = substringAfter(expression, "response.data.");

					final String propertyValue = getPropertyAsString(result, propertyName);

					System.out.println("let " + variableName + " = " + propertyValue);

					variables.put(variableName, propertyValue);
				}
			}

			for (final Assertion assertion : step.getAssertions()) {

				final AssertionType assertionType = assertion.getType();

				if (assertionType != AssertionType.EQUALS) {
					throw new NotImplementedException("assertion.type: " + assertionType);
				}

				final String left = assertion.getLeft();
				final String right = assertion.getRight();

				if ("response.status".contentEquals(left)) {

					assertEquals(Integer.parseInt(right), statusCode, "Step #" + stepIndex + ": " + left);

				} else if (left.startsWith("response.data.")) {

					assertNotNull(result, "Step #" + stepIndex + ": result should not be null");

					final String propertyName = substringAfter(left, "response.data.");

					final String propertyValue = getPropertyAsString(result, propertyName);

					assertEquals( //
							compute(right, ImmutableMap.copyOf(variables)), //
							propertyValue, //
							"Step #" + stepIndex + ": " + left);

				} else {

					final String leftValue = compute(left, ImmutableMap.copyOf(variables));
					final String rightValue = compute(right, ImmutableMap.copyOf(variables));

					assertEquals( //
							rightValue, //
							leftValue, //
							"Step #" + stepIndex + ": " + left + " should be equal to: " + right);
				}
			}

			++stepIndex;
		}
	}

	@Nullable
	private static Method extractGetterOrNull(final Class<?> clazz, final String propertyName) {

		for (final Class<?> i : clazz.getInterfaces()) {

			final Method method = extractGetterOrNull(i, propertyName);

			if (method != null) {

				return method;
			}
		}

		final String is = "is" + capitalize(propertyName);

		final String get = "get" + capitalize(propertyName);

		for (final Method method : clazz.getMethods()) {

			if (method.getParameterCount() != 0) {
				continue;
			}

			final String methodName = method.getName();

			if (is.contentEquals(methodName) || get.contentEquals(methodName)) {
				return method;
			}
		}

		return null;
	}

	protected static Method extractGetter(final Class<?> clazz, final String propertyName) {

		final Method method = extractGetterOrNull(clazz, propertyName);

		if (method != null) {

			return method;
		}

		throw new IllegalArgumentException("clazz: " + clazz.getName() + ", propertyName: " + propertyName);
	}

	protected static Object invoke(final Method method, final Object dataBean) {

		try {

			return method.invoke(dataBean);

		} catch (final IllegalAccessException e) {

			throw new RuntimeException(e);

		} catch (final InvocationTargetException e) {

			throw new RuntimeException(e.getTargetException());
		}
	}
}
