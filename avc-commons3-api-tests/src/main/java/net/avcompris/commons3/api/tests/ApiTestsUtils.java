package net.avcompris.commons3.api.tests;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;

import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;

public abstract class ApiTestsUtils {

	public static Stream<Arguments> testSpecs(final Class<?>... classes) throws Exception {

		final Collection<Object[]> parameters = parameters(classes);

		final Collection<TestSpec> testSpecs = parameters.stream() //
				.map(args -> (TestSpec) args[0]) //
				.collect(Collectors.toList());

		TestSpecParametersExtension.inject(testSpecs);

		return testSpecs.stream() //
				.map(testSpec -> Arguments.of(testSpec));
	}

	private static Collection<Object[]> parameters(final Class<?>... classes) throws Exception {

		final String RESOURCE_PATH = "api-tests.yml";

		final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(RESOURCE_PATH);

		if (is == null) {
			throw new FileNotFoundException("Cannot find resource: " + RESOURCE_PATH);
		}

		final TestsSpec spec;

		try {

			spec = TestsSpec.load(is, classes);

		} finally {
			is.close();
		}

		final List<Object[]> parameters = new ArrayList<>();

		for (final TestSpec test : spec.getTests()) {

			parameters.add(new Object[] {

					test,

			});
		}

		return parameters;
	}
}
