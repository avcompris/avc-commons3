package net.avcompris.commons3.api.tests;

import static com.google.common.collect.Lists.newArrayList;
import static net.avcompris.commons3.api.tests.DataUtils.getPropertyAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;

import com.google.common.collect.Iterables;

public class DataUtilsTest {

	private static class MyDataBean {

		private int age;
		private final List<String> labels = newArrayList();
		private final List<MySub> subs = newArrayList();

		@SuppressWarnings("unused")
		public int getAge() {
			return age;
		}

		@SuppressWarnings("unused")
		public String[] getLabels() {
			return Iterables.toArray(labels, String.class);
		}

		@SuppressWarnings("unused")
		public MySub[] getSubs() {
			return Iterables.toArray(subs, MySub.class);
		}

		public MyDataBean setAge(final int age) {
			this.age = age;
			return this;
		}

		public MyDataBean addToLabels(final String label) {
			labels.add(label);
			return this;
		}

		public MyDataBean addToSubs(final MySub sub) {
			subs.add(sub);
			return this;
		}
	}

	private static class MySub {

		private String id;

		@SuppressWarnings("unused")
		public String getId() {
			return id;
		}

		public MySub setId(final String id) {
			this.id = id;
			return this;
		}
	}

	@Test
	public void testExtractSimpleInt_Pojo() throws Exception {

		assertEquals("12", getPropertyAsString(new MyDataBean() //
				.setAge(12), //
				"age"));
	}

	@Test
	public void testExtractStringArray_Pojo() throws Exception {

		final MyDataBean pojo = new MyDataBean() //
				.addToLabels("coco");

		assertEquals("1", getPropertyAsString(pojo, //
				"labels.length"));

		assertEquals("coco", getPropertyAsString(pojo, //
				"labels[0]"));
	}

	@Test
	public void testExtractSubArray_Pojo() throws Exception {

		final MyDataBean pojo = new MyDataBean() //
				.addToSubs(new MySub() //
						.setId("xoxox"));

		assertEquals("1", getPropertyAsString(pojo, //
				"subs.length"));

		assertEquals("xoxox", getPropertyAsString(pojo, //
				"subs[0].id"));
	}

	private static JSONObject parseJSON(final String s) throws ParseException {

		return (JSONObject) new JSONParser().parse(s);
	}

	@Test
	public void testExtractSimpleInt_JSON() throws Exception {

		assertEquals("12", getPropertyAsString(parseJSON("{\"age\":12}"),

				"age"));
	}

	@Test
	public void testExtractStringArray_JSON() throws Exception {

		final Object json = parseJSON("{\"labels\":[\"a\",\"b\",\"c\"]}");

		assertEquals("3", getPropertyAsString(json, //
				"labels.length"));

		assertEquals("a", getPropertyAsString(json, //
				"labels[0]"));
	}

	@Test
	public void testExtractSubArray_JSON() throws Exception {

		final Object json = parseJSON("{\"subs\":[{\"id\":\"xoxox\"}]}");

		assertEquals("1", getPropertyAsString(json, //
				"subs.length"));

		assertEquals("xoxox", getPropertyAsString(json, //
				"subs[0].id"));
	}
}
