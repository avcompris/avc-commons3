package net.avcompris.commons3.api.tests;

import static net.avcompris.commons3.api.tests.ExpressionUtils.compute;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableMap;

public class ExpressionUtilsTest {

	@Test
	public void test_1() throws Exception {

		assertEquals("1", compute("1"));
	}

	@Test
	public void test_count0_123() throws Exception {

		assertEquals("123", compute("count0", ImmutableMap.of("count0", "123")));
	}

	@Test
	public void test_dollar_count0_123() throws Exception {

		assertEquals("123", compute("$count0", ImmutableMap.of("count0", "123")));
	}

	@Test
	public void test_count0_plus_1() throws Exception {

		assertEquals("124", compute("count0 + 1", ImmutableMap.of("count0", "123")));
	}

	@Test
	public void test_dollar_count0_plus_1() throws Exception {

		assertEquals("124", compute("$count0 + 1", ImmutableMap.of("count0", "123")));
	}
}
