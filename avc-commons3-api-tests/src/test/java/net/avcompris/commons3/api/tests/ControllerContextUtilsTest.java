package net.avcompris.commons3.api.tests;

import static net.avcompris.commons3.api.tests.ControllerContextUtils.extractControllerContext;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.GET;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.POST;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.avcompris.commons3.web.AbstractController;

public class ControllerContextUtilsTest {

	@Test
	public void testSimplePath() throws Exception {

		final ControllerContext context = extractControllerContext(POST, "/a/b/c", DummyController.class);

		assertEquals(DummyController.class, context.controllerClass);
		assertEquals("createABC", context.controllerMethod.getName());
	}

	@Test
	public void testPath_with1Substitution_trailing() throws Exception {

		final ControllerContext context = extractControllerContext(GET, "/a/b/c/tralala", DummyController.class);

		assertEquals(DummyController.class, context.controllerClass);
		assertEquals("getABC_xxx", context.controllerMethod.getName());
		assertEquals("tralala", context.getVariable("xxx"));
	}

	@Test
	public void testPath_with1Substitution_inline() throws Exception {

		final ControllerContext context = extractControllerContext(GET, "/a/b/c/tralala/d", DummyController.class);

		assertEquals(DummyController.class, context.controllerClass);
		assertEquals("getABC_xxx_d", context.controllerMethod.getName());
		assertEquals("tralala", context.getVariable("xxx"));
	}

	@Test
	public void testPath_with2Substitutions_trailing() throws Exception {

		final ControllerContext context = extractControllerContext(POST, "/a/4521/c/tralala", DummyController.class);

		assertEquals(DummyController.class, context.controllerClass);
		assertEquals("createA4521C_xxx", context.controllerMethod.getName());
		assertEquals("4521", context.getVariable("yyy"));
		assertEquals("tralala", context.getVariable("xxx"));
	}

	@Test
	public void testPath_with2Substitutions_trailing_short() throws Exception {

		final ControllerContext context = extractControllerContext(POST, "/a/4/c/8", DummyController.class);

		assertEquals(DummyController.class, context.controllerClass);
		assertEquals("createA4521C_xxx", context.controllerMethod.getName());
		assertEquals("4", context.getVariable("yyy"));
		assertEquals("8", context.getVariable("xxx"));
	}

	private static final class DummyController extends AbstractController {

		private DummyController() {
			super(null, null, null);
		}

		@RequestMapping(method = RequestMethod.POST, value = "/a/b/c")
		public void createABC() {

		}

		@RequestMapping(method = RequestMethod.GET, value = "/x/y/z")
		public void getXYZ( //
				@RequestParam(name = "q") final String q //
		) {

		}

		@RequestMapping(method = RequestMethod.GET, value = "/a/b/c/{xxx}")
		public void getABC_xxx( //
				@PathVariable("xxx") final String xxx) {

		}

		@RequestMapping(method = RequestMethod.GET, value = "/a/b/c/{xxx}/d")
		public void getABC_xxx_d( //
				@PathVariable("xxx") final String xxx) {

		}

		@RequestMapping(method = RequestMethod.POST, value = "/a/{yyy}/c/{xxx}")
		public void createA4521C_xxx( //
				@PathVariable("yyy") final String yyy, @PathVariable("xxx") final String xxx) {

		}

		@Override
		protected boolean isSecure() {
			return false;
		}

		@Override
		protected boolean isHttpOnly() {
			return false;
		}
	}

	@Test
	public void testPath_withRequestParams() throws Exception {

		final ControllerContext context = extractControllerContext(GET, "/x/y/z?q=ABCD+eq+4", DummyController.class);

		assertEquals(DummyController.class, context.controllerClass);
		assertEquals("getXYZ", context.controllerMethod.getName());
		assertEquals("ABCD eq 4", context.getVariable("q"));
	}
}
