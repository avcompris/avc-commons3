package net.avcompris.commons3.api.tests;

import net.avcompris.commons3.api.Entities;

public interface DummyUsersInfo extends Entities<DummyUserInfo> {

	@Override
	DummyUserInfo[] getResults();
}
