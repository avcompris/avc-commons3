package net.avcompris.commons3.api.tests;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.Entity;
import net.avcompris.commons3.types.DateTimeHolder;

public interface DummyUserInfo extends Entity {

	// @Override
	String getUsername();

	// @Override
	DummyRole getRole();

	@Nullable
	String getPreferredLang();

	@Nullable
	String getPreferredTimeZone();

	boolean isEnabled();

	@Nullable
	DateTimeHolder getLastActiveAt();
}
