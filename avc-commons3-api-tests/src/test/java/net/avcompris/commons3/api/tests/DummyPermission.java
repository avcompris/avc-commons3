package net.avcompris.commons3.api.tests;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.EnumPermission;

public enum DummyPermission implements EnumPermission {

	// ======== ANONYMOUS ========

	ANY,

	// ======== ADMIN ========

	SUPERADMIN,

	GET_ANY_USER_SESSION, TERMINATE_ANY_USER_SESSION,

	QUERY_ALL_USERS, CREATE_ANY_USER, GET_ANY_USER, UPDATE_ANY_USER, DELETE_ANY_USER,

	WORKERS, PURGE_CORRELATION_IDS,

	// ======== ME ========

	SET_LAST_ACTIVE_AT, GET_MY_SESSION, TERMINATE_MY_SESSION,

	GET_USER_ME, UPDATE_USER_ME,

	// ======== ROUTING ========

	ROUTE;

	@Override
	public boolean isSuperadminPermission() {

		return this == SUPERADMIN;
	}

	@Override
	public boolean isAnyUserPermission() {

		return this == ANY;
	}

	@Override
	@Nullable
	public String getExpression() {

		return null;
	}
}
