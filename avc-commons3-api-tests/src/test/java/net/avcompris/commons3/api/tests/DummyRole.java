package net.avcompris.commons3.api.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.api.tests.DummyPermission.ANY;
import static net.avcompris.commons3.api.tests.DummyPermission.CREATE_ANY_USER;
import static net.avcompris.commons3.api.tests.DummyPermission.DELETE_ANY_USER;
import static net.avcompris.commons3.api.tests.DummyPermission.GET_ANY_USER;
import static net.avcompris.commons3.api.tests.DummyPermission.GET_ANY_USER_SESSION;
import static net.avcompris.commons3.api.tests.DummyPermission.GET_MY_SESSION;
import static net.avcompris.commons3.api.tests.DummyPermission.GET_USER_ME;
import static net.avcompris.commons3.api.tests.DummyPermission.PURGE_CORRELATION_IDS;
import static net.avcompris.commons3.api.tests.DummyPermission.QUERY_ALL_USERS;
import static net.avcompris.commons3.api.tests.DummyPermission.ROUTE;
import static net.avcompris.commons3.api.tests.DummyPermission.SET_LAST_ACTIVE_AT;
import static net.avcompris.commons3.api.tests.DummyPermission.TERMINATE_ANY_USER_SESSION;
import static net.avcompris.commons3.api.tests.DummyPermission.TERMINATE_MY_SESSION;
import static net.avcompris.commons3.api.tests.DummyPermission.UPDATE_ANY_USER;
import static net.avcompris.commons3.api.tests.DummyPermission.UPDATE_USER_ME;
import static net.avcompris.commons3.api.tests.DummyPermission.WORKERS;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;

import net.avcompris.commons3.api.EnumRole;

public enum DummyRole implements EnumRole {

	ANONYMOUS(null,

			ANY),

	REGULAR(new DummyRole[] { ANONYMOUS },

			SET_LAST_ACTIVE_AT, GET_MY_SESSION, TERMINATE_MY_SESSION, //
			GET_USER_ME, UPDATE_USER_ME //
	),

	USERMGR(new DummyRole[] { REGULAR },

			QUERY_ALL_USERS, //
			CREATE_ANY_USER, GET_ANY_USER, UPDATE_ANY_USER, DELETE_ANY_USER, //
			GET_ANY_USER_SESSION, //
			TERMINATE_ANY_USER_SESSION //
	),

	SILENT_WORKER(new DummyRole[] { ANONYMOUS },

			ROUTE),

	ADMIN(new DummyRole[] { USERMGR, SILENT_WORKER },

			PURGE_CORRELATION_IDS, WORKERS),

	SUPERADMIN(new DummyRole[] { ADMIN },

			DummyPermission.SUPERADMIN);

	private final DummyRole[] superRoles;
	private final DummyPermission[] permissions;

	DummyRole(@Nullable final DummyRole[] superRoles, final DummyPermission... permissions) {

		this.superRoles = (superRoles != null) ? superRoles : new DummyRole[0];
		this.permissions = checkNotNull(permissions, "permissions");
	}

	public String getRolename() {

		return name();
	}

	@Override
	public DummyRole[] getSuperRoles() {

		return ArrayUtils.clone(superRoles);
	}

	@Override
	public DummyPermission[] getPermissions() {

		return ArrayUtils.clone(permissions);
	}

	public boolean canManage(final DummyRole role) {

		checkNotNull(role, "role");

		switch (this) {
		case SUPERADMIN:
			return true;
		case ADMIN:
			switch (role) {
			case ADMIN:
			case SILENT_WORKER:
				return true;
			default:
				break;
			}
		case USERMGR:
			switch (role) {
			case USERMGR:
				return true;
			default:
				break;
			}
		default:
			return false;
		}
	}
}
