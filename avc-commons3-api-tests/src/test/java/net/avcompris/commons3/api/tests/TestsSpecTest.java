package net.avcompris.commons3.api.tests;

import static net.avcompris.commons3.api.tests.TestsSpec.AssertionType.EQUALS;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.GET;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.POST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.File;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.api.tests.TestsSpec.Step;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;

public class TestsSpecTest {

	@Test
	public void testApiTestSpec_001() throws Exception {

		final TestsSpec spec = TestsSpec.load(new File("src/test/yaml", "api-tests_001.yml"));

		// tests:
		//   healthcheck:
		//     authentified: false
		//     request: GET /healthcheck
		//     response: 200

		assertEquals(1, spec.getTests().length);

		final TestSpec test0 = spec.getTests()[0];

		assertEquals("healthcheck", test0.getName());
		assertEquals("healthcheck", test0.toString());
		assertNull(test0.getAuthentication());
		assertEquals(1, test0.getSteps().length);

		final Step step0 = test0.getSteps()[0];

		assertSame(GET, step0.getHttpMethod());
		assertEquals("/healthcheck", step0.getPath());
		assertEquals(0, step0.getData().length);
		assertEquals(null, step0.getReturnType());

		assertEquals(1, step0.getAssertions().length);

		assertSame(EQUALS, step0.getAssertions()[0].getType());
		assertEquals("response.status", step0.getAssertions()[0].getLeft());
		assertEquals("200", step0.getAssertions()[0].getRight());

		assertEquals(0, step0.getLets().length);
	}

	@Test
	public void testApiTestSpec_002() throws Exception {

		final TestsSpec spec = TestsSpec.load(new File("src/test/yaml", "api-tests_002.yml"), //
				DummyUserInfo.class, //
				DummyUsersInfo.class);

		assertEquals(6, spec.getTests().length);
	}

	@Test
	public void testApiTestSpec_003() throws Exception {

		final TestsSpec spec = TestsSpec.load(new File("src/test/yaml", "api-tests_003.yml"), //
				DummyUsersInfo.class);

		// tests:
		//   get_users:
		//     authentified: true
		//     request: GET /api/v1/users
		//     response:
		//       status: 200
		//       class: DummyUsersInfo

		assertEquals(1, spec.getTests().length);

		final TestSpec test0 = spec.getTests()[0];

		assertEquals("get_users", test0.getName());
		assertNotNull(test0.getAuthentication());
		assertEquals(1, test0.getSteps().length);

		final Step step0 = test0.getSteps()[0];

		assertSame(GET, step0.getHttpMethod());
		assertEquals("/api/v1/users", step0.getPath());
		assertEquals(DummyUsersInfo.class, step0.getReturnType());

		assertEquals(1, step0.getAssertions().length);

		assertSame(EQUALS, step0.getAssertions()[0].getType());
		assertEquals("response.status", step0.getAssertions()[0].getLeft());
		assertEquals("200", step0.getAssertions()[0].getRight());
	}

	@Test
	public void testApiTestSpec_004() throws Exception {

		final TestsSpec spec = TestsSpec.load(new File("src/test/yaml", "api-tests_004.yml"), //
				DummyUserInfo.class);

		// tests:
		//
		//   create_user:
		//     authentified: true
		//     request: POST /api/v1/users/toto
		//     data:
		//       role: REGULAR
		//       enabled: true
		//     response:
		//       status: 201
		//       class: DummyUserInfo
		//     assertions:
		//       - response.data.username == toto
		//       - response.data.role == REGULAR

		assertEquals(1, spec.getTests().length);

		final TestSpec test0 = spec.getTests()[0];

		assertEquals("create_user", test0.getName());
		assertNotNull(test0.getAuthentication());
		assertEquals(1, test0.getSteps().length);

		final Step step0 = test0.getSteps()[0];

		assertSame(POST, step0.getHttpMethod());
		assertEquals("/api/v1/users/toto", step0.getPath());
		assertEquals(DummyUserInfo.class, step0.getReturnType());

		assertEquals(2, step0.getData().length);

		assertEquals("role", step0.getData()[0].getName());
		assertEquals("REGULAR", step0.getData()[0].getValue());

		assertEquals("enabled", step0.getData()[1].getName());
		assertEquals("true", step0.getData()[1].getValue());

		assertEquals(3, step0.getAssertions().length);

		assertSame(EQUALS, step0.getAssertions()[0].getType());
		assertEquals("response.status", step0.getAssertions()[0].getLeft());
		assertEquals("201", step0.getAssertions()[0].getRight());

		assertSame(EQUALS, step0.getAssertions()[1].getType());
		assertEquals("response.data.username", step0.getAssertions()[1].getLeft());
		assertEquals("toto", step0.getAssertions()[1].getRight());

		assertSame(EQUALS, step0.getAssertions()[2].getType());
		assertEquals("response.data.role", step0.getAssertions()[2].getLeft());
		assertEquals("REGULAR", step0.getAssertions()[2].getRight());
	}

	@Test
	public void testApiTestSpec_005() throws Exception {

		final TestsSpec spec = TestsSpec.load(new File("src/test/yaml", "api-tests_005.yml"), //
				DummyUserInfo.class, //
				DummyUsersInfo.class);

		assertEquals(1, spec.getTests().length);

		final TestSpec test0 = spec.getTests()[0];

		assertEquals("create_user_get_users", test0.getName());
		assertNotNull(test0.getAuthentication());
		assertEquals(4, test0.getSteps().length);

		final Step step0 = test0.getSteps()[0];

		// - request: GET /api/v1/users
		//   response:
		//     status: 200
		//     class: DummyUsersInfo
		//   let:
		//     count0: response.data.total

		assertSame(GET, step0.getHttpMethod());
		assertEquals("/api/v1/users", step0.getPath());
		assertEquals(DummyUsersInfo.class, step0.getReturnType());

		assertEquals(1, step0.getAssertions().length);

		assertSame(EQUALS, step0.getAssertions()[0].getType());
		assertEquals("response.status", step0.getAssertions()[0].getLeft());
		assertEquals("200", step0.getAssertions()[0].getRight());

		assertEquals(1, step0.getLets().length);

		assertEquals("count0", step0.getLets()[0].getVariableName());
		assertEquals("response.data.total", step0.getLets()[0].getExpression());

		final Step step1 = test0.getSteps()[1];

		// - request: POST /api/v1/users/toto
		//   data:
		//     role: REGULAR
		//     enabled: true
		//   response: 201

		assertSame(POST, step1.getHttpMethod());
		assertEquals("/api/v1/users/toto", step1.getPath());
		assertEquals(null, step1.getReturnType());

		assertEquals(1, step1.getAssertions().length);

		assertSame(EQUALS, step1.getAssertions()[0].getType());
		assertEquals("response.status", step1.getAssertions()[0].getLeft());
		assertEquals("201", step1.getAssertions()[0].getRight());

		assertEquals(0, step1.getLets().length);

		final Step step2 = test0.getSteps()[2];

		// - request: GET /api/v1/users
		//   data:
		//     role: REGULAR
		//     enabled: true
		//   response: 200
		//   let:
		//     count1: response.data.total
		//   assertions:
		//     - count1 == count0 + 1

		assertSame(GET, step2.getHttpMethod());
		assertEquals("/api/v1/users", step2.getPath());
		assertEquals(null, step2.getReturnType());

		assertEquals(2, step2.getAssertions().length);

		assertSame(EQUALS, step2.getAssertions()[0].getType());
		assertEquals("response.status", step2.getAssertions()[0].getLeft());
		assertEquals("200", step2.getAssertions()[0].getRight());

		assertSame(EQUALS, step2.getAssertions()[1].getType());
		assertEquals("count1", step2.getAssertions()[1].getLeft());
		assertEquals("count0 + 1", step2.getAssertions()[1].getRight());

		assertEquals(1, step2.getLets().length);

		assertEquals("count1", step2.getLets()[0].getVariableName());
		assertEquals("response.data.total", step2.getLets()[0].getExpression());

		final Step step3 = test0.getSteps()[3];

		// - request: GET /api/v1/users/toto
		//   response:
		//     status: 200
		//     class: DummyUserInfo
		//   assertions:
		//     - response.data.username == toto
		//     - response.data.role == REGULAR

		assertSame(GET, step3.getHttpMethod());
		assertEquals("/api/v1/users/toto", step3.getPath());
		assertEquals(DummyUserInfo.class, step3.getReturnType());

		assertEquals(3, step3.getAssertions().length);

		assertSame(EQUALS, step3.getAssertions()[0].getType());
		assertEquals("response.status", step2.getAssertions()[0].getLeft());
		assertEquals("200", step3.getAssertions()[0].getRight());

		assertSame(EQUALS, step3.getAssertions()[1].getType());
		assertEquals("response.data.username", step3.getAssertions()[1].getLeft());
		assertEquals("toto", step3.getAssertions()[1].getRight());

		assertSame(EQUALS, step3.getAssertions()[2].getType());
		assertEquals("response.data.role", step3.getAssertions()[2].getLeft());
		assertEquals("REGULAR", step3.getAssertions()[2].getRight());

		assertEquals(0, step3.getLets().length);
	}

	@Test
	public void testApiTestSpec_006() throws Exception {

		final TestsSpec spec = TestsSpec.load(new File("src/test/yaml", "api-tests_006.yml"), //
				DummyUserInfo.class);

		// tests:
		//
		//   create_user:
		//     authentified: true
		//     request: POST /api/v1/users/$username
		//     let:
		//       username: $random_alpha(20)
		//     data:
		//       role: REGULAR
		//       enabled: true
		//     response:
		//       status: 201
		//       class: DummyUserInfo
		//     assertions:
		//       - response.data.username == $username
		//       - response.data.role == REGULAR

		assertEquals(1, spec.getTests().length);

		final TestSpec test0 = spec.getTests()[0];

		assertEquals("create_user", test0.getName());
		assertNotNull(test0.getAuthentication());
		assertEquals(1, test0.getSteps().length);

		final Step step0 = test0.getSteps()[0];

		assertEquals(1, step0.getLets().length);

		assertEquals("username", step0.getLets()[0].getVariableName());
		assertEquals("$random_alpha(20)", step0.getLets()[0].getExpression());

		assertSame(POST, step0.getHttpMethod());
		assertEquals("/api/v1/users/$username", step0.getPath());
		assertEquals(DummyUserInfo.class, step0.getReturnType());

		assertEquals(2, step0.getData().length);

		assertEquals("role", step0.getData()[0].getName());
		assertEquals("REGULAR", step0.getData()[0].getValue());

		assertEquals("enabled", step0.getData()[1].getName());
		assertEquals("true", step0.getData()[1].getValue());

		assertEquals(3, step0.getAssertions().length);

		assertSame(EQUALS, step0.getAssertions()[0].getType());
		assertEquals("response.status", step0.getAssertions()[0].getLeft());
		assertEquals("201", step0.getAssertions()[0].getRight());

		assertSame(EQUALS, step0.getAssertions()[1].getType());
		assertEquals("response.data.username", step0.getAssertions()[1].getLeft());
		assertEquals("$username", step0.getAssertions()[1].getRight());

		assertSame(EQUALS, step0.getAssertions()[2].getType());
		assertEquals("response.data.role", step0.getAssertions()[2].getLeft());
		assertEquals("REGULAR", step0.getAssertions()[2].getRight());
	}
}