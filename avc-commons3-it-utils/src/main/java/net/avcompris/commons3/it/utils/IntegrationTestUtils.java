package net.avcompris.commons3.it.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.join;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class IntegrationTestUtils {

	private static final String[] RESOURCE_PATHS = { "env.properties", //
			"test.properties", //
	};

	/**
	 * Retrieve a <em>processed</em> property from the local "env.properties" file.
	 */
	public static String getTestProperty(final String propertyName) throws IOException {

		checkNotNull(propertyName, "propertyName");

		final Properties properties = new Properties();

		try (InputStream is = getTestResourceAsStream()) {

			properties.load(is);
		}

		final String value = properties.getProperty(propertyName);

		if (isBlank(value)) {

			throw new IOException("value shoud not be blank for property: " + propertyName + ": " + value);

		} else if (value.startsWith("$") || value.contains("{")) {

			throw new IllegalStateException(
					"Value has not been processed for property: " + propertyName + ": " + value);
		}

		return value;
	}

	/**
	 * Retrieve a <em>processed</em> property from the local "env.properties" file.
	 */
	public static int getIntTestProperty(final String propertyName) throws IOException {

		final String valueAsString = getTestProperty(propertyName);

		try {

			return Integer.parseInt(valueAsString);

		} catch (final NumberFormatException e) {

			throw new IOException("Cannot parse value to int for property: " + propertyName + ": " + valueAsString);
		}
	}

	private static InputStream getTestResourceAsStream() throws IOException {

		for (final String resourcePath : RESOURCE_PATHS) {

			final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath);

			if (is != null) {

				System.out.println("Using: " + resourcePath);

				return is;
			}
		}

		throw new FileNotFoundException("Cannot find resource in: " + join(RESOURCE_PATHS, ", "));
	}

	public static File getFilteredFile(final String fileName) throws IOException {

		checkNotNull(fileName, "fileName");

		final File[] files = new File[] { //
				new File("target/classes", fileName), //
				new File("target/test-classes", fileName), //
		};

		for (final File file : files) {

			if (file.isFile()) {

				System.out.println("Using: " + file.getCanonicalPath());

				return file;
			}
		}

		final String[] filePaths = new String[files.length];

		for (int i = 0; i < files.length; ++i) {

			filePaths[i] = files[i].getCanonicalPath();
		}

		throw new FileNotFoundException(join(filePaths, ", or: "));
	}
}
