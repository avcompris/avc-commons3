package net.avcompris.commons3.api.exception;

import java.util.ConcurrentModificationException;

import javax.annotation.Nullable;

/**
 * This exception is thrown when a resource update could not take place because
 * the "fromRevision" field specified in the update didn’t match the actual
 * "revision" field in the database.
 *
 * @author dandriana
 */
public final class IllegalStateUpdateException extends ServiceException {

	private static final long serialVersionUID = -2892863391217283996L;

	public IllegalStateUpdateException(@Nullable final String message) {

		super(409, message);
	}

	public IllegalStateUpdateException(@Nullable final String message, @Nullable final Throwable cause) {

		super(409, message, cause);
	}

	public IllegalStateUpdateException(@Nullable final ConcurrentModificationException cause) {

		super(409, "The resource has been changed already.", cause);
	}
}
