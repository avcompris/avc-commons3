package net.avcompris.commons3.api.exception;

public final class UnauthenticatedException extends ServiceException {

	private static final long serialVersionUID = -7985256380863584485L;

	public UnauthenticatedException() {

		super(401, null);
	}
}
