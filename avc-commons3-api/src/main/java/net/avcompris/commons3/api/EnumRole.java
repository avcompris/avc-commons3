package net.avcompris.commons3.api;

public interface EnumRole {

	String name();

	String getRolename();

	EnumRole[] getSuperRoles();

	EnumPermission[] getPermissions();
}
