package net.avcompris.commons3.api;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;

public interface EntitiesQueryRaw<T extends Filtering<U>, U extends Filtering.Field> extends EntitiesQuery<T, U> {

	@Nullable
	String getQ();

	@Nullable
	String getSort();

	@Nullable
	String getExpand();

	EntitiesQueryRaw<T, U> setQ(@Nullable String q);

	EntitiesQueryRaw<T, U> setSort(@Nullable String sort);

	EntitiesQueryRaw<T, U> setExpand(@Nullable String expand);;

	EntitiesQueryRaw<T, U> setStart(@Nullable Integer start);

	EntitiesQueryRaw<T, U> setLimit(@Nullable Integer limit);
}
