package net.avcompris.commons3.api;

import javax.annotation.Nullable;

public interface Entities<T extends Entity> {

	T[] getResults();

	int getStart();

	int getLimit();

	@Nullable
	String getSqlWhereClause();

	int getSize();

	int getTotal();

	int getTookMs();
}
