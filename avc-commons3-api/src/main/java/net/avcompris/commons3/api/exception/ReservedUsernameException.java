package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class ReservedUsernameException extends ServiceException {

	private static final long serialVersionUID = 2076945753057338090L;

	public ReservedUsernameException(@Nullable final String username) {

		super(409, "Username is reserved: " + username);
	}
}
