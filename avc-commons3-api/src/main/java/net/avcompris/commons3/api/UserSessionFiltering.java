package net.avcompris.commons3.api;

import org.joda.time.DateTime;

import net.avcompris.commons.query.Filtering;

public interface UserSessionFiltering extends Filtering<UserSessionFiltering.Field> {

	enum Field implements Filtering.Field {

		@Spec(type = String.class, alias = "user")
		USERNAME,

		@Spec(type = String.class)
		USER_SESSION_ID,

		@Spec(type = DateTime.class)
		CREATED_AT,

		@Spec(type = DateTime.class)
		UPDATED_AT,

		@Spec(type = DateTime.class)
		EXPIRES_AT,

		@Spec(type = DateTime.class)
		EXPIRED_AT,
	}
}
