package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class InvalidPasswordException extends ServiceException {

	private static final long serialVersionUID = -4965332273642783777L;

	public InvalidPasswordException(@Nullable final String description) {

		super(422, "Password is invalid." + (description != null ? " " + description : ""));
	}
}
