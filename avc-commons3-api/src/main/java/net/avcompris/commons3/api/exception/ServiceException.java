package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public abstract class ServiceException extends Exception {

	private static final long serialVersionUID = 4710950710702281780L;

	private final int httpErorCode;

	@Nullable
	private final String description;

	protected ServiceException(final int httpErorCode, @Nullable final String description) {

		this(httpErorCode, description, null);
	}

	protected ServiceException(final int httpErorCode, @Nullable final String description,
			@Nullable final Throwable cause) {

		super(httpErorCode + ": " + description, cause);

		this.httpErorCode = httpErorCode;
		this.description = description;
	}

	public final int getHttpErrorCode() {

		return httpErorCode;
	}

	@Nullable
	public final String getDescription() {

		return description;
	}
}
