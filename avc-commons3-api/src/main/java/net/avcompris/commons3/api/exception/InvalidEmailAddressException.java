package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class InvalidEmailAddressException extends ServiceException {

	private static final long serialVersionUID = -1488505458198373119L;

	public InvalidEmailAddressException(@Nullable final String description) {

		super(422, "Email is invalid." + (description != null ? " " + description : ""));
	}
}
