package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class InvalidUsernameException extends ServiceException {

	private static final long serialVersionUID = 1822094327025236336L;

	public InvalidUsernameException(@Nullable final String description) {

		super(422, "Username is invalid." + (description != null ? " " + description : ""));
	}
}
