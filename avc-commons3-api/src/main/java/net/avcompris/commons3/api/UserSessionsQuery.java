package net.avcompris.commons3.api;

import javax.annotation.Nullable;

public interface UserSessionsQuery extends EntitiesQuery<UserSessionFiltering, UserSessionFiltering.Field> {

	@Override
	SortBy[] getSortBys();

	@Override
	Expand[] getExpands();

	UserSessionsQuery setFiltering(@Nullable UserSessionFiltering filtering);

	UserSessionsQuery setSortBys(SortBy... sortBys);

	UserSessionsQuery setExpands(Expand... expands);

	enum SortBy {

		SORT_BY_USERNAME, SORT_BY_USERNAME_DESC, //
		SORT_BY_USER_SESSION_ID, SORT_BY_USER_SESSION_ID_DESC, //
		SORT_BY_CREATED_AT, SORT_BY_CREATED_AT_DESC, //
		SORT_BY_UPDATED_AT, SORT_BY_UPDATED_AT_DESC, //
		SORT_BY_EXPIRES_AT, SORT_BY_EXPIRES_AT_DESC, //
		SORT_BY_EXPIRED_AT, SORT_BY_EXPIRED_AT_DESC, //
	}

	interface Expand {

	}

	UserSessionsQuery setStart(@Nullable Integer start);

	UserSessionsQuery setLimit(@Nullable Integer limit);
}
