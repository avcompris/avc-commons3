package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public class UnknownPasswordResetTokenException extends ServiceException {

	private static final long serialVersionUID = -1454624899382532876L;

	public UnknownPasswordResetTokenException(@Nullable final Throwable cause) {

		super(404, "Unknown token", cause);
	}
}
