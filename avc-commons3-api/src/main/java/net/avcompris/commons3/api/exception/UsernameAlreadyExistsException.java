package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class UsernameAlreadyExistsException extends ServiceException {

	private static final long serialVersionUID = -8823526456548125731L;

	public UsernameAlreadyExistsException(@Nullable final String username) {

		super(409, "Username already exists in the database: " + username);
	}

	public UsernameAlreadyExistsException(@Nullable final String username, @Nullable final Throwable cause) {

		super(409, "Username already exists in the database: " + username, cause);
	}
}
