package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class UnauthorizedException extends ServiceException {

	private static final long serialVersionUID = 7379077089338160687L;

	public UnauthorizedException(@Nullable final String message) {

		super(403, message);
	}
}
