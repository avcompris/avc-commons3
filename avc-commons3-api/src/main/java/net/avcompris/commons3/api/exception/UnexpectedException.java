package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class UnexpectedException extends ServiceException {

	private static final long serialVersionUID = 2546966591177146034L;

	public UnexpectedException(@Nullable final String message) {

		super(500, message);
	}

	public UnexpectedException(@Nullable final String message, @Nullable final Throwable cause) {

		super(500, message, cause);
	}

	public UnexpectedException(@Nullable final Throwable cause) {

		super(500, null, cause);
	}
}
