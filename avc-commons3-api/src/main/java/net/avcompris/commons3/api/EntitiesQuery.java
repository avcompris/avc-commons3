package net.avcompris.commons3.api;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;

public interface EntitiesQuery<T extends Filtering<U>, U extends Filtering.Field> {

	@Nullable
	T getFiltering();

	Object[] getSortBys();

	Object[] getExpands();

	@Nullable
	Integer getStart();

	@Nullable
	Integer getLimit();

	interface EnumSortBy {

		String name();
	}
}
