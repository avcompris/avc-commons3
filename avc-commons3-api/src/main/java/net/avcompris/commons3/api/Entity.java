package net.avcompris.commons3.api;

import net.avcompris.commons3.types.DateTimeHolder;

public interface Entity {

	int getRevision();

	DateTimeHolder getCreatedAt();

	DateTimeHolder getUpdatedAt();
}
