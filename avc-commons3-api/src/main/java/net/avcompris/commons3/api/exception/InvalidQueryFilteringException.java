package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class InvalidQueryFilteringException extends ServiceException {

	private static final long serialVersionUID = 6944861497107286001L;

	public InvalidQueryFilteringException(@Nullable final String description) {

		super(422, "Query filtering is invalid." + (description != null ? " " + description : ""));
	}

	public InvalidQueryFilteringException(@Nullable final Throwable cause) {

		super(422, "Query filtering is invalid.", cause);
	}
}
