package net.avcompris.commons3.api;

import javax.annotation.Nullable;

public interface EnumPermission {

	String name();

	@Nullable
	String getExpression();

	boolean isAnyUserPermission();

	boolean isSuperadminPermission();
}
