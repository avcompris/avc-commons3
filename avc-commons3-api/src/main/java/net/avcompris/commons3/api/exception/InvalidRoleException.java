package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class InvalidRoleException extends ServiceException {

	private static final long serialVersionUID = -7866596130792729423L;

	public InvalidRoleException(@Nullable final String description) {

		super(422, "Role is invalid." + (description != null ? " " + description : ""));
	}
}
