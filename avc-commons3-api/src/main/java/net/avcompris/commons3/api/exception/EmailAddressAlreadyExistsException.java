package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class EmailAddressAlreadyExistsException extends ServiceException {

	private static final long serialVersionUID = 7660769768476033516L;

	public EmailAddressAlreadyExistsException(@Nullable final String emailAddress) {

		super(409, "Email already exists in the database: " + emailAddress);
	}

	public EmailAddressAlreadyExistsException(@Nullable final String emailAddress, @Nullable final Throwable cause) {

		super(409, "Email already exists in the database: " + emailAddress, cause);
	}
}
