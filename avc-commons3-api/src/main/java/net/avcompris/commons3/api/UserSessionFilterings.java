package net.avcompris.commons3.api;

import org.joda.time.DateTime;

import net.avcompris.commons.query.Filterings;

public interface UserSessionFilterings extends Filterings<UserSessionFiltering, UserSessionFiltering.Field> {

	// ======== STRING COMPARISONS

	@Override
	UserSessionFiltering eq(UserSessionFiltering.Field field, String s);

	@Override
	UserSessionFiltering neq(UserSessionFiltering.Field field, String s);

	@Override
	UserSessionFiltering contains(UserSessionFiltering.Field field, String s);

	@Override
	UserSessionFiltering doesntContain(UserSessionFiltering.Field field, String s);

	// ======== INT COMPARISONS

	@Override
	UserSessionFiltering eq(UserSessionFiltering.Field field, int n);

	@Override
	UserSessionFiltering neq(UserSessionFiltering.Field field, int n);

	@Override
	UserSessionFiltering gt(UserSessionFiltering.Field field, int n);

	@Override
	UserSessionFiltering gte(UserSessionFiltering.Field field, int n);

	@Override
	UserSessionFiltering lt(UserSessionFiltering.Field field, int n);

	@Override
	UserSessionFiltering lte(UserSessionFiltering.Field field, int n);

	// ======== BOOLEAN COMPARISONS

	@Override
	UserSessionFiltering eq(UserSessionFiltering.Field field, boolean b);

	@Override
	UserSessionFiltering neq(UserSessionFiltering.Field field, boolean b);

	// ======== DATETIME COMPARISONS

	@Override
	UserSessionFiltering eq(UserSessionFiltering.Field field, DateTime dateTime);

	@Override
	UserSessionFiltering gt(UserSessionFiltering.Field field, DateTime dateTime);

	@Override
	UserSessionFiltering gte(UserSessionFiltering.Field field, DateTime dateTime);

	@Override
	UserSessionFiltering lt(UserSessionFiltering.Field field, DateTime dateTime);

	@Override
	UserSessionFiltering lte(UserSessionFiltering.Field field, DateTime dateTime);
}
