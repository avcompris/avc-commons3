package net.avcompris.commons3.api;

public interface User {

	String getUsername();

	EnumRole getRole();
}
