package net.avcompris.commons3.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.types.DateTimeHolder;

public interface UserSession {

	String getUserSessionId();

	String getUsername();

	DateTimeHolder getCreatedAt();

	DateTimeHolder getUpdatedAt();

	DateTimeHolder getExpiresAt();

	@Nullable
	DateTimeHolder getExpiredAt();
}
