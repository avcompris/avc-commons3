package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class DaoException extends ServiceException {

	private static final long serialVersionUID = -1153777170198241104L;

	public DaoException(@Nullable final Throwable cause) {

		this(null, cause);
	}

	public DaoException(@Nullable final String description, @Nullable final Throwable cause) {

		super(500, description, cause);

		if (cause != null) {
			cause.printStackTrace();
		}
	}
}
