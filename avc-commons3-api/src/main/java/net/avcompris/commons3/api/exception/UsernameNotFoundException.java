package net.avcompris.commons3.api.exception;

import javax.annotation.Nullable;

public final class UsernameNotFoundException extends ServiceException {

	private static final long serialVersionUID = 3850340613551420259L;

	public UsernameNotFoundException(@Nullable final String username) {

		super(404, "Username cannot be found in the database: " + username);
	}
}
