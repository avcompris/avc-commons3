package net.avcompris.commons3.api;

import javax.annotation.Nullable;

public interface UserSessions {

	UserSession[] getResults();

	int getStart();

	int getLimit();

	@Nullable
	String getSqlWhereClause();

	int getSize();

	int getTotal();

	int getTookMs();
}
