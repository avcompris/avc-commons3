package net.avcompris.commons3.api;

public interface EntityUpdate {

	int getFromRevision();

	EntityUpdate setFromRevision(int revision);
}
