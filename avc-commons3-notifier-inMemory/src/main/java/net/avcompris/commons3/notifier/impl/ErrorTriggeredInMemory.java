package net.avcompris.commons3.notifier.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.notifier.utils.NotificationUtils;
import net.avcompris.commons3.triggered.ErrorTriggeredAction;
import net.avcompris.commons3.triggered.impl.AbstractErrorTriggeredImpl;

public final class ErrorTriggeredInMemory extends AbstractErrorTriggeredImpl {

	private final String name;

	@Nullable
	private User threadUser;

	public ErrorTriggeredInMemory(final String name) {

		checkNotNull(name, "name");

		this.name = name;

		ErrorNotifierInMemory.getInstance().register(this);
	}

	public ErrorTriggeredInMemory(final Class<?> clazz) {

		this(checkNotNull(clazz, "clazz").getSimpleName());
	}

	@Override
	public String getName() {

		return name;
	}

	void trigger(@Nullable final String correlationId, @Nullable final String username, final Throwable throwable)
			throws ServiceException {

		checkNotNull(throwable, "throwable");

		final User threadUserCopy = threadUser;

		if (threadUserCopy != null && !threadUserCopy.getUsername().contentEquals(username)) {

			throw new NotImplementedException("user: " + username + ", threadUser: " + threadUserCopy.getUsername());
		}

		final ErrorTriggeredAction action = getAction();

		if (action == null) {

			return;
		}

		action.execute(correlationId, username, NotificationUtils.toErrorNotification(throwable));
	}

	@Override
	@Nullable
	public User getThreadUser() {

		return threadUser;
	}

	@Override
	public void setThreadUser(@Nullable final String correlationId, @Nullable final User user) {

		threadUser = user;
	}
}
