package net.avcompris.commons3.notifier.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.logging.Log;
import org.springframework.stereotype.Component;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.notifier.ErrorNotifier;
import net.avcompris.commons3.triggered.ErrorTriggered;
import net.avcompris.commons3.utils.LogFactory;

@Component
public final class ErrorNotifierInMemory implements ErrorNotifier {

	private static final Log logger = LogFactory.getLog(ErrorNotifierInMemory.class);

	private static ErrorNotifierInMemory INSTANCE;

	private final List<ErrorTriggered> allTriggered = new ArrayList<>();

	private ErrorNotifierInMemory() {

	}

	public static ErrorNotifierInMemory getInstance() {

		if (INSTANCE == null) {

			loadInstance();
		}

		return INSTANCE;
	}

	private static synchronized void loadInstance() {

		if (INSTANCE != null) {
			return;
		}

		INSTANCE = new ErrorNotifierInMemory();
	}

	@Override
	public void notifyError(@Nullable final String correlationId, @Nullable final String username,
			final Throwable throwable) {

		checkNotNull(throwable, "throwable");

		for (final ErrorTriggered triggered : allTriggered) {

			new Thread() {

				@Override
				public void run() {

					try {

						((ErrorTriggeredInMemory) triggered).trigger(correlationId, username, throwable);

					} catch (final ServiceException | RuntimeException | Error e) {

						logger.error("Triggered: " + triggered.getName(), e);
					}
				}

			}.start();
		}
	}

	void register(final ErrorTriggered triggered) {

		checkNotNull(triggered, "triggered");

		allTriggered.add(triggered);
	}
}
