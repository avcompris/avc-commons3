package net.avcompris.commons3.utils;

import static org.joda.time.DateTimeZone.UTC;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

public abstract class LogUtils {

	public static String format( //
			@Nullable final String correlationId, //
			@Nullable final Level level, //
			@Nullable final String message) {

		final DateTime now = new DateTime().withZone(UTC);

		return now //
				+ " " + (level == null ? null : level.paddedLabel()) //
				+ " [" + correlationId + "] " //
				+ message;
	}
}
