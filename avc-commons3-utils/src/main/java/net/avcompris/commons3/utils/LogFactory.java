package net.avcompris.commons3.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import javax.annotation.Nullable;

import org.apache.commons.logging.Log;

public abstract class LogFactory {

	private static final ThreadLocal<String> CORRELATION_IDS = new ThreadLocal<>();

	private static Level level = Level.INFO;

	public static Log getLog(final Class<?> clazz) {

		return new LogImpl(clazz);
	}

	public static void resetCorrelationId() {

		CORRELATION_IDS.remove();
	}

	public static void setCorrelationId(final String correlationId) {

		checkNotNull(correlationId, "correlationId");

		final String oldCorrelationId = CORRELATION_IDS.get();

		checkState(oldCorrelationId == null || correlationId.contentEquals(oldCorrelationId), //
				"correlationId: Expected: %s, but was: %s", oldCorrelationId, correlationId);

		CORRELATION_IDS.set(correlationId);
	}

	private static String getCorrelationId() {

		final String correlationId = CORRELATION_IDS.get();

		if (correlationId == null) {

			final RuntimeException e = new IllegalStateException("correlationId should have been set");

			e.printStackTrace();

			// e.g "C-1566876463041-n2Lb6lKrl7Mc94PH3Itl";

			return "????????????????????????????????????";
		}

		return correlationId;
	}

	private static class LogImpl implements Log {

		private final String prefix;

		LogImpl(final Class<?> clazz) {

			checkNotNull(clazz, "class");

			prefix = clazz.getSimpleName() + ": ";
		}

		@Override
		public void trace(@Nullable final Object message) {

			if (!isTraceEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.TRACE, prefix + message));
		}

		@Override
		public void trace(@Nullable final Object message, @Nullable final Throwable t) {

			if (!isTraceEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.TRACE, prefix + message));

			if (t != null) {

				t.printStackTrace(System.out);
			}
		}

		@Override
		public void debug(@Nullable final Object message) {

			if (!isDebugEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.DEBUG, prefix + message));
		}

		@Override
		public void debug(@Nullable final Object message, @Nullable final Throwable t) {

			if (!isDebugEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.DEBUG, prefix + message));

			if (t != null) {

				t.printStackTrace(System.out);
			}
		}

		@Override
		public void info(@Nullable final Object message) {

			if (!isInfoEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.INFO, prefix + message));
		}

		@Override
		public void info(@Nullable final Object message, @Nullable final Throwable t) {

			if (!isInfoEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.INFO, prefix + message));

			if (t != null) {

				t.printStackTrace(System.out);
			}
		}

		@Override
		public void warn(@Nullable final Object message) {

			if (!isWarnEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.WARN, prefix + message));
		}

		@Override
		public void warn(@Nullable final Object message, @Nullable final Throwable t) {

			if (!isWarnEnabled()) {
				return;
			}

			System.out.println(LogUtils.format(getCorrelationId(), Level.WARN, prefix + message));

			if (t != null) {

				t.printStackTrace(System.out);
			}
		}

		@Override
		public void error(@Nullable final Object message) {

			if (!isErrorEnabled()) {
				return;
			}

			System.err.println(LogUtils.format(getCorrelationId(), Level.ERROR, prefix + message));
		}

		@Override
		public void error(@Nullable final Object message, @Nullable final Throwable t) {

			if (!isErrorEnabled()) {
				return;
			}

			System.err.println(LogUtils.format(getCorrelationId(), Level.ERROR, prefix + message + ": " + t));

			if (t != null) {

				t.printStackTrace(System.err);
			}
		}

		@Override
		public void fatal(@Nullable final Object message) {

			if (!isFatalEnabled()) {
				return;
			}

			System.err.println(LogUtils.format(getCorrelationId(), Level.FATAL, prefix + message));
		}

		@Override
		public void fatal(@Nullable final Object message, @Nullable final Throwable t) {

			if (!isFatalEnabled()) {
				return;
			}

			System.err.println(LogUtils.format(getCorrelationId(), Level.FATAL, prefix + message + ": " + t));

			if (t != null) {

				t.printStackTrace(System.err);
			}
		}

		@Override
		public boolean isDebugEnabled() {

			return !getLevel().isGreaterThan(Level.DEBUG);
		}

		@Override
		public boolean isErrorEnabled() {

			return !getLevel().isGreaterThan(Level.ERROR);
		}

		@Override
		public boolean isFatalEnabled() {

			return !getLevel().isGreaterThan(Level.FATAL);
		}

		@Override
		public boolean isInfoEnabled() {

			return !getLevel().isGreaterThan(Level.INFO);
		}

		@Override
		public boolean isTraceEnabled() {

			return !getLevel().isGreaterThan(Level.TRACE);
		}

		@Override
		public boolean isWarnEnabled() {

			return !getLevel().isGreaterThan(Level.WARN);
		}

		private Level getLevel() {

			return LogFactory.getLevel();
		}
	}

	public static void setLevel(final Level level) {

		LogFactory.level = checkNotNull(level, "level");
	}

	private static Level getLevel() {

		return level;
	}
}
