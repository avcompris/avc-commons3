package net.avcompris.commons3.utils;

import static com.google.common.base.Preconditions.checkNotNull;

public enum Level {

	TRACE(0, "TRACE"), //
	DEBUG(1, "DEBUG"), //
	INFO(2, "INFO "), //
	WARN(3, "WARN "), //
	ERROR(4, "ERROR"), //
	FATAL(5, "FATAL");

	private final int value;
	private final String paddedLabel;

	Level(final int value, final String paddedLabel) {

		this.value = value;
		this.paddedLabel = checkNotNull(paddedLabel, "paddedLabel");
	}

	public int intValue() {

		return value;
	}

	public String paddedLabel() {

		return paddedLabel;
	}

	public boolean isGreaterThan(final Level level) {

		checkNotNull(level, "level");

		return value > level.value;
	}
}
