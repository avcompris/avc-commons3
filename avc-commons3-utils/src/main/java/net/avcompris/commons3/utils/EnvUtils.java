package net.avcompris.commons3.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class EnvUtils {

	public static String getEnvProperty(final String propertyName, final String sampleValue) {

		checkNotNull(propertyName, "propertyName");
		checkNotNull(sampleValue, "sampleValue");

		final String propertyValue = System.getProperty(propertyName);

		if (isBlank(propertyValue)) {

			throw new RuntimeException("Cannot read environment property: " + propertyName
					+ ", it should be of the form: \"" + sampleValue + "\", but was: " + propertyValue);
		}

		return propertyValue;
	}
}
