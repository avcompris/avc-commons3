package net.avcompris.commons3.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.stereotype.Component;

@Component
public final class ClockImpl implements Clock {

	/**
	 * This method uses Joda Time to return the current DateTime, always using UTC
	 * TimeZone.
	 *
	 * @return the current DateTime, with UTC TimeZone
	 */
	@Override
	public DateTime now() {

		return new DateTime().withZone(DateTimeZone.UTC);
	}

	/**
	 * This method uses Joda Time to return the current UNIX time in milliseconds.
	 *
	 * @return the current UNIX time, in milliseconds
	 */
	@Override
	public long nowMs() {

		return new DateTime().getMillis();
	}
}
