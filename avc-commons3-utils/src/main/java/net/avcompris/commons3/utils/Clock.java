package net.avcompris.commons3.utils;

import org.joda.time.DateTime;

/**
 * We use a “Clock” Bean so we can run tests against session timeouts.
 *
 * @author dandriana
 */
public interface Clock {

	/**
	 * Please serve only UTC DatetTime values
	 */
	DateTime now();

	long nowMs();
}
