package net.avcompris.commons3.utils;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class LogUtilsTest {

	@Test
	public void test_notNulls() {

		final String s = LogUtils.format("C-012", Level.INFO, "Hello, World!");

		assertTrue(s.endsWith(" INFO  [C-012] Hello, World!"));
	}

	@Test
	public void test_nulls() {

		final String s = LogUtils.format(null, null, null);

		assertTrue(s.endsWith(" null [null] null"));
	}
}
