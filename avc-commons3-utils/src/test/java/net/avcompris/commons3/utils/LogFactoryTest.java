package net.avcompris.commons3.utils;

import org.apache.commons.logging.Log;
import org.junit.jupiter.api.Test;

public class LogFactoryTest {

	private static final Log logger = LogFactory.getLog(LogFactoryTest.class);

	@Test
	public void test_logWithoutCorrelationId_TRACE() {

		LogFactory.resetCorrelationId();

		LogFactory.setLevel(Level.TRACE);

		logger.trace("WITHOUT CORRELATION-ID");
	}

	@Test
	public void test_logWithoutCorrelationId_DEBUG() {

		LogFactory.resetCorrelationId();

		LogFactory.setLevel(Level.DEBUG);

		logger.debug("WITHOUT CORRELATION-ID");
	}

	@Test
	public void test_logWithoutCorrelationId_INFO() {

		LogFactory.resetCorrelationId();

		LogFactory.setLevel(Level.INFO);

		logger.info("WITHOUT CORRELATION-ID");
	}

	@Test
	public void test_logWithoutCorrelationId_WARN() {

		LogFactory.resetCorrelationId();

		LogFactory.setLevel(Level.WARN);

		logger.warn("WITHOUT CORRELATION-ID");
	}

	@Test
	public void test_logWithoutCorrelationId_ERROR() {

		LogFactory.resetCorrelationId();

		LogFactory.setLevel(Level.ERROR);

		logger.error("WITHOUT CORRELATION-ID");
	}

	@Test
	public void test_logWithoutCorrelationId_FATAL() {

		LogFactory.resetCorrelationId();

		LogFactory.setLevel(Level.FATAL);

		logger.fatal("WITHOUT CORRELATION-ID");
	}

	@Test
	public void test_logWithCorrelationId() {

		LogFactory.resetCorrelationId();

		LogFactory.setCorrelationId("blah-blah-c-id");

		logger.info("WITH CORRELATION-ID");
	}
}
