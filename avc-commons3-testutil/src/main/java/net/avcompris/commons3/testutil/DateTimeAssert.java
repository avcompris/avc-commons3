package net.avcompris.commons3.testutil;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.TimeZone;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

public abstract class DateTimeAssert {

	public static void assertDateTimeEquals(
		@Nullable final DateTime ref,
		@Nullable final DateTime test
	) {

		if (ref != null && test != null //
				&& ref.getMillis() == test.getMillis()) {

			final TimeZone refTimeZone = ref.getZone().toTimeZone();
			final TimeZone testTimeZone = test.getZone().toTimeZone();

			if (refTimeZone.getRawOffset() + refTimeZone.getDSTSavings() //
					== testTimeZone.getRawOffset() + testTimeZone.getDSTSavings()) {

				// OK
				//
				return;
			}
		}

		// fail
		//
		assertEquals(ref, test);
	}
}
