package net.avcompris.commons3.testutil;

import static com.google.common.base.Preconditions.checkNotNull;

public final class Four<T, U, V, W> {

	private final T left;
	private final U middleLeft;
	private final V middleRight;
	private final W right;

	private Four(final T left, final U middleLeft, final V middleRight, final W right) {

		this.left = checkNotNull(left, "left");
		this.middleLeft = checkNotNull(middleLeft, "middleLeft");
		this.middleRight = checkNotNull(middleRight, "middleRight");
		this.right = checkNotNull(right, "right");
	}

	public T getLeft() {

		return left;
	}

	public U getMiddleLeft() {

		return middleLeft;
	}

	public V getMiddleRight() {

		return middleRight;
	}

	public W getRight() {

		return right;
	}

	public static <T, U, V, W> Four<T, U, V, W> of(final T left, final U middleLeft, final V middleRight,
			final W right) {

		return new Four<T, U, V, W>(left, middleLeft, middleRight, right);
	}
}
