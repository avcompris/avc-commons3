package net.avcompris.commons3.testutil;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.join;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DynamicContainer;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;

public abstract class TestUtils {

	public static void assertFileContentEquals(final File refFile, final File testFile) throws IOException {

		final List<String> refLines = FileUtils.readLines(refFile, UTF_8);
		final List<String> testLines = FileUtils.readLines(testFile, UTF_8);

		for (int i = 0; i < refLines.size() || i < testLines.size(); ++i) {

			final String refLine = refLines.get(i);
			final String testLine = testLines.get(i);

			assertEquals(refLine, testLine, //
					"Line #" + (i + 1));
		}
	}

	public static InputStream getResourceAsStream(final String resourcePath) throws IOException {

		final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath);

		if (is == null) {
			throw new FileNotFoundException("resourcePath: " + resourcePath);
		}

		return is;
	}

	public static byte[] readResource(final String resourcePath) throws IOException {

		checkNotNull(resourcePath, "resourcePath");

		final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath);

		if (is == null) {
			throw new FileNotFoundException("resourcePath: " + resourcePath);
		}

		try {

			return IOUtils.toByteArray(is);

		} finally {
			is.close();
		}
	}

	public static void assertContains(final String ref, final String[] array) {

		assertTrue(ArrayUtils.contains(array, ref), //
				"Array should countain: " + ref + ", but was: " + join(array, ", "));
	}

	public static void assertStartsWith(final String ref, final String s) {

		assertTrue(s.startsWith(ref), //
				"String should start with: " + ref + ", but was: " + s);
	}

	public static DynamicContainer dynamicContainer(final Object instance) {

		checkNotNull(instance, "instance");

		final List<DynamicTest> tests = newArrayList();

		final Class<? extends Object> clazz = instance.getClass();

		for (final Method method : clazz.getMethods()) {

			if (!method.isAnnotationPresent(Test.class)) {
				continue;
			}

			if (method.isAnnotationPresent(Disabled.class)) {
				continue;
			}

			method.setAccessible(true);

			tests.add(DynamicTest.dynamicTest(method.getName(), () -> {

				try {

					method.invoke(instance);

				} catch (final InvocationTargetException e) {

					throw e.getTargetException();
				}

			}));
		}

		return DynamicContainer.dynamicContainer(instance.toString(), tests);
	}
}
