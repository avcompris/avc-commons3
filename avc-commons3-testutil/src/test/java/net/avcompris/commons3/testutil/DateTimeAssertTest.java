package net.avcompris.commons3.testutil;

import static net.avcompris.commons3.testutil.DateTimeAssert.assertDateTimeEquals;
import static org.joda.time.DateTimeZone.UTC;
import static org.joda.time.DateTimeZone.forOffsetHours;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

public class DateTimeAssertTest {

	@Test
	public void testDateTimeAssert() throws Exception {

		final DateTime now = new DateTime();

		assertDateTimeEquals(now, now);

		assertThrows(AssertionFailedError.class, ()

		-> assertDateTimeEquals( //
				now.withZone(UTC), //
				now.withZone(forOffsetHours(3))));
	}
}
