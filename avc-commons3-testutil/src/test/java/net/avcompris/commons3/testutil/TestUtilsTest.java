package net.avcompris.commons3.testutil;

import static net.avcompris.commons3.testutil.TestUtils.assertFileContentEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;

import org.junit.jupiter.api.Test;

public class TestUtilsTest {

	@Test
	public void testAssertFileContentEquals_sameFile() throws Exception {

		assertFileContentEquals(new File("src/test/files", "a"), new File("src/test/files", "a"));
	}

	@Test
	public void testAssertFileContentEquals_differentFile() throws Exception {

		assertThrows(AssertionError.class, ()

		-> assertFileContentEquals(new File("src/test/files", "a"), new File("src/test/files", "b")));
	}
}
