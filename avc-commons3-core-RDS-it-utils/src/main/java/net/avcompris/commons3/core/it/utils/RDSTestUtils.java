package net.avcompris.commons3.core.it.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.it.utils.IntegrationTestUtils.getTestProperty;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.zaxxer.hikari.HikariDataSource;

import net.avcompris.commons3.dao.DbTable;
import net.avcompris.commons3.dao.DbTablesUtils;

public abstract class RDSTestUtils {

	public static final String DEFAULT_PROPERTY_PREFIX_RDS = "rds";

	private RDSTestUtils() {

		throw new IllegalStateException();
	}

	private static final CacheLoader<String, DataSource> DATASOURCES_LOADER = new CacheLoader<String, DataSource>() {

		@Override
		public DataSource load(final String prefix) throws Exception {

			return setUpDataSource(prefix);
		}
	};

	private static final LoadingCache<String, DataSource> DATASOURCES = CacheBuilder.newBuilder()
			.build(DATASOURCES_LOADER);

	private static DataSource setUpDataSource(final String prefix) throws SQLException, IOException {

		final String dbURL = getTestProperty(prefix + ".url");
		final String dbUsername = getTestProperty(prefix + ".username");
		final String dbPassword = getTestProperty(prefix + ".password");

		System.out.println(prefix + ".url: " + dbURL);
		System.out.println(prefix + ".username: " + dbUsername);

		final DataSource dataSource = DataSourceBuilder.create() //
				.type(HikariDataSource.class) //
				.url(dbURL) //
				.username(dbUsername) //
				.password(dbPassword) //
				.build();

		((HikariDataSource) dataSource).setConnectionInitSql("SET TIME ZONE 'UTC'");

		return dataSource;
	}

	public static DataSource getDataSource() throws SQLException, IOException {

		return getDataSource(DEFAULT_PROPERTY_PREFIX_RDS);
	}

	public static DataSource getDataSource(final String prefix) throws SQLException, IOException {

		try {

			return DATASOURCES.get(prefix);

		} catch (final ExecutionException e) {

			final Throwable cause = e.getCause();

			if (cause == null) {

				throw new RuntimeException(e);

			} else if (cause instanceof SQLException) {

				throw (SQLException) cause;

			} else if (cause instanceof IOException) {

				throw (IOException) cause;

			} else if (cause instanceof RuntimeException) {

				throw (RuntimeException) cause;

			} else if (cause instanceof Error) {

				throw (Error) cause;

			} else {

				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Make sure the corresponding table exists in the database and complies to the
	 * description given in the XxxDbTable enum.
	 *
	 * @return the DB table’s name.
	 */
	public static String ensureDbTableName(final DbTable dbTable) throws IOException, SQLException {

		return ensureDbTableName(dbTable, DEFAULT_PROPERTY_PREFIX_RDS);
	}

	private static boolean hasDbTable(final DatabaseMetaData metadata, final String runtimeDbTableName)
			throws SQLException {

		System.err.println("runtimeDbTableName: " + runtimeDbTableName);

		try (ResultSet rs = metadata.getTables(null, null, runtimeDbTableName, null)) {

			if (rs.next()) {

				return true;
			}
		}

		return false;
	}

	private static boolean isDbTableCompatible(final DatabaseMetaData metadata, final String runtimeDbTableName,
			final DbTable dbTable) throws SQLException {

		return false;
	}

	private static String ensureDbTableName(final DbTable dbTable, final String prefix)
			throws IOException, SQLException {

		checkNotNull(dbTable, "dbTable");
		checkNotNull(prefix, "prefix");

		final String tableNamePrefix = getTestProperty(prefix + ".tableNamePrefix");

		final String runtimeDbTableName = dbTable.getRuntimeDbTableNameWithPrefix(tableNamePrefix);

		final DataSource dataSource = getDataSource(prefix);

		try (Connection cxn = dataSource.getConnection()) {

			final DatabaseMetaData metadata = cxn.getMetaData();

			final boolean toCreate;

			if (!hasDbTable(metadata, runtimeDbTableName)) {

				toCreate = true;

			} else if (isDbTableCompatible(metadata, runtimeDbTableName, dbTable)) {

				toCreate = false;

			} else {

				for (final String sqlDropCommand : DbTablesUtils.composeSQLDropCommands(tableNamePrefix, dbTable)) {

					System.out.println("Executing DROP command: " + sqlDropCommand);

					try (PreparedStatement pstmt = cxn.prepareStatement(sqlDropCommand)) {

						pstmt.executeUpdate();

					} catch (final SQLException e) {

						System.err.println(e);
					}
				}

				toCreate = true;
			}

			if (toCreate) {

				for (final String sqlCreateCommand : DbTablesUtils.composeSQLCreateCommands(tableNamePrefix, dbTable)) {

					System.out.println("Executing CREATE command: " + sqlCreateCommand);

					try (PreparedStatement pstmt = cxn.prepareStatement(sqlCreateCommand)) {

						pstmt.executeUpdate();
					}
				}
			}
		}

		return runtimeDbTableName;
	}
}
