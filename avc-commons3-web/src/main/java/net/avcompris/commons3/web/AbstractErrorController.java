package net.avcompris.commons3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.web.AbstractController.CORRELATION_ID_ATTRIBUTE_NAME;
import static net.avcompris.commons3.web.AbstractController.USER_SESSION_ID_ATTRIBUTE_NAME;

import javax.annotation.Nullable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.NestedServletException;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthenticatedException;
import net.avcompris.commons3.notifier.ErrorNotifier;

public abstract class AbstractErrorController implements ErrorController {

	private static final String ERROR_PATH = "/error";

	private final ErrorNotifier notifier;

	private final boolean debug;

	public AbstractErrorController(final ErrorNotifier notifier) {

		this.notifier = checkNotNull(notifier, "notifier");

		debug = System.getProperty("debug") != null;
	}

	// @Override
	// public final String getErrorPath() {
	// 
	// 	return ERROR_PATH;
	// }

	@RequestMapping(ERROR_PATH)
	@ResponseBody
	public final ResponseEntity<ApplicationError> handleError(final HttpServletRequest request,
			final HttpServletResponse response) {

		Throwable exception = (Exception) request.getAttribute("javax.servlet.error.exception");

		final Integer servletStatusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");

		final String correlationId = (String) request.getAttribute(CORRELATION_ID_ATTRIBUTE_NAME);
		final String userSessionId = (String) request.getAttribute(USER_SESSION_ID_ATTRIBUTE_NAME);

		if (userSessionId != null) {

			setUserSessionCookie(response, userSessionId);
		}

		if (exception != null) {

			exception.printStackTrace();

			notifier.notifyError(correlationId, null, exception);

			if (exception instanceof NestedServletException) {

				exception = exception.getCause();
			}
		}

		final int httpErrorCode;
		final String description;
		final String type;

		if (exception == null) {

			exception = (Exception) request.getAttribute("org.springframework.web.servlet.DispatcherServlet.EXCEPTION");

			if (exception != null) {

				if (debug) {

					exception.printStackTrace();
				}

				notifier.notifyError(correlationId, null, exception);
			}

			type = (exception != null) ? exception.getClass().getName() : null;

			if (servletStatusCode != null) {

				httpErrorCode = servletStatusCode;

				if (servletStatusCode == 404) {

					final String requestURI = (String) request.getAttribute("javax.servlet.forward.request_uri");

					description = requestURI;

				} else {

					description = (exception != null) ? exception.getMessage() : null;
				}

			} else {

				httpErrorCode = 500;

				description = (exception != null) ? exception.getMessage() : "Internal server error";
			}

		} else if (exception instanceof ServiceException) {

			final ServiceException serviceException = (ServiceException) exception;

			httpErrorCode = serviceException.getHttpErrorCode();
			description = serviceException.getDescription();
			type = exception.getClass().getSimpleName();

			if (exception instanceof UnauthenticatedException) {

				return handleError(correlationId, httpErrorCode, description, type,

						HttpHeaders.WWW_AUTHENTICATE,

						"Bearer realm=\"Avantage Compris\"");
			}

		} else {

			httpErrorCode = 500;
			description = "Internal server error: " + exception;
			type = exception.getClass().getSimpleName();
		}

		return handleError(correlationId, httpErrorCode, description, type);
	}

	private static ResponseEntity<ApplicationError> handleError(@Nullable final String correlationId,
			final int httpErrorCode, @Nullable final String description, @Nullable final String type,
			final String... headerNameValuePairs) {

		final ApplicationError error = new ApplicationError() {

			@Override
			public String getCorrelationId() {
				return correlationId;
			}

			@Override
			public int getStatusCode() {
				return httpErrorCode;
			}

			@Override
			public String getType() {
				return type;
			}

			@Override
			public String getDescription() {
				return description;
			}
		};

		final HttpHeaders headers = new HttpHeaders();

		for (int i = 0; i < headerNameValuePairs.length / 2; ++i) {

			final String headerName = headerNameValuePairs[i * 2];
			final String headerValue = headerNameValuePairs[i * 2 + 1];

			headers.add(headerName, headerValue);
		}

		headers.add("Correlation-ID", correlationId);

		return ResponseEntity //
				.status(httpErrorCode) //
				.headers(headers) //
				.body(error);
	}

	protected abstract boolean isApplicationSecure();

	protected abstract boolean isApplicationHttpOnly();

	private void setUserSessionCookie(final HttpServletResponse response, final String userSessionId) {

		checkNotNull(userSessionId, "userSessionId");

		final Cookie cookie = new Cookie(USER_SESSION_ID_ATTRIBUTE_NAME, userSessionId);

		// cookie.setDomain("toptal.com");
		cookie.setSecure(isApplicationSecure());
		cookie.setHttpOnly(isApplicationHttpOnly());
		cookie.setMaxAge(3600);
		cookie.setPath("/");

		response.addCookie(cookie);
	}
}
