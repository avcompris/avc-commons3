package net.avcompris.commons3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthenticatedException;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.AuthService;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;

public abstract class AbstractController {

	public static final String CORRELATION_ID_ATTRIBUTE_NAME = "Correlation-ID";
	public static final String USER_SESSION_ID_ATTRIBUTE_NAME = "user_session_id";

	private static final Log logger = LogFactory.getLog(AbstractController.class);

	protected final CorrelationService correlationService;
	protected final Clock clock;

	@Nullable
	private final SessionPropagator sessionPropagator;

	protected AbstractController(final CorrelationService correlationService, final SessionPropagator sessionPropagator,
			final Clock clock) {

		this.correlationService = checkNotNull(correlationService, "correlationService");
		this.sessionPropagator = checkNotNull(sessionPropagator, "sessionPropagator");
		this.clock = checkNotNull(clock, "clock");
	}

	@Nullable
	private static String getCookie(final HttpServletRequest request, final String cookieName) {

		checkNotNull(request, "request");
		checkNotNull(cookieName, "cookieName");

		final Cookie[] cookies = request.getCookies();

		if (cookies == null) {
			return null;
		}

		for (final Cookie cookie : cookies) {

			if (cookieName.contentEquals(cookie.getName())) {

				return cookie.getValue();
			}
		}

		return null;
	}

	@Nullable
	private final String getAuthorization(final HttpServletRequest request) {

		final String authorizationHeader = request.getHeader(AUTHORIZATION);

		if (authorizationHeader != null) {

			return authorizationHeader;
		}

		final String authorizationCookie = getCookie(request, AUTHORIZATION);

		if (authorizationCookie == null) {

			return null;
		}

		final String userSessionId = getUserSessionId(request);

		return userSessionId == null

				? authorizationCookie

				: null;
	}

	@Nullable
	protected final String getUserSessionId(final HttpServletRequest request) {

		final String userSessionIdParam = request.getParameter(USER_SESSION_ID_ATTRIBUTE_NAME);
		final String userSessionIdHeader = request.getHeader(USER_SESSION_ID_ATTRIBUTE_NAME);
		final String userSessionIdCookie = getCookie(request, USER_SESSION_ID_ATTRIBUTE_NAME);

		if (userSessionIdParam != null) {

			return userSessionIdParam;

		} else if (userSessionIdHeader != null) {

			return userSessionIdHeader;

		} else if (userSessionIdCookie != null) {

			return userSessionIdCookie;

		} else {

			return null;
		}
	}

	protected final <T> ResponseEntity<T> wrapAuthenticated(final HttpServletRequest request,
			final HttpServletResponse response, final AuthService authService, final AuthenticatedAction<T> action)
			throws ServiceException {

		checkNotNull(request, "request");
		checkNotNull(action, "action");

		// TODO: How to factor this code with wrapAuthenticatedServletAction()?!

		final long startMs = System.currentTimeMillis();

		final String authorization = getAuthorization(request);
		final String userSessionId = getUserSessionId(request);
		final String correlationId = getCorrelationId(request);

		sessionPropagator.setAuthorizationHeader(authorization);
		sessionPropagator.setUserSessionId(userSessionId);

		@Nullable
		final User user = authService.getAuthenticatedUser(authorization, userSessionId);

		if (user == null) {

			throw new UnauthenticatedException();
		}

		if (userSessionId != null) {

			// Use this to propage userSessionId all the way to ApplicationErrorController
			//
			request.setAttribute(USER_SESSION_ID_ATTRIBUTE_NAME, userSessionId);
		}

		final Pair<Class<?>, Method> endpoint = extractControllerCurrentEndpoint();

		final String methodName = endpoint.getRight().getName();

		final Log logger = LogFactory.getLog(endpoint.getLeft());

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() started... +ms: " + (System.currentTimeMillis() - startMs));
		}

		final ResponseEntity<T> result;

		authService.setLastActiveAt(correlationId, user);

		try {

			result = action.action(correlationId, user);

		} catch (final ServiceException e) {

			final int httpErrorCode = e.getHttpErrorCode();

			final long elapsedMs = System.currentTimeMillis() - startMs;

			logger.error(methodName + "() ERROR. " + httpErrorCode + ". elapsedMs: " + elapsedMs, e);

			throw e;
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() ended. " + result.getStatusCode() + ". elapsedMs: " + elapsedMs);
		}

		return enrich(response, userSessionId, result, correlationId);
	}

	protected final <T> ResponseEntity<T> wrapAuthenticatedServletAction(final HttpServletRequest request,
			final HttpServletResponse response, final AuthService authService,
			final AuthenticatedServletAction<T> action) throws ServiceException, IOException, ServletException {

		checkNotNull(request, "request");
		checkNotNull(action, "action");

		// TODO: How to factor this code with wrapAuthenticated()?!

		final long startMs = System.currentTimeMillis();

		final String authorization = getAuthorization(request);
		final String userSessionId = getUserSessionId(request);
		final String correlationId = getCorrelationId(request);

		sessionPropagator.setAuthorizationHeader(authorization);
		sessionPropagator.setUserSessionId(userSessionId);

		@Nullable
		final User user = authService.getAuthenticatedUser(authorization, userSessionId);

		if (user == null) {

			throw new UnauthenticatedException();
		}

		if (userSessionId != null) {

			// Use this to propage userSessionId all the way to ApplicationErrorController
			//
			request.setAttribute(USER_SESSION_ID_ATTRIBUTE_NAME, userSessionId);
		}

		final Pair<Class<?>, Method> endpoint = extractControllerCurrentEndpoint();

		final String methodName = endpoint.getRight().getName();

		final Log logger = LogFactory.getLog(endpoint.getLeft());

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() started... +ms: " + (System.currentTimeMillis() - startMs));
		}

		authService.setLastActiveAt(correlationId, user);

		final ResponseEntity<T> result;

		try {

			result = action.action(correlationId, user);

		} catch (final ServiceException e) {

			final int httpErrorCode = e.getHttpErrorCode();

			final long elapsedMs = System.currentTimeMillis() - startMs;

			logger.error(methodName + "() ERROR. " + httpErrorCode + ". elapsedMs: " + elapsedMs, e);

			throw e;

		} catch (final IOException | ServletException e) {

			final long elapsedMs = System.currentTimeMillis() - startMs;

			logger.error(methodName + "() elapsedMs: " + elapsedMs, e);

			throw e;
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() ended. " + result.getStatusCode() + ". elapsedMs: " + elapsedMs);
		}

		return enrich(response, userSessionId, result, correlationId);
	}

	protected final <T> ResponseEntity<T> wrapAuthenticatedOrNot(final HttpServletRequest request,
			final HttpServletResponse response, final AuthService authService, final AuthenticatedAction<T> action)
			throws ServiceException {

		checkNotNull(request, "request");
		checkNotNull(action, "action");

		final long startMs = System.currentTimeMillis();

		final String authorization = getAuthorization(request);
		final String userSessionId = getUserSessionId(request);
		final String correlationId = getCorrelationId(request);

		sessionPropagator.setAuthorizationHeader(authorization);
		sessionPropagator.setUserSessionId(userSessionId);

		@Nullable
		final User user = authService.getAuthenticatedUser(authorization, userSessionId);

		final Pair<Class<?>, Method> endpoint = extractControllerCurrentEndpoint();

		final String methodName = endpoint.getRight().getName();

		final Log logger = LogFactory.getLog(endpoint.getLeft());

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() started... +ms: " + (System.currentTimeMillis() - startMs));
		}

		final ResponseEntity<T> result;

		if (user != null) {

			authService.setLastActiveAt(correlationId, user);
		}

		try {

			result = action.action(correlationId, user);

		} catch (final ServiceException e) {

			final int httpErrorCode = e.getHttpErrorCode();

			final long elapsedMs = System.currentTimeMillis() - startMs;

			logger.error(methodName + "() ERROR. " + httpErrorCode + ". elapsedMs: " + elapsedMs, e);

			throw e;
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() ended. " + result.getStatusCode() + ". elapsedMs: " + elapsedMs);
		}

		return enrich(response, userSessionId, result, correlationId);
	}

	protected final <T> ResponseEntity<T> wrapWithoutCorrelationId(final HttpServletRequest request,
			final HttpServletResponse response, final AuthService authService, final AuthenticatedAction<T> action)
			throws ServiceException {

		checkNotNull(request, "request");
		checkNotNull(action, "action");

		final long startMs = System.currentTimeMillis();

		final String authorization = getAuthorization(request);
		final String userSessionId = getUserSessionId(request);

		String fakeCorrelationId = "N/A";

		try {

			fakeCorrelationId = getCorrelationId(request);

		} catch (final Throwable e) {

			e.printStackTrace(System.out);

			fakeCorrelationId = "N/A";
		}

		LogFactory.resetCorrelationId();

		LogFactory.setCorrelationId(fakeCorrelationId);

		sessionPropagator.setAuthorizationHeader(authorization);
		sessionPropagator.setUserSessionId(userSessionId);

		final User user = authService.getAuthenticatedUser(authorization, userSessionId);

		if (user == null) {

			throw new UnauthenticatedException();
		}

		final Pair<Class<?>, Method> endpoint = extractControllerCurrentEndpoint();

		final String methodName = endpoint.getRight().getName();

		final Log logger = LogFactory.getLog(endpoint.getLeft());

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() started... +ms: " + (System.currentTimeMillis() - startMs));
		}

		final ResponseEntity<T> result;

		try {

			result = action.action(fakeCorrelationId, user);

		} catch (final ServiceException e) {

			final int httpErrorCode = e.getHttpErrorCode();

			final long elapsedMs = System.currentTimeMillis() - startMs;

			logger.error(methodName + "() ERROR. " + httpErrorCode + ". elapsedMs: " + elapsedMs, e);

			throw e;
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() ended. " + result.getStatusCode() + ". elapsedMs: " + elapsedMs);
		}

		return enrich(response, userSessionId, result, fakeCorrelationId);
	}

	private String getCorrelationId(final HttpServletRequest request) throws ServiceException {

		final String correlationIdParam = request.getParameter("correlationId");
		final String correlationIdHeader = request.getHeader(CORRELATION_ID_ATTRIBUTE_NAME);

		final long startMs = System.currentTimeMillis();

		final String correlationId = correlationService.getCorrelationId(correlationIdParam, correlationIdHeader);

		LogFactory.resetCorrelationId();

		LogFactory.setCorrelationId(correlationId);

		request.setAttribute(CORRELATION_ID_ATTRIBUTE_NAME, correlationId);

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isDebugEnabled()) {
			logger.debug("getCorrelationId(), elapsedMs: " + elapsedMs);
		}

		return correlationId;
	}

	protected final <T> ResponseEntity<T> wrapNonAuthenticated(final HttpServletRequest request,
			final UnauthenticatedAction<T> action) throws ServiceException {

		checkNotNull(request, "request");
		checkNotNull(action, "action");

		final long startMs = System.currentTimeMillis();

		final String correlationId = getCorrelationId(request);

		final Pair<Class<?>, Method> endpoint = extractControllerCurrentEndpoint();

		final String methodName = endpoint.getRight().getName();

		final Log logger = LogFactory.getLog(endpoint.getLeft());

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() started... +ms: " + (System.currentTimeMillis() - startMs));
		}

		final ResponseEntity<T> result;

		try {

			result = action.action(correlationId);

		} catch (final ServiceException e) {

			final int httpErrorCode = e.getHttpErrorCode();

			final long elapsedMs = System.currentTimeMillis() - startMs;

			logger.error(methodName + "() ERROR. " + httpErrorCode + ". elapsedMs: " + elapsedMs, e);

			throw e;
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() ended. " + result.getStatusCode() + ". elapsedMs: " + elapsedMs);
		}

		return enrich(null, null, result, correlationId);
	}

	protected final <T> ResponseEntity<T> wrapNonAuthenticatedWithoutCorrelationId(final HttpServletRequest request,
			final UnauthenticatedAnonymousAction<T> action) throws ServiceException {

		checkNotNull(request, "request");
		checkNotNull(action, "action");

		final long startMs = System.currentTimeMillis();

		final Pair<Class<?>, Method> endpoint = extractControllerCurrentEndpoint();

		final String methodName = endpoint.getRight().getName();

		String fakeCorrelationId = "N/A";

		try {

			fakeCorrelationId = getCorrelationId(request);

		} catch (final Throwable e) {

			e.printStackTrace(System.out);

			fakeCorrelationId = "N/A";
		}

		LogFactory.resetCorrelationId();

		LogFactory.setCorrelationId(fakeCorrelationId);

		final Log logger = LogFactory.getLog(endpoint.getLeft());

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() started... +ms: " + (System.currentTimeMillis() - startMs));
		}

		final ResponseEntity<T> result;

		try {

			result = action.action();

		} catch (final ServiceException e) {

			final int httpErrorCode = e.getHttpErrorCode();

			final long elapsedMs = System.currentTimeMillis() - startMs;

			logger.error(methodName + "() ERROR. " + httpErrorCode + ". elapsedMs: " + elapsedMs, e);

			throw e;
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isInfoEnabled()) {
			logger.info(methodName + "() ended. " + result.getStatusCode() + ". elapsedMs: " + elapsedMs);
		}

		// return enrich(null, null, result, correlationId);

		return result;
	}

	private <T> ResponseEntity<T> enrich(@Nullable final HttpServletResponse httpServletResponse, //
			@Nullable final String userSessionId, //
			final ResponseEntity<T> response, final String correlationId) {

		checkNotNull(response, "response");
		checkNotNull(correlationId, "correlationId");

		final HttpHeaders headers = new HttpHeaders();

		headers.putAll(response.getHeaders());

		headers.add(CORRELATION_ID_ATTRIBUTE_NAME, correlationId);

		if (httpServletResponse != null && userSessionId != null) {

			setUserSessionCookie(httpServletResponse, userSessionId);
		}

		return ResponseEntity.status(response.getStatusCode()) //
				.headers(headers) //
				.body(response.getBody());
	}

	private static Pair<Class<?>, Method> extractControllerCurrentEndpoint() {

		for (final StackTraceElement ste : Thread.currentThread().getStackTrace()) {

			final String className = ste.getClassName();
			final String methodName = ste.getMethodName();

			if (!className.endsWith("Controller")) {
				continue;
			}

			final Class<?> controllerClass;

			try {

				controllerClass = Class.forName(className);

			} catch (final ClassNotFoundException e) {

				continue;
			}

			if (Modifier.isAbstract(controllerClass.getModifiers())) {
				continue;
			}

			final Method method = extractDeclaredMethod(controllerClass, methodName);

			if (method.getAnnotation(RequestMapping.class) == null || !Modifier.isPublic(method.getModifiers())) {
				continue;
			}

			return Pair.of(controllerClass, method);
		}

		throw new IllegalStateException("Cannot extract controller current endpoint");
	}

	private static Method extractDeclaredMethod(final Class<?> controllerClass, final String methodName) {

		for (final Method method : controllerClass.getDeclaredMethods()) {

			if (methodName.contentEquals(method.getName()) || Modifier.isPublic(method.getModifiers())) {

				return method;
			}
		}

		throw new IllegalStateException(
				"Cannot extract method: " + methodName + " from controllerClass: " + controllerClass.getName());
	}

	@FunctionalInterface
	protected interface UnauthenticatedAnonymousAction<T> {

		ResponseEntity<T> action() throws ServiceException;
	}

	@FunctionalInterface
	protected interface UnauthenticatedAction<T> {

		ResponseEntity<T> action(String correlationId) throws ServiceException;
	}

	@FunctionalInterface
	protected interface AuthenticatedAction<T> {

		ResponseEntity<T> action(String correlationId, User user) throws ServiceException;
	}

	@FunctionalInterface
	protected interface AuthenticatedServletAction<T> {

		ResponseEntity<T> action(String correlationId, User user)
				throws ServiceException, ServletException, IOException;
	}

	protected final <T extends ResponseEntity<?>> T handleServiceException(final ServiceException e) {

		checkNotNull(e, "e");

		throw new NotImplementedException("");
	}

	protected static HttpHeaders headers(final String... headerNameValuePairs) {

		final HttpHeaders headers = new HttpHeaders();

		for (int i = 0; i < headerNameValuePairs.length / 2; ++i) {

			final String headerName = headerNameValuePairs[i * 2];
			final String headerValue = headerNameValuePairs[i * 2 + 1];

			headers.set(headerName, headerValue);
		}

		return headers;
	}

	protected abstract boolean isSecure();

	protected abstract boolean isHttpOnly();

	protected final void setUserSessionCookie(final HttpServletResponse response, final String userSessionId) {

		checkNotNull(response, "response");
		checkNotNull(userSessionId, "userSessionId");

		final Cookie cookie = new Cookie(USER_SESSION_ID_ATTRIBUTE_NAME, userSessionId);

		// cookie.setDomain("...");
		cookie.setSecure(isSecure());
		cookie.setHttpOnly(isHttpOnly());
		cookie.setMaxAge(3600);
		cookie.setPath("/");

		response.addCookie(cookie);
	}
}
