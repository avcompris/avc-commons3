package net.avcompris.commons3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;

import com.zaxxer.hikari.HikariDataSource;

public abstract class AbstractApplicationConfig {

	public abstract static class AbstractTableNamesConfig {

		protected abstract String getTableNamePrefix();
	}

	public abstract static class AbstractRDSConfig extends AbstractTableNamesConfig {

		private final String dbUrlPropertyName;
		private final String dbUsernamePropertyName;
		private final String dbPasswordPropertyName;
		private final String dbTableNamePrefixPropertyName;

		private DataSource dataSource;
		private String tableNamePrefix;
		private String dbURL;
		private String dbUsername;

		public AbstractRDSConfig(final String dbUrlPropertyName, final String dbUsernamePropertyName,
				final String dbPasswordPropertyName, final String dbTableNamePrefixPropertyName) {

			this.dbUrlPropertyName = checkNotNull(dbUrlPropertyName, "dbUrlPropertyName");
			this.dbUsernamePropertyName = checkNotNull(dbUsernamePropertyName, "dbUsernamePropertyName");
			this.dbPasswordPropertyName = checkNotNull(dbPasswordPropertyName, "dbPasswordPropertyName");
			this.dbTableNamePrefixPropertyName = checkNotNull(dbTableNamePrefixPropertyName,
					"dbTableNamePrefixPropertyName");
		}

		private synchronized void loadDataSource() {

			// e.g "jdbc:postgresql://localhost:5432/devdb"
			//
			// dbURL = System.getProperty(dbUrlPropertyName);

			if (dbURL == null) {
				loadDbURL();
			}

			if (dbUsername == null) {
				loadDbUsername();
			}

			final String dbPassword = System.getProperty(dbPasswordPropertyName);

			if (isBlank(dbPassword)) {
				throw new IllegalStateException(dbPasswordPropertyName + " should be set as an environment variable.");
			}

			try {

				Class.forName(org.postgresql.Driver.class.getName());

			} catch (final ClassNotFoundException e) {

				throw new RuntimeException(e);
			}

			dataSource = DataSourceBuilder.create() //
					.type(HikariDataSource.class) //
					.url(dbURL) //
					.username(dbUsername) //
					.password(dbPassword) //
					.build();
		}

		public final DataSource getDataSource() {

			if (dataSource == null) {
				loadDataSource();
			}

			return dataSource;
		}

		public final String getUrl() {

			if (dbURL == null) {
				loadDbURL();
			}

			return dbURL;
		}

		public final String getUsername() {

			if (dbUsername == null) {
				loadDbUsername();
			}

			return dbUsername;
		}

		@Override
		public final String getTableNamePrefix() {

			if (tableNamePrefix == null) {
				loadTableNamePrefix();
			}

			return tableNamePrefix;
		}

		private synchronized void loadDbURL() {

			final String dbURL = System.getProperty(dbUrlPropertyName);

			System.out.println(dbUrlPropertyName + ": " + dbURL);

			if (isBlank(dbURL)) {
				throw new IllegalStateException(dbUrlPropertyName + " should be set as an environment variable."
						+ " It should be of the form: \"jdbc:postgresql://localhost:5432/devdb\"");
			}

			this.dbURL = dbURL;
		}

		private synchronized void loadDbUsername() {

			final String dbUsername = System.getProperty(dbUsernamePropertyName);

			System.out.println(dbUsernamePropertyName + ": " + dbUsername);

			if (isBlank(dbUsername)) {
				throw new IllegalStateException(dbUsernamePropertyName + " should be set as an environment variable."
						+ " It should be of the form: \"postgres\"");
			}

			this.dbUsername = dbUsername;
		}

		private synchronized void loadTableNamePrefix() {

			// e.g "dev_"
			//
			final String tableNamePrefix = System.getProperty(dbTableNamePrefixPropertyName);

			System.out.println(dbTableNamePrefixPropertyName + ": " + tableNamePrefix);

			if (isBlank(tableNamePrefix)) {
				throw new IllegalStateException(
						dbTableNamePrefixPropertyName + " should be set as an environment variable."
								+ " It should be of the form: \"dev_\", \"qa_\", \"prod_\", etc.");
			}

			if (!tableNamePrefix.endsWith("_")) {
				throw new IllegalStateException(
						"tableNamePrefix should end with \"xxx_\", but was: " + tableNamePrefix);
			}

			this.tableNamePrefix = tableNamePrefix;
		}
	}
}
