package net.avcompris.commons3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.repeat;

import java.io.PrintStream;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Nullable;

import org.springframework.context.ApplicationContext;

public abstract class ApplicationUtils {

	/**
	 * Dump the list of custom beans created for the application.
	 *
	 * @param packagePrefixes e.g. "net.avcompris.commons3.examples.users" (without
	 *                      the trailing ".")
	 */
	public static void dumpBeans(final ApplicationContext context, final PrintStream ps,
			final String... packagePrefixes) {

		checkNotNull(context, "context");
		checkNotNull(ps, "ps");
		checkNotNull(packagePrefixes, "packagePrefixes");

		System.out.println("Beans:");

		final Map<String, String> beanNames = new TreeMap<>();

		int beanClassNameMaxLength = 0;

		for (final String beanName : context.getBeanDefinitionNames()) {

			final Object bean = context.getBean(beanName);
			final Class<?> beanClass = bean.getClass();

			final Package beanPackage = beanClass.getPackage();

			if (!containsPackagePrefix(packagePrefixes, beanPackage)) {
				continue;
			}

			final String beanClassName = beanClass.getName();

			beanNames.put(beanClassName, beanName);

			beanClassNameMaxLength = Math.max(beanClassNameMaxLength, beanClassName.length());
		}

		for (final String beanClassName : beanNames.keySet()) {

			final String beanName = beanNames.get(beanClassName);

			System.out.println("  - " + beanClassName

					+ repeat(' ', beanClassNameMaxLength - beanClassName.length() + 1)

					+ "(" + beanName + ")");
		}
	}

	private static boolean containsPackagePrefix(final String[] packagePrefixes, @Nullable final Package beanPackage) {

		if (beanPackage == null) {
			return false;
		}

		for (final String packagePrefix : packagePrefixes) {

			if (beanPackage.getName().startsWith(packagePrefix + ".")) {

				return true;
			}
		}

		return false;
	}
}
