package net.avcompris.commons3.web;

import javax.annotation.Nullable;

public interface ApplicationError {

	@Nullable
	String getCorrelationId();

	int getStatusCode();

	@Nullable
	String getType();

	@Nullable
	String getDescription();
}
