package net.avcompris.commons3.web;

import com.beust.jcommander.Parameter;

public abstract class AbstractApplicationArgs {

	@Parameter(names = "--debug")
	public boolean debug;

	@Parameter(names = "--info")
	public boolean info;

	@Parameter(names = "--warn")
	public boolean warn;

	@Parameter(names = "--error")
	public boolean error;

	@Parameter(names = "--unsecure")
	public boolean unsecure;

	@Parameter(names = "--inMemory")
	public boolean inMemory;

	@Parameter(names = "--rds")
	public boolean rds;
}
