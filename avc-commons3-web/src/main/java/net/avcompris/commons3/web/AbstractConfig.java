package net.avcompris.commons3.web;

import static com.google.common.base.Preconditions.checkNotNull;

import net.avcompris.commons3.utils.Level;
import net.avcompris.commons3.utils.LogFactory;

public abstract class AbstractConfig {

	public final boolean inMemory;
	public final boolean rds;

	protected AbstractConfig(final AbstractApplicationArgs myargs) {

		checkNotNull(myargs, "myargs");

		if (myargs.inMemory) {

			inMemory = true;
			rds = false;

		} else if (myargs.rds) {

			inMemory = false;
			rds = true;

		} else {

			throw new IllegalStateException(
					"Either \"--inMemory\" or \"--rds\" must be specified when launching the application.");
		}

		if (myargs.debug || System.getProperty("debug") != null) {

			LogFactory.setLevel(Level.DEBUG);

		} else if (myargs.info) {

			LogFactory.setLevel(Level.INFO);

		} else if (myargs.warn) {

			LogFactory.setLevel(Level.WARN);

		} else if (myargs.error) {

			LogFactory.setLevel(Level.ERROR);

		} else {

			LogFactory.setLevel(Level.INFO);
		}
	}
}
