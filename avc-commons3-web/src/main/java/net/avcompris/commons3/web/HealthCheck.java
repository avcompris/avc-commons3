package net.avcompris.commons3.web;

import javax.annotation.Nullable;

import net.avcompris.commons3.dao.HealthCheckDb;

public interface HealthCheck extends HealthCheckDb {

	@Override
	boolean isOk();

	String getComponentName();

	@Nullable
	@Override
	String[] getErrors();
}
