package net.avcompris.commons3.web;

import net.avcompris.commons3.dao.impl.MutableHealthCheckDb;

public interface MutableHealthCheck extends MutableHealthCheckDb, HealthCheck {

	@Override
	MutableHealthCheck setOk(boolean ok);

	MutableHealthCheck setComponentName(String componentName);

	@Override
	MutableHealthCheck addToErrors(String errorMessage);
}
