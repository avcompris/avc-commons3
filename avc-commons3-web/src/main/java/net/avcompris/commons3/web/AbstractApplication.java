package net.avcompris.commons3.web;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.join;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * Base class for all our Spring Boot applications.
 *
 * @author dandriana
 */
public abstract class AbstractApplication {

	/**
	 * In here, we just set JSON indentation for API responses.
	 */
	protected AbstractApplication() {

		System.setProperty("spring.jackson.serialization.indent_output", Boolean.TRUE.toString());
	}

	/**
	 * Choose a DAO class among two or three, depending on arg flags.
	 */
	protected static <T> Class<? extends T> dao(final Class<T> daoClass, final Object... flagClassPairs) {

		checkNotNull(daoClass, "daoClass");
		checkNotNull(flagClassPairs, "flagClassPairs");

		checkArgument(daoClass.isInterface(), //
				"daoClass should be an interface, but was: %s", daoClass.getName());

		checkArgument(flagClassPairs.length % 2 == 0, //
				"flagClassPairs.length should be % 2 == 0, but was: %s", flagClassPairs.length);

		Class<? extends T> selectedDaoImplClass = null;

		final List<String> daoImplClassNames = new ArrayList<>();

		for (int i = 0; i < flagClassPairs.length / 2; ++i) {

			final boolean flag = (Boolean) flagClassPairs[i * 2];
			final Class<?> daoImplClass = (Class<?>) flagClassPairs[i * 2 + 1];

			checkArgument(!daoImplClass.isInterface(), //
					"daoImplClass should not be an interface, but was: %s", daoImplClass.getName());

			checkArgument(!Modifier.isAbstract(daoImplClass.getModifiers()),
					"daoImplClass should not be an abstract class, but was: %s", daoImplClass.getName());

			checkArgument(daoClass.isAssignableFrom(daoImplClass),
					"daoImplClass should implement the DAO interface: %s, but was: %s", daoClass.getName(),
					daoImplClass.getName());

			daoImplClassNames.add(daoImplClass.getName());

			if (flag && selectedDaoImplClass == null) {

				selectedDaoImplClass = daoImplClass.asSubclass(daoClass);
			}
		}

		checkArgument(selectedDaoImplClass != null, //
				"Could not choose between: %s.  Make sure you are passing an DAO-option to the command line, such as: --inMemory, --rds or --redis",
				join(daoImplClassNames, ", "));

		return selectedDaoImplClass;
	}
}
