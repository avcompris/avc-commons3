package net.avcompris.commons3.core;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;

public interface CorrelationService {

	/**
	 * Get a non-null correlationId from the HTTP request, or create a new
	 * correlationId if needed.
	 *
	 * @param correlationIdParam  the "correlationId", if any, passed as a parameter
	 *                            in the HTTP URL’s query (overwrite
	 *                            correlationIdHeader)
	 * @param correlationIdHeader the "correlationId", if any, passed as a Header in
	 *                            the HTTP Request
	 */
	String getCorrelationId(@Nullable String correlationIdParam, @Nullable String correlationIdHeader)
			throws ServiceException;

	void purgeOlderThanSec(String correlationId, User user, int seconds) throws ServiceException;
}
