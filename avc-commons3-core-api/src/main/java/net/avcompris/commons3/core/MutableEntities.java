package net.avcompris.commons3.core;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.Entities;
import net.avcompris.commons3.api.Entity;

/**
 * Please note that because of Jackson deserialization (tested in our
 * <code>JSONUtilsTest</code> class), we must not declare a generic
 * <code>setResults(T[] item)</code> method in this super-super-interface, but
 * rather have the <code>setResults(xxx)</code> method declared in the final
 * implementation interface, such as: MutableCustomers, etc.
 *
 * @author dandriana
 */
public interface MutableEntities<T extends Entity> extends Entities<T> {

	// MutableEntities<T> setResults(T[] item);

	MutableEntities<T> addToResults(T item);

	MutableEntities<T> setStart(int start);

	MutableEntities<T> setLimit(int limit);

	MutableEntities<T> setSize(int size);

	MutableEntities<T> setTotal(int total);

	MutableEntities<T> setTookMs(int tookMs);

	MutableEntities<T> setSqlWhereClause(@Nullable String sqlWhereClause);
}
