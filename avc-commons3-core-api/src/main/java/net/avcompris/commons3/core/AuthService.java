package net.avcompris.commons3.core;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.api.UserSessions;
import net.avcompris.commons3.api.UserSessionsQuery;
import net.avcompris.commons3.api.exception.ServiceException;

public interface AuthService {

	@Nullable
	UserSessionsQuery validateUserSessionsQuery(String correlationId, User user, //
			@Nullable String q, //
			@Nullable String sort, //
			@Nullable Integer start, //
			@Nullable Integer limit, //
			@Nullable String expand //
	) throws ServiceException;

	UserSessions getUserSessions(String correlationId, User user, //
			@Nullable UserSessionsQuery query //
	) throws ServiceException;

	@Nullable
	User getAuthenticatedUser(@Nullable String authorization, @Nullable String userSessionId) throws ServiceException;

	@Nullable
	UserSession authenticate(String correlationId, String username, String password) throws ServiceException;

	void setLastActiveAt(String correlationId, User user) throws ServiceException;

	UserSession getMySession(String correlationId, User user, String userSessionId) throws ServiceException;

	UserSession terminateMySession(String correlationId, User user, String userSessionId) throws ServiceException;

	UserSession getUserSession(String correlationId, User user, String userSessionId) throws ServiceException;

	UserSession terminateUserSession(String correlationId, User user, String userSessionId) throws ServiceException;
}
