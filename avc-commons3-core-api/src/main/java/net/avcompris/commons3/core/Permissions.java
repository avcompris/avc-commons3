package net.avcompris.commons3.core;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.UnauthorizedException;

public interface Permissions {

	void assertAuthorized(String correlationId, User user, Object... contextNameValuePairs)
			throws UnauthorizedException;
}
