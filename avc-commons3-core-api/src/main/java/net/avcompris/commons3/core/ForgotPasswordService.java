package net.avcompris.commons3.core;

import net.avcompris.commons3.api.exception.ServiceException;

public interface ForgotPasswordService {

	/**
	 * Send the token via email
	 */
	void forgotPassword(String correlationId, String uiUrl, String emailAddress) throws ServiceException;

	void resetPassword(String correlationId, String token, String password) throws ServiceException;
}
