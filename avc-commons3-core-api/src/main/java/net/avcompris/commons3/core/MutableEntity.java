package net.avcompris.commons3.core;

import net.avcompris.commons3.api.Entity;
import net.avcompris.commons3.types.DateTimeHolder;

public interface MutableEntity extends Entity {

	MutableEntity setRevision(int revision);

	MutableEntity setCreatedAt(DateTimeHolder createdAt);

	MutableEntity setUpdatedAt(DateTimeHolder updatedAt);
}
