package net.avcompris.commons3.core;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import net.avcompris.commons3.api.EnumPermission;
import net.avcompris.commons3.api.EnumRole;
import net.avcompris.commons3.api.User;

public abstract class PermissionUtils<T extends EnumRole> {

	private static final Table<EnumRole, EnumPermission, Boolean> CACHED_PERMISSION_TYPES = HashBasedTable.create();

	private final Class<T> roleClass;

	private final Map<String, EnumRole> roles = new HashMap<>();

	public PermissionUtils(final Class<T> roleClass) {

		this.roleClass = checkNotNull(roleClass, "roleClass");

		checkArgument(roleClass.isEnum(), //
				"roleClass should be enum, but was: %s", roleClass.getName());

		final Method valuesMethod;

		try {

			valuesMethod = roleClass.getMethod("values");

		} catch (final NoSuchMethodException e) {
			throw new IllegalStateException(e);
		}

		final Object values;

		try {

			values = valuesMethod.invoke(null);

		} catch (final IllegalAccessException | InvocationTargetException e) {
			throw new IllegalStateException(e);
		}

		final int count = Array.getLength(values);

		for (int i = 0; i < count; ++i) {

			final Object item = Array.get(values, i);

			final EnumRole role = roleClass.cast(item);

			roles.put(role.getRolename(), role);
		}
	}

	public static boolean has(final User user, final EnumPermission permission) {

		checkNotNull(user, "user");

		return has(user.getRole(), permission);
	}

	public EnumRole getRole(final String rolename) {

		checkNotNull(rolename, "rolename");

		final EnumRole role = roles.get(rolename);

		checkArgument(role != null, //
				"Unknown role: %s, in class: %s", rolename, roleClass.getName());

		return role;
	}

	public static boolean has(final EnumRole role, final EnumPermission permission) {

		checkNotNull(role, "role");
		checkNotNull(permission, "permission");

		final Boolean cached = CACHED_PERMISSION_TYPES.get(role, permission);

		if (cached != null) {

			return cached;
		}

		final boolean checked = check(role, permission);

		CACHED_PERMISSION_TYPES.put(role, permission, checked);

		return checked;
	}

	private static boolean check(final EnumRole role, final EnumPermission permission) {

		for (final EnumRole superRole : role.getSuperRoles()) {

			if (has(superRole, permission)) {

				return true;
			}
		}

		for (final EnumPermission p : role.getPermissions()) {

			if (p == permission) {

				return true;
			}
		}

		return false;
	}

	public static void assertPermissionTypeClass(final Class<? extends EnumPermission> permissionTypeClass) {

		checkNotNull(permissionTypeClass, "permissionTypeClass");

		checkArgument(permissionTypeClass.isEnum(), //
				"permissionTypeClass should be Enum, but was: %s", permissionTypeClass.getName());
	}
}
