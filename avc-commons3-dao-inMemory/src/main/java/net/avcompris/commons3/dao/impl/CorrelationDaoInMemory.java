package net.avcompris.commons3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.commons3.dao.exception.DuplicateEntityException;
import net.avcompris.commons3.utils.Clock;

@Component
public final class CorrelationDaoInMemory extends AbstractDao implements CorrelationDao {

	private final Map<String, Holder> holders = newHashMap();

	@Autowired
	public CorrelationDaoInMemory(final Clock clock) {

		super(clock);
	}

	private static class Holder {

		public final String correlationId;
		public final DateTime createdAt;

		public Holder(final String correlationId, final DateTime createdAt) {

			this.correlationId = checkNotNull(correlationId, "correlationId");
			this.createdAt = checkNotNull(createdAt, "createdAt");
		}
	}

	@Override
	public boolean isCorrelationIdValid(final String correlationId) {

		checkNotNull(correlationId, "correlationId");

		return holders.containsKey(correlationId);
	}

	@Override
	public void addCorrelationId(final String correlationId)
			throws SQLException, IOException, DuplicateEntityException {

		checkNotNull(correlationId, "correlationId");

		synchronized (holders) {

			if (holders.containsKey(correlationId)) {
				throw new DuplicateEntityException("correlationId: " + correlationId);
			}

			holders.put(correlationId, new Holder(correlationId, clock.now()));
		}
	}

	@Override
	public void purgeOlderThanSec(final int seconds) throws SQLException, IOException {

		final DateTime olderThan = clock.now().minusSeconds(seconds);

		final List<String> correlationIdsToRemove = holders.values().stream() //
				.filter(holder -> holder.createdAt.isBefore(olderThan)) //
				.map(holder -> holder.correlationId) //
				.collect(Collectors.toList());

		correlationIdsToRemove.stream().forEach(correlationId

		-> holders.remove(correlationId));
	}
}
