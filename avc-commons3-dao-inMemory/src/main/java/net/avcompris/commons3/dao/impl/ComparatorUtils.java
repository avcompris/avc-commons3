package net.avcompris.commons3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Comparator;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.commons3.dao.EntityDto;

public abstract class ComparatorUtils {

	public static Comparator<EntityDto> getComparator_CREATED_AT() {

		return new Comparator<EntityDto>() {

			@Override
			public int compare(final EntityDto e1, final EntityDto e2) {
				return e1.getCreatedAt().compareTo(e2.getCreatedAt());
			}
		};
	}

	public static Comparator<EntityDto> getComparator_CREATED_AT_DESC() {

		return new Comparator<EntityDto>() {

			@Override
			public int compare(final EntityDto e1, final EntityDto e2) {
				return e2.getCreatedAt().compareTo(e1.getCreatedAt());
			}
		};
	}

	public static Comparator<EntityDto> getComparator_UPDATED_AT() {

		return new Comparator<EntityDto>() {

			@Override
			public int compare(final EntityDto e1, final EntityDto e2) {
				return e1.getUpdatedAt().compareTo(e2.getUpdatedAt());
			}
		};
	}

	public static Comparator<EntityDto> getComparator_UPDATED_AT_DESC() {

		return new Comparator<EntityDto>() {

			@Override
			public int compare(final EntityDto e1, final EntityDto e2) {
				return e2.getUpdatedAt().compareTo(e1.getUpdatedAt());
			}
		};
	}

	public static Comparator<EntityDto> getComparator_REVISION() {

		return new Comparator<EntityDto>() {

			@Override
			public int compare(final EntityDto e1, final EntityDto e2) {
				return Integer.compare(e1.getRevision(), e2.getRevision());
			}
		};
	}

	public static Comparator<EntityDto> getComparator_REVISION_DESC() {

		return new Comparator<EntityDto>() {

			@Override
			public int compare(final EntityDto e1, final EntityDto e2) {
				return Integer.compare(e2.getRevision(), e1.getRevision());
			}
		};
	}

	public static int compare_ASC(final String s1, final String s2) {

		checkNotNull(s1, "s1");
		checkNotNull(s2, "s2");

		return s1.compareToIgnoreCase(s2);
	}

	public static int compare_DESC(final String s1, final String s2) {

		checkNotNull(s1, "s1");
		checkNotNull(s2, "s2");

		return s2.compareToIgnoreCase(s1);
	}

	public static int compare_ASC(final int n1, final int n2) {

		return Integer.compare(n1, n2);
	}

	public static int compare_DESC(final int n1, final int n2) {

		return Integer.compare(n2, n1);
	}

	public static int compare_ASC(final long n1, final long n2) {

		return Long.compare(n1, n2);
	}

	public static int compare_DESC(final long n1, final long n2) {

		return Long.compare(n2, n1);
	}

	public static int compare_ASC(final boolean b1, final boolean b2) {

		return Boolean.compare(b1, b2);
	}

	public static int compare_DESC(final boolean b1, final boolean b2) {

		return Boolean.compare(b2, b1);
	}

	public static int compare_ASC(final DateTime dateTime1, final DateTime dateTime2) {

		checkNotNull(dateTime1, "dateTime1");
		checkNotNull(dateTime2, "dateTime2");

		return dateTime1.compareTo(dateTime2);
	}

	public static int compare_DESC(final DateTime dateTime1, final DateTime dateTime2) {

		checkNotNull(dateTime1, "dateTime1");
		checkNotNull(dateTime2, "dateTime2");

		return dateTime2.compareTo(dateTime1);
	}

	public static int compareNullable_ASC(@Nullable final String s1, @Nullable final String s2) {

		return s1 == null && s2 == null //
				? 0 //
				: s1 == null //
						? 1 //
						: s2 == null //
								? -1 //
								: s1.compareTo(s2);
	}

	public static int compareNullable_DESC(@Nullable final String s1, @Nullable final String s2) {

		return s1 == null && s2 == null //
				? 0 //
				: s2 == null //
						? 1 //
						: s1 == null //
								? -1 //
								: s2.compareTo(s1);
	}

	public static int compareNullable_ASC(@Nullable final Integer n1, @Nullable final Integer n2) {

		return n1 == null && n2 == null //
				? 0 //
				: n1 == null //
						? 1 //
						: n2 == null //
								? -1 //
								: n1.compareTo(n2);
	}

	public static int compareNullable_DESC(@Nullable final Integer n1, @Nullable final Integer n2) {

		return n1 == null && n2 == null //
				? 0 //
				: n2 == null //
						? 1 //
						: n1 == null //
								? -1 //
								: n2.compareTo(n1);
	}

	public static int compareNullable_ASC(@Nullable final DateTime dateTime1, @Nullable final DateTime dateTime2) {

		return dateTime1 == null && dateTime2 == null //
				? 0 //
				: dateTime1 == null //
						? 1 //
						: dateTime2 == null //
								? -1 //
								: dateTime1.compareTo(dateTime2);
	}

	public static int compareNullable_DESC(@Nullable final DateTime dateTime1, @Nullable final DateTime dateTime2) {

		return dateTime1 == null && dateTime2 == null //
				? 0 //
				: dateTime2 == null //
						? 1 //
						: dateTime1 == null //
								? -1 //
								: dateTime2.compareTo(dateTime1);
	}
}
