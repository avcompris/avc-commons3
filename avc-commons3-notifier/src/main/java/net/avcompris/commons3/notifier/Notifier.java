package net.avcompris.commons3.notifier;

import java.io.IOException;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.notifier.api.Notification;

public interface Notifier {

	void notify(String correlationId, User user, Notification notification) throws IOException;
}
