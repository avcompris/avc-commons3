package net.avcompris.commons3.notifier;

public interface NotifierFactory {

	Notifier createNotifier(String rabbitmqURL, String queueName);

	ErrorNotifier createErrorNotifier(String rabbitmqURL, String queueName);
}
