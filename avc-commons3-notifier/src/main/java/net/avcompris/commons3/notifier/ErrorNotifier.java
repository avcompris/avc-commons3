package net.avcompris.commons3.notifier;

import javax.annotation.Nullable;

public interface ErrorNotifier {

	void notifyError(@Nullable String correlationId, @Nullable String username, Throwable throwable);
}
