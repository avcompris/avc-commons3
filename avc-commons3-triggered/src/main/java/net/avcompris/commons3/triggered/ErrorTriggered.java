package net.avcompris.commons3.triggered;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.User;

public interface ErrorTriggered {

	String getName();

	/**
	 * If null, the thread is stopped.
	 */
	@Nullable
	User getThreadUser();

	/**
	 * If null, stop the thread.
	 */
	void setThreadUser(@Nullable String correlationId, @Nullable User user);

	void register(ErrorTriggeredAction action);
}
