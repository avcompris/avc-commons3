package net.avcompris.commons3.triggered;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.notifier.api.ErrorNotification;

@FunctionalInterface
public interface ErrorTriggeredAction {

	void execute(//
			@Nullable String correlationId, //
			@Nullable String username, //
			ErrorNotification notification //
	) throws ServiceException;
}
