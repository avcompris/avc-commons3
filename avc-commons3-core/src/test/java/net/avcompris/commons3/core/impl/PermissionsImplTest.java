package net.avcompris.commons3.core.impl;

import static net.avcompris.commons3.core.impl.PermissionsImpl.matches;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.core.impl.PermissionsImpl.CallContext;

public class PermissionsImplTest {

	@Test
	public void testMatches_REGULAR_GET_USER_ME() throws Exception {

		assertTrue(matches(DummyRole.REGULAR,

				new CallContext(DummyPermission.GET_USER_ME, "toto")));
	}

	@Test
	public void testMatches_USERMGR_GET_USER_ME() throws Exception {

		assertTrue(matches(DummyRole.USERMGR,

				new CallContext(DummyPermission.GET_USER_ME, "toto")));
	}
}
