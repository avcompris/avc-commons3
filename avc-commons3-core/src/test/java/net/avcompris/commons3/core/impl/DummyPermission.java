package net.avcompris.commons3.core.impl;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.EnumPermission;

public enum DummyPermission implements EnumPermission {

	ANY,

	SUPERADMIN,

	GET_MY_SESSION, TERMINATE_MY_SESSION,

	GET_APPINFO, CREATE_REGULAR_USER,

	GET_ROLES,

	QUERY_USERS,

	CREATE_FULL_USER, GET_USER, UPDATE_USER, DELETE_USER,

	GET_USER_SESSION, TERMINATE_USER_SESSION,

	UPDATE_USER_ROLE,

	GET_USER_ME, UPDATE_USER_ME,

	QUERY_CALORIES_ENTRIES,

	CREATE_CALORIES_ENTRY, GET_CALORIES_ENTRY, UPDATE_CALORIES_ENTRY, DELETE_CALORIES_ENTRY,

	QUERY_MY_CALORIES_ENTRIES,

	GET_MY_CALORIES_ENTRY, UPDATE_MY_CALORIES_ENTRY, DELETE_MY_CALORIES_ENTRY,;

	@Override
	public boolean isSuperadminPermission() {

		return this == SUPERADMIN;
	}

	@Override
	public boolean isAnyUserPermission() {

		return this == ANY;
	}

	@Override
	@Nullable
	public String getExpression() {

		return null;
	}
}
