package net.avcompris.commons3.core.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Maps.newHashMap;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.annotation.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import net.avcompris.commons3.api.EnumPermission;
import net.avcompris.commons3.api.EnumRole;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.UnauthorizedException;
import net.avcompris.commons3.core.Permissions;

@Component
public final class PermissionsImpl implements Permissions {

	@Autowired
	public PermissionsImpl() {

	}

	static class CallContext {

		private final EnumPermission declaredPermissionType;

		@SuppressWarnings("unused")
		private final String username;

		@SuppressWarnings("unused")
		private final Object[] contextNameValuePairs;

		CallContext(final EnumPermission declaredPermissionType, final String username,
				final Object... contextNameValuePairs) {

			this.declaredPermissionType = checkNotNull(declaredPermissionType, "declaredPermissionType");
			this.username = checkNotNull(username, "username");
			this.contextNameValuePairs = checkNotNull(contextNameValuePairs, "contextNameValuePairs");
		}

		public boolean permissionMatches(final EnumPermission permission) throws UnauthorizedException {

			checkNotNull(permission, "permission");

			if (permission.isSuperadminPermission()) {

				return true; // SUPERADMIN can do all
			}

			if (permission == declaredPermissionType) {

				return true;
			}

			return false;
		}
	}

	static boolean matches(final EnumRole role, final CallContext callContext) throws UnauthorizedException {

		for (final EnumRole superRole : role.getSuperRoles()) {

			if (matches(superRole, callContext)) {

				return true;
			}
		}

		for (final EnumPermission permission : role.getPermissions()) {

			if (callContext.permissionMatches(permission)) {

				return true;
			}
		}

		return false;
	}

	@Override
	public void assertAuthorized(final String correlationId, final User user, final Object... contextNameValuePairs)
			throws UnauthorizedException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(contextNameValuePairs, "contextNameValuePairs");

		// Implementation note: This method should look for the implemented service
		// interface, such as "UsersService", etc., then fetch the outer-most
		// method, and infer the required EnumPermission, then check the compatibility
		// with the UserPermissions passed in the User object.

		final EnumPermission declaredPermissionType = getCurrentServiceMethodDeclaredPermissionType();

		if (declaredPermissionType.isAnyUserPermission()) {
			return;
		}

		final String username = user.getUsername();

		final CallContext callContext = new CallContext(declaredPermissionType, username, contextNameValuePairs);

		final EnumRole role = user.getRole();

		if (matches(role, callContext)) {

			return;
		}

		System.err.println("ERROR *** Could not find permissionType: " + declaredPermissionType + " for username: "
				+ user.getUsername());

		for (final EnumRole superRole : role.getSuperRoles()) {

			System.err.println("    Found: " + user.getUsername() + "/" + superRole);
		}

		for (final EnumPermission permission : role.getPermissions()) {

			System.err.println("    Found: " + user.getUsername() + "/" + permission);
		}

		throw new UnauthorizedException(declaredPermissionType + " (" + username + ")");
	}

	private static EnumPermission getCurrentServiceMethodDeclaredPermissionType() {

		for (final StackTraceElement ste : Thread.currentThread().getStackTrace()) {

			final String className = ste.getClassName();

			final Class<?> clazz;

			try {

				clazz = Class.forName(className);

			} catch (final ClassNotFoundException e) {

				throw new RuntimeException(e);
			}

			// See: ServiceImplClassesTest in shared3-ut for a check on this annotation.
			//
			if (clazz.getAnnotation(Service.class) == null) {
				continue;
			}

			final String methodName = ste.getMethodName();

			for (Class<?> c = clazz; c != null; c = c.getSuperclass()) {

				for (final Class<?> i : c.getInterfaces()) {

					for (final Method method : i.getMethods()) {

						if (!methodName.contentEquals(method.getName())) {
							continue;
						}

						for (final Annotation annotation : method.getAnnotations()) {

							final EnumPermission permissionType = loadAnnotatedPermissionType(annotation);

							if (permissionType != null) {
								return permissionType;
							}
						}
					}
				}
			}

			throw new IllegalStateException(
					"Cannot find @PermissionTypes annotation for: " + className + "." + methodName + "()");
		}

		throw new IllegalStateException("Cannot find @Service + @PermissionTypes annotations for current call.");
	}

	private static final class AnnotatedPermissionType {

		private static final AnnotatedPermissionType NULL_INSTANCE = new AnnotatedPermissionType(null);

		private static final Map<Annotation, AnnotatedPermissionType> ANNOTATED_PERMISSION_TYPES = newHashMap();

		@Nullable
		public final EnumPermission permissionType;

		private AnnotatedPermissionType(@Nullable final EnumPermission permissionType) {

			this.permissionType = permissionType;
		}

		@Nullable
		public static AnnotatedPermissionType getCached(final Annotation annotation) {

			return ANNOTATED_PERMISSION_TYPES.get(annotation);
		}

		@Nullable
		public static EnumPermission putNullPermissionTypeFor(final Annotation annotation) {

			ANNOTATED_PERMISSION_TYPES.put(annotation, NULL_INSTANCE);

			return NULL_INSTANCE.permissionType;
		}

		@Nullable
		public static EnumPermission putPermissionTypeFor(final Annotation annotation,
				@Nullable final EnumPermission permissionType) {

			final AnnotatedPermissionType annotated = new AnnotatedPermissionType(permissionType);

			ANNOTATED_PERMISSION_TYPES.put(annotation, annotated);

			return permissionType;
		}
	}

	@Nullable
	private static EnumPermission loadAnnotatedPermissionType(final Annotation annotation) {

		final AnnotatedPermissionType cached = AnnotatedPermissionType.getCached(annotation);

		if (cached != null) {
			return cached.permissionType;
		}

		final Method method;

		try {

			method = annotation.annotationType().getMethod("value");

		} catch (final NoSuchMethodException e) {

			return AnnotatedPermissionType.putNullPermissionTypeFor(annotation);
		}

		final Class<?> returnType = method.getReturnType();

		if (returnType == null) {

			return AnnotatedPermissionType.putNullPermissionTypeFor(annotation);

		} else if (EnumPermission.class.isAssignableFrom(returnType)) {

			final EnumPermission permissionType = extractPermissionTypeValueFrom(annotation, method);

			return AnnotatedPermissionType.putPermissionTypeFor(annotation, permissionType);

		} else if (EnumPermission[].class.isAssignableFrom(returnType)) {

			final EnumPermission[] permissionTypes = extractPermissionTypeValuesFrom(annotation, method);

			if (permissionTypes == null || permissionTypes.length == 0) {
				return AnnotatedPermissionType.putNullPermissionTypeFor(annotation);
			}

			checkState(permissionTypes.length == 1, //
					"PermissionTypes.length should be 1, but was: %s", permissionTypes.length);

			return AnnotatedPermissionType.putPermissionTypeFor(annotation, permissionTypes[0]);

		} else {

			return AnnotatedPermissionType.putNullPermissionTypeFor(annotation);
		}
	}

	@Nullable
	private static EnumPermission extractPermissionTypeValueFrom(final Annotation annotation, final Method method) {

		final Object result;

		try {

			result = method.invoke(annotation);

		} catch (final InvocationTargetException | IllegalAccessException e) {

			return null;
		}

		return (EnumPermission) result;
	}

	@Nullable
	private static EnumPermission[] extractPermissionTypeValuesFrom(final Annotation annotation, final Method method) {

		final Object result;

		try {

			result = method.invoke(annotation);

		} catch (final InvocationTargetException | IllegalAccessException e) {

			return null;
		}

		return (EnumPermission[]) result;
	}
}
