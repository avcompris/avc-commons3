package net.avcompris.commons3.core.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.collect.Iterables;

public final class SortBysParser<T extends Enum<?>> {

	private final Class<T> clazz;

	private final Map<String, T> byLowercases = newHashMap();

	public SortBysParser(final Class<T> clazz, final T[] values) {

		this.clazz = checkNotNull(clazz, "clazz");

		checkNotNull(values, "values");

		for (final T value : values) {

			final String name = value.toString();

			checkArgument(name.startsWith("SORT_BY_"), //
					"SortBy item should start with \"SORT_BY_\", but was: %s", name);

			final String lowercase = name.replace("_", "").toLowerCase(Locale.ENGLISH);

			byLowercases.put(lowercase, value);
		}
	}

	@Nullable
	public T[] parse(@Nullable final String s) {

		if (isBlank(s)) {
			return null;
		}

		final List<T> sortBys = new ArrayList<>();

		final StringBuilder sb = new StringBuilder();

		boolean asc = true;

		final String trim = s.trim().replace("_", "").toLowerCase(Locale.ENGLISH);

		for (final char c : trim.toCharArray()) {

			if (c == '+') {

				if (sb.length() != 0) {

					handleValue(sortBys, asc, sb.toString());

					sb.setLength(0);
				}

				asc = true;

			} else if (c == '-') {

				if (sb.length() != 0) {

					handleValue(sortBys, asc, sb.toString());

					sb.setLength(0);
				}

				asc = false;

			} else if (c == ' ' || c == ';' || c == ',' || c == '&') {

				if (sb.length() != 0) {

					handleValue(sortBys, asc, sb.toString());

					sb.setLength(0);

					asc = true;
				}

			} else {

				sb.append(c);
			}
		}

		if (sb.length() != 0) {

			handleValue(sortBys, asc, sb.toString());
		}

		return sortBys.isEmpty() ? null : Iterables.toArray(sortBys, clazz);
	}

	private void handleValue(final List<T> sortBys, final boolean asc, final String s) {

		final String lowercase;

		if (s.endsWith("desc")) {

			lowercase = "sortby" + substringBeforeLast(s, "desc") + (asc ? "desc" : "");

		} else {

			lowercase = "sortby" + s + (asc ? "" : "desc");
		}

		final T value = byLowercases.get(lowercase);

		checkArgument (value != null, //
				"Unknown SortBy: %s", lowercase);

		sortBys.add(value);
	}
}
