package net.avcompris.commons3.core.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ConcurrentModificationException;

import javax.annotation.Nullable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.avcompris.commons3.api.EntitiesQuery;
import net.avcompris.commons3.api.exception.DaoException;
import net.avcompris.commons3.api.exception.IllegalStateUpdateException;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.dao.exception.ForeignEntityViolationException;
import net.avcompris.commons3.utils.Clock;

public abstract class AbstractServiceImpl {

	protected final Permissions permissions;
	protected final Clock clock;

	private static final Log logger = LogFactory.getLog(AbstractServiceImpl.class);

	protected AbstractServiceImpl(final Permissions permissions, final Clock clock) {

		this.permissions = checkNotNull(permissions, "permissions");
		this.clock = checkNotNull(clock, "clock");
	}

	protected static void wrap(final ActionVoid action) throws ServiceException {

		try {

			action.execute();

		} catch (final ConcurrentModificationException e) {

			throw new IllegalStateUpdateException(e);

		} catch (final SQLException | IOException e) {

			throw new DaoException(e);
		}
	}

	protected static <T> T wrap(final Action<T> action) throws ServiceException {

		try {

			return action.execute();

		} catch (final ConcurrentModificationException e) {

			throw new IllegalStateUpdateException(e);

		} catch (final SQLException | IOException e) {

			throw new DaoException(e);
		}
	}

	protected static void unchecked(final ActionVoid action) {

		try {

			action.execute();

		} catch (final ServiceException e) {

			throw new RuntimeException(e);

		} catch (final SQLException | IOException e) {

			throw new RuntimeException(e);
		}
	}

	@FunctionalInterface
	protected interface ActionVoid {

		void execute() throws SQLException, IOException, ServiceException;
	}

	@FunctionalInterface
	protected interface Action<T> {

		T execute() throws SQLException, IOException, ServiceException;
	}

	@FunctionalInterface
	protected interface ServiceAction<T> {

		T execute() throws ServiceException;
	}

	protected final int getQueryStart(@Nullable final EntitiesQuery<?, ?> query, final int defaultValue) {

		final Integer start = (query == null) ? null : query.getStart();

		return (start != null) ? start : defaultValue;
	}

	protected final int getQueryLimit(@Nullable final EntitiesQuery<?, ?> query, final int defaultValue) {

		final Integer limit = (query == null) ? null : query.getLimit();

		return (limit != null) ? limit : defaultValue;
	}

	@FunctionalInterface
	protected interface DaoAction<T> {

		T action() throws SQLException, IOException, ServiceException;
	}

	@FunctionalInterface
	protected interface DaoActionWithForeignEntity<T> {

		T action() throws SQLException, IOException, ForeignEntityViolationException;
	}

	protected static <T> T retryDaoUntil(final long timeoutMs, final long delayMs, final DaoAction<T> action)
			throws SQLException, IOException, ServiceException {

		return retryDaoUntil(timeoutMs, delayMs, true, action);
	}

	protected static <T> T retryDaoWithForeignEntityUntil(final long timeoutMs, final long delayMs,
			final DaoActionWithForeignEntity<T> action)
			throws SQLException, IOException, ForeignEntityViolationException {

		return retryDaoWithForeignEntityUntil(timeoutMs, delayMs, true, action);
	}

	protected static <T> T retryDaoUntil(final long timeoutMs, final long delayMs,
			final boolean throwExceptionInCaseOfTimeout, final DaoAction<T> action)
			throws SQLException, IOException, ServiceException {

		checkNotNull(action, "action");

		for (final long startMs = System.currentTimeMillis(); System.currentTimeMillis() < startMs + timeoutMs;) {

			T result = null;

			try {

				result = action.action();

			} catch (final SQLException | IOException | RuntimeException | Error e) {

				throw e;

			} catch (final Exception e) {

				throw new RuntimeException(e);
			}

			if (result != null) {

				return result;
			}

			try {

				Thread.sleep(delayMs);

			} catch (final InterruptedException e) {

				e.printStackTrace();

				// do nothing
			}
		}

		logger.error("A timeout occurs (" + timeoutMs + " ms)");

		if (throwExceptionInCaseOfTimeout) {

			throw new RuntimeException("Timeout after: " + timeoutMs + " ms");

		} else {

			return null;
		}
	}

	protected static <T> T retryDaoWithForeignEntityUntil(final long timeoutMs, final long delayMs,
			final boolean throwExceptionInCaseOfTimeout, final DaoActionWithForeignEntity<T> action)
			throws SQLException, IOException, ForeignEntityViolationException {

		checkNotNull(action, "action");

		for (final long startMs = System.currentTimeMillis(); System.currentTimeMillis() < startMs + timeoutMs;) {

			T result = null;

			try {

				result = action.action();

			} catch (final SQLException | IOException | RuntimeException | ForeignEntityViolationException | Error e) {

				throw e;

			} catch (final Exception e) {

				throw new RuntimeException(e);
			}

			if (result != null) {

				return result;
			}

			try {

				Thread.sleep(delayMs);

			} catch (final InterruptedException e) {

				e.printStackTrace();

				// do nothing
			}
		}

		logger.error("A timeout occurs (" + timeoutMs + " ms)");

		if (throwExceptionInCaseOfTimeout) {

			throw new RuntimeException("Timeout after: " + timeoutMs + " ms");

		} else {

			return null;
		}
	}
}
