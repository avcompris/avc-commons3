package net.avcompris.commons3.core;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.joda.time.DateTimeZone.UTC;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import net.avcompris.commons3.types.DateTimeHolder;

public final class DateTimeHolderImpl implements DateTimeHolder {

	private static final long serialVersionUID = 1160765887175893130L;

	private static final DateTimeFormatter ISO_FORMATTER = ISODateTimeFormat.dateTime();

	private final DateTime dateTime;
	private final String text;
	private final long ms;

	private DateTimeHolderImpl(final DateTime dateTime) {

		this.dateTime = checkNotNull(dateTime, "dateTime");

		text = dateTime.withZone(UTC).toString(ISO_FORMATTER);

		ms = dateTime.getMillis();
	}

	@Nullable
	public static DateTimeHolder toDateTimeHolderOrNull(@Nullable final DateTime dateTime) {

		if (dateTime == null) {
			return null;
		}

		return toDateTimeHolder(dateTime);
	}

	@Nullable
	public static DateTimeHolder toDateTimeHolder(final DateTime dateTime) {

		checkNotNull(dateTime, "dateTime");

		return new DateTimeHolderImpl(dateTime);
	}

	@Nullable
	public static DateTimeHolder toDateTimeHolder(final long timestamp) {

		return new DateTimeHolderImpl(new DateTime(timestamp).withZone(UTC));
	}

	@Override
	public String getDateTime() {
		return text;
	}

	@Override
	public long getTimestamp() {
		return ms;
	}

	@Override
	public String toString() {

		return "{dateTime: \"" + text + "\", timestamp: " + ms + "}";
	}

	@Override
	public int hashCode() {

		return dateTime.hashCode();
	}

	@Override
	public boolean equals(@Nullable final Object o) {

		if (o == null || !(o instanceof DateTimeHolder)) {
			return false;
		}

		return toString().contentEquals(o.toString());
	}

	public static DateTime toDateTimeOrNull(@Nullable final DateTimeHolder holder) {

		if (holder == null) {
			return null;
		}

		return toDateTime(holder);
	}

	public static DateTime toDateTime(final DateTimeHolder holder) {

		checkNotNull(holder, "holder");

		checkArgument(holder instanceof DateTimeHolderImpl, //
				"holder should be of type %s, but was: %s", //
				DateTimeHolderImpl.class.getName(), holder.getClass().getName());

		return ((DateTimeHolderImpl) holder).dateTime;
	}

	@Override
	public int compareTo(@Nullable final DateTimeHolder d) {

		if (d == null) {

			return dateTime.compareTo(null);

		} else if (d instanceof DateTimeHolderImpl) {

			return dateTime.compareTo(((DateTimeHolderImpl) d).dateTime);

		} else {

			throw new NotImplementedException("d.class: " + d.getClass().getName());
		}
	}

	@Override
	public Object clone() {

		return toDateTimeHolder(dateTime);
	}
}
