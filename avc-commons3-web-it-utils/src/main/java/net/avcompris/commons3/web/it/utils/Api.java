package net.avcompris.commons3.web.it.utils;

import net.avcompris.commons3.api.User;

public interface Api {

	User user(String authorization);

	<U, V extends U> U forge(Class<U> serviceClass, String apiBaseURL, Class<V> serviceClientClass);
}
