package net.avcompris.commons3.web.it.utils;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Maps.newHashMap;
import static io.restassured.RestAssured.given;
import static net.avcompris.commons3.api.tests.ControllerContextUtils.extractControllerContext;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.DELETE;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.GET;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.POST;
import static net.avcompris.commons3.api.tests.TestsSpec.HttpMethod.PUT;
import static net.avcompris.commons3.web.it.utils.ExpressionUtils.process;
import static net.avcompris.commons3.web.it.utils.JSONUtils.parseJSON;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.BeforeEach;

import com.google.common.collect.ImmutableMap;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.avcompris.commons3.api.tests.AbstractApiTest;
import net.avcompris.commons3.api.tests.TestsSpec.AuthenticationSpec;
import net.avcompris.commons3.api.tests.TestsSpec.Data;
import net.avcompris.commons3.api.tests.TestsSpec.HttpMethod;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.web.AbstractController;

public abstract class AbstractWebApiTest extends AbstractApiTest {

	private String baseURL;

	private final Map<Class<? extends AbstractController>, String> moreRestAssured = newHashMap();

	protected AbstractWebApiTest(final TestSpec spec, //
			@Nullable final String superadminAuthorization) {

		super(spec, superadminAuthorization);

		final Class<? extends AbstractWebApiTest> clazz = this.getClass();

		final RestAssured restAssuredAnnotation = clazz.getAnnotation(RestAssured.class);

		checkState(restAssuredAnnotation != null, //
				"Class should be annotated with @RestAssured: %s", clazz.getName());

		try {

			baseURL = process(restAssuredAnnotation.value());

		} catch (final IOException e) {

			throw new RuntimeException(e);
		}
	}

	@Override
	protected final StepExecutionResult execute(final int stepIndex, final StepExecution step) throws Exception {

		final String uri = step.getPath();

		final HttpMethod httpMethod = step.getHttpMethod();

		final String baseURL = moreRestAssured.entrySet().stream()

				.filter(entry -> extractControllerContext(httpMethod, uri, entry.getKey()) != null)

				.map(entry -> entry.getValue())

				.findFirst().orElse(null);

		AbstractWebTest.setUpRestAssured(baseURL != null ? baseURL : this.baseURL);

		final RequestSpecification request = given();

		final AuthenticationSpec authenticationSpec = step.getAuthentication();

		if (authenticationSpec != null) {

			request.header("Authorization", superadminAuthorization);
		}

		final String path;
		final Map<String, String> queryParams;

		if (uri.contains("?")) {

			path = substringBefore(uri, "?");

			final String query = substringAfter(uri, "?");

			if (query.contains("&")) {
				throw new NotImplementedException("uri: " + uri);
			}

			queryParams = ImmutableMap.<String, String>builder().put( //
					substringBefore(query, "="), //
					substringAfter(query, "=").replace('+', ' ') //
			).build();

		} else {

			path = uri;
			queryParams = null;
		}

		final Response response;

		if (httpMethod == GET) {

			if (queryParams != null) {

				request.queryParams(queryParams);
			}

			response = request

					.when().get(path);

		} else if (httpMethod == POST) {

			final Map<String, Object> map = newHashMap();

			for (final Data data : step.getData()) {

				map.put(data.getName(), data.getValue());
			}

			response = request

					.header("Content-Type", "application/json")

					.body(map)

					.when().post(path);

		} else if (httpMethod == PUT) {

			final Map<String, Object> map = newHashMap();

			for (final Data data : step.getData()) {

				map.put(data.getName(), data.getValue());
			}

			response = request

					.header("Content-Type", "application/json")

					.body(map)

					.when().put(path);

		} else if (httpMethod == DELETE) {

			response = request

					.when().delete(path);

		} else {

			throw new NotImplementedException("httpMethod: " + httpMethod);
		}

		response.then();

		final int statusCode = response.then().extract().statusCode();

		final String resultAsString = response.then().extract().asString();

		final Class<?> returnType = step.getReturnType();

		final Object dataBean;

		if (returnType == null && isBlank(resultAsString)) {

			dataBean = null;

		} else if (returnType == null) {

			dataBean = new JSONParser().parse(resultAsString);

		} else {

			dataBean = parseJSON(resultAsString, returnType);
		}

		return new StepExecutionResult(statusCode, dataBean);
	}

	/**
	 * Use the "@RestAssured" annotation set on the test class (e.g. AppInfoTest) to
	 * retrieve the env property to use for the API baseURL (e.g.
	 * "customers.baseURL"), and then call
	 * {@link AbstractWebTest#setUpRestAssured(String)}
	 */
	@BeforeEach
	public final void setUpRestAssured() throws Exception {

		AbstractWebTest.setUpRestAssured(baseURL);
	}

	protected final void moreRestAssured(final String expression,
			final Class<? extends AbstractController> controllerClass0) {

		try {

			moreRestAssured.putIfAbsent(controllerClass0, process(expression));

		} catch (final IOException e) {

			throw new RuntimeException(e);
		}
	}
}