package net.avcompris.commons3.web.it.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.it.utils.IntegrationTestUtils.getTestProperty;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import java.io.IOException;

import org.apache.commons.lang3.NotImplementedException;

public abstract class ExpressionUtils {

	public static String process(final String expression) throws IOException {

		checkNotNull(expression, "expression");

		final String processed;

		if (!expression.contains("${")) {

			processed = expression;

		} else if (expression.startsWith("${") && expression.endsWith("}")) {

			final String propertyName = substringBetween(expression, "${", "}");

			if (propertyName.contains("$")) {

				throw new NotImplementedException("propertyName: " + propertyName + ", in expression: " + expression);
			}

			processed = getTestProperty(propertyName);

		} else {

			throw new NotImplementedException("expression: " + expression);
		}

		return processed;
	}
}
