package net.avcompris.commons3.web.it.utils;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface RestAssured {

	/**
	 * The name of the env property to use as the Base URL for Rest Assured. e.g.
	 * "my_service.baseURL"
	 */
	String value();
}
