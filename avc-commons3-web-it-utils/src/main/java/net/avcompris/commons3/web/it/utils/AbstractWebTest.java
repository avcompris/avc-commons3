package net.avcompris.commons3.web.it.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static net.avcompris.commons3.web.it.utils.ExpressionUtils.process;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;

public abstract class AbstractWebTest {

	private static String CURRENT_BASE_URL;

	/**
	 * Use the "@RestAssured" annotation set on the test class (e.g. AppInfoTest) to
	 * retrieve the env property to use for the API baseURL (e.g.
	 * "customers.baseURL"), and then call {@link #setUpRestAssured(String)}
	 */
	@BeforeEach
	public final void setUpRestAssured() throws Exception {

		final Class<? extends AbstractWebTest> clazz = this.getClass();

		final RestAssured restAssuredAnnotation = clazz.getAnnotation(RestAssured.class);

		checkState(restAssuredAnnotation != null, //
				"Class should be annotated with @RestAssured: %s", clazz.getName());

		final String baseURL = process(restAssuredAnnotation.value());

		setUpRestAssured(baseURL);
	}

	/**
	 * Set up Rest Assured with the env property to use for the API baseURL (e.g.
	 * "${customers.baseURL}"), which should have a value such as
	 * "http://localhost:8080/api/v1".
	 * 
	 * Please use {@link ExpressionUtils#process(String)} if you want to process an
	 * expression that uses variables.
	 */
	public static void setUpRestAssured(final String baseURL) throws IOException {

		checkNotNull(baseURL, "baseURL");

		if (CURRENT_BASE_URL != null && baseURL.contentEquals(CURRENT_BASE_URL)) {
			return;
		}

		CURRENT_BASE_URL = baseURL;

		System.out.println("setUpRestAssured(): baseURL: " + baseURL);

		final String baseURI = substringBefore(baseURL, "//") + "//" + substringBetween(baseURL, "//", ":");

		System.out.println("  baseURI: " + baseURI);

		final String suffix = substringAfter(substringAfter(baseURL, "//"), ":");

		final int port = Integer.parseInt(suffix.contains("/") ? substringBefore(suffix, "/") : suffix);

		System.out.println("  port: " + port);

		io.restassured.RestAssured.baseURI = baseURI;
		io.restassured.RestAssured.port = port;
	}
}
