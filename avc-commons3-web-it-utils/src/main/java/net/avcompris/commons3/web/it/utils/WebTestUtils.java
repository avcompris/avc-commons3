package net.avcompris.commons3.web.it.utils;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons3.api.EnumRole;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.client.impl.AbstractClient;

public abstract class WebTestUtils {

	public static Api api(final String apiBaseURL) throws IOException {

		checkNotNull(apiBaseURL, "apiBaseURL");

		return new ApiImpl(apiBaseURL);
	}

	private static final class ApiImpl implements Api {

		private final String apiBaseURL;

		public ApiImpl(final String apiBaseURL) {

			this.apiBaseURL = checkNotNull(apiBaseURL, "apiBaseURL");
		}

		@Override
		public User user(final String authorization) {

			return new UserImpl(authorization);
		}

		@Override
		public <U, V extends U> U forge(final Class<U> serviceClass, final String apiBaseURL,
				final Class<V> serviceClientClass) {

			checkNotNull(serviceClass, "serviceClass");
			checkNotNull(apiBaseURL, "apiBaseURL");
			checkNotNull(serviceClientClass, "serviceClientClass");

			checkArgument(apiBaseURL.startsWith("${") && apiBaseURL.endsWith("}"), //
					"apiBaseURL should be of the form \"${...}\", e.g. \"${users.apiBaseURL}\", but was: %s",
					apiBaseURL);

			final String propertyName = substringBetween(apiBaseURL, "${", "}");

			System.setProperty(propertyName, this.apiBaseURL);

			checkArgument(AbstractClient.class.isAssignableFrom(serviceClientClass), //
					"serviceClientClass shoud extend AbstractClient, but was: %s", serviceClientClass.getName());

			final SessionPropagator sessionPropagator = new SessionPropagator();

			final V client;

			try {

				client = serviceClientClass //
						.getConstructor(SessionPropagator.class) //
						.newInstance(sessionPropagator);

			} catch (final InstantiationException | IllegalAccessException | InvocationTargetException
					| NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			final Object proxy = Proxy.newProxyInstance( //
					serviceClass.getClassLoader(), //
					new Class<?>[] { serviceClass }, //
					new InvocationHandler() {

						@Override
						public Object invoke(final Object proxy, final Method method, final Object[] args)
								throws Throwable {

							if (serviceClass.equals(method.getDeclaringClass())) {

								return invokeServiceMethod(sessionPropagator, client, method, args);

							} else {

								throw new NotImplementedException(
										"method: " + method.getName() + "(), serviceClass: " + serviceClass.getName());
							}
						}
					});

			return serviceClass.cast(proxy);
		}

		private Object invokeServiceMethod(final SessionPropagator sessionPropagator, final Object client,
				final Method method, final Object[] args) throws Throwable {

			checkArgument(args.length >= 2, //
					"args.length should be >= 2, but was: %s", args.length);

			checkArgument(args[0] == null, //
					"args[0] should be null, but was: %s", args[0]);

			final String correlationId = "C-" + randomAlphanumeric(20);

			args[0] = correlationId;

			final User user = (User) args[1];

			checkArgument(user != null, "user arg should not be null");

			checkArgument(user instanceof UserImpl, //
					"user arg should be obtained through the \"Api#user()\" method, but was: %s",
					user.getClass().getName());

			final String authorization = ((UserImpl) user).getAuthorization();

			sessionPropagator.setAuthorizationHeader(authorization);

			return method.invoke(client, args);
		}
	}

	private static final class UserImpl implements User {

		private final String authorization;

		public UserImpl(final String authorization) {

			this.authorization = checkNotNull(authorization, "authorization");
		}

		@Override
		public String getUsername() {

			throw new NotImplementedException("");
		}

		@Override
		public EnumRole getRole() {

			throw new NotImplementedException("");
		}

		public String getAuthorization() {

			return authorization;
		}
	}
}
