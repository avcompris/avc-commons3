package net.avcompris.commons3.web.it.utils;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.StringUtils.uncapitalize;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Set;

import org.apache.commons.lang3.NotImplementedException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.avcompris.commons3.core.DateTimeHolderImpl;
import net.avcompris.commons3.types.DateTimeHolder;

public abstract class JSONUtils {

	public static <T> T parseJSON(final String json, final Class<T> clazz) throws IOException, ParseException {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		// final ObjectMapper mapper = new ObjectMapper();
		//
		// final SimpleModule module = new SimpleModule();
		//
		// addDeserializer(module, clazz);
		//
		// mapper.registerModule(module);
		//
		// return mapper.readValue(json, clazz);

		final JSONParser parser = new JSONParser();

		final JSONObject jsonObject = (JSONObject) parser.parse(json);

		return parseJSON(jsonObject, clazz);
	}

	public static <T> T parseJSON(final JSONObject json, final Class<T> clazz) throws IOException {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		checkArgument(clazz.isInterface(), "clazz should be an interface: %s", clazz.getName());

		final ClassLoader classLoader = // Thread.currentThread().getContextClassLoader();
				clazz.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, new Class<?>[] { //
				clazz //
		}, new InvocationHandler() {

			@Override
			public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {

				final int parameterCount = method.getParameterCount();

				final String methodName = method.getName();

				if (parameterCount == 0) {

					if ("getClass".contentEquals(methodName)) {

						throw new NotImplementedException("method: " + method);

					} else if ("hashCode".contentEquals(methodName)) {

						return json.hashCode();

					} else if ("toString".contentEquals(methodName)) {

						return json.toJSONString();
					}

					final String suffix;

					if (methodName.startsWith("get")) {

						suffix = methodName.substring(3);

					} else if (methodName.startsWith("is")) {

						suffix = methodName.substring(2);

					} else {

						throw new NotImplementedException("method: " + method);
					}

					final String propertyName = uncapitalize(suffix);

					final Object value = json.get(propertyName);

					final Class<?> returnType = method.getReturnType();

					if (returnType == null) {
						throw new NotImplementedException("method: " + method);
					}

					if (boolean.class.equals(returnType)) {

						if (value instanceof Boolean) {

							return (boolean) value;

						} else {

							throw new NotImplementedException("property \"" + propertyName + "\" should be a boolean: "
									+ value + ", but was: " + value.getClass().getName());
						}

					} else if (int.class.equals(returnType)) {

						if (value instanceof Long) {

							return ((Long) value).intValue();

						} else if (value instanceof Integer) {

							return (int) value;

						} else {

							throw new NotImplementedException(
									"property \"" + propertyName + "\" should be a long or an int: " + value
											+ ", but was: " + value.getClass().getName());
						}

					} else if (String.class.equals(returnType)) {

						if (value == null) {

							return null;

						} else if (value instanceof String) {

							return (String) value;

						} else {

							throw new NotImplementedException("property \"" + propertyName + "\" should be a String: "
									+ value + ", but was: " + value.getClass().getName());
						}

					} else if (DateTimeHolder.class.equals(returnType)) {

						if (value == null) {

							return null;

						} else if (value instanceof JSONObject) {

							@SuppressWarnings("unused")
							final String dateTime = (String) ((JSONObject) value).get("dateTime");

							final long timestamp = (Long) ((JSONObject) value).get("timestamp");

							return DateTimeHolderImpl.toDateTimeHolder(timestamp);

						} else {

							throw new NotImplementedException(
									"property \"" + propertyName + "\" should be a DateTimeHolder: " + value);
						}

					} else if (returnType.isEnum()) {

						if (value == null) {

							return null;

						} else if (value instanceof String) {

							final String name = (String) value;

							for (final Object constant : returnType.getEnumConstants()) {

								final Enum<?> enumConstant = (Enum<?>) constant;

								if (name.equals(enumConstant.name())) {

									return enumConstant;
								}
							}

							throw new IOException("Illegal Enum value for \"" + propertyName + "\": " + value
									+ " (Enum class: " + returnType.getName() + ")");

						} else {

							throw new NotImplementedException("property \"" + propertyName + "\" should be a String: "
									+ value + ", but was: " + value.getClass().getName());
						}

					} else if (returnType.isInterface()) {

						if (value == null) {

							return null;

						} else if (value instanceof JSONObject) {

							return parseJSON((JSONObject) value, returnType);

						} else {

							throw new NotImplementedException("property \"" + propertyName
									+ "\" should be a JSONObject, but was: " + value.getClass().getName());

						}

					} else if (returnType.isArray()) {

						if (value == null) {

							return null;

						} else if (value instanceof JSONArray) {

							final JSONArray jsonArray = (JSONArray) value;

							final int count = jsonArray.size();

							final Class<?> componentType = returnType.getComponentType();

							final Object array = Array.newInstance(componentType, count);

							for (int i = 0; i < count; ++i) {

								final Object item = parseJSON((JSONObject) jsonArray.get(i), componentType);

								Array.set(array, i, item);
							}

							return array;

						} else {

							throw new NotImplementedException("property \"" + propertyName
									+ "\" should be an array, but was: " + value.getClass().getName());
						}
					}

				} else if (parameterCount == 1 //
						&& "equals".contentEquals(methodName) //
						&& Object.class.equals(method.getParameterTypes()[0])) {

					final Object arg = args[0];

					if (arg == null || !clazz.isInstance(arg)) {
						return false;
					}

					final Set<Method> getters1 = extractGetters(clazz);
					final Set<Method> getters2 = extractGetters(arg.getClass());

					if (getters1.size() != getters2.size()) {
						return false;
					}

					for (final Method getter : getters1) {

						final Object result1 = getter.invoke(proxy);
						final Object result2 = getter.invoke(arg);

						if (result1 == null && result2 == null) {
							continue;
						} else if (result1 == null || result2 == null) {
							return false;
						}

						final Class<?> returnType = getter.getReturnType();

						if (returnType.isArray()) {

							final int size1 = Array.getLength(result1);
							final int size2 = Array.getLength(result2);

							if (size1 != size2) {
								return false;
							}

							for (int i = 0; i < size1; ++i) {

								final Object item1 = Array.get(result1, i);
								final Object item2 = Array.get(result2, i);

								if (item1 == null && item2 == null) {
									continue;
								} else if (item1 == null || item2 == null) {
									return false;
								} else if (!item1.equals(item2)) {
									return false;
								}
							}

						} else if (!result1.equals(result2)) {
							return false;
						}
					}

					return true;
				}

				throw new NotImplementedException("method: " + method);
			}
		});

		return clazz.cast(proxy);
	}

	private static Set<Method> extractGetters(final Class<?> clazz) {

		final Set<Method> getters = newHashSet();

		for (final Method method : clazz.getMethods()) {

			final int parameterCount = method.getParameterCount();

			final String methodName = method.getName();

			if (parameterCount != 0 //
					|| "notify".contentEquals(methodName) //
					|| "notifyAll".contentEquals(methodName) //
					|| "wait".contentEquals(methodName) //
					|| "getClass".contentEquals(methodName) //
					|| "hashCode".contentEquals(methodName) //
					|| "toString".contentEquals(methodName)) {

				continue;
			}

			getters.add(method);
		}

		return getters;
	}

//	private static <T> DataBeanDeserializer<T> addDeserializer(final SimpleModule module, final Class<T> clazz) {
	//
	// final DataBeanDeserializer<T> deserializer =
	// DataBeanDeserializer.createDeserializer(clazz);
	//
	// module.addDeserializer(clazz, deserializer);
	//
	// return deserializer;
//	}
}
