package net.avcompris.commons3.web.it.utils;

import net.avcompris.commons3.api.Entity;

public interface DummyNode extends Entity {

	String getNodeId();

	String getDisplayName();

	String getNodeUrl();

	boolean isEnabled();
}
