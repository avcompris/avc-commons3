package net.avcompris.commons3.web.it.utils;

import net.avcompris.commons3.api.Entities;

public interface DummyUsersInfo extends Entities<DummyUserInfo> {

	@Override
	DummyUserInfo[] getResults();
}
