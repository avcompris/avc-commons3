package net.avcompris.commons3.web.it.utils;

import static net.avcompris.commons3.web.it.utils.ExpressionUtils.process;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ExpressionUtilsTest {

	@Test
	public void testProcess_empty() throws Exception {

		assertEquals("", process(""));
	}

	@Test
	public void testProcess_unchanged() throws Exception {

		assertEquals("xxx", process("xxx"));
	}

	@Test
	public void testProcess_john_Doe() throws Exception {

		assertEquals("Doe", process("${john}"));
	}
}
