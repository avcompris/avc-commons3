package net.avcompris.commons3.web.it.utils;

import net.avcompris.commons3.api.Entities;

public interface DummyCustomers extends Entities<DummyCustomer> {

	@Override
	DummyCustomer[] getResults();
}
