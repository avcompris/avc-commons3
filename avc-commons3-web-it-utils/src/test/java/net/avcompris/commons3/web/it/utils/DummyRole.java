package net.avcompris.commons3.web.it.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.web.it.utils.DummyPermission.ANY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.CREATE_CALORIES_ENTRY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.CREATE_FULL_USER;
import static net.avcompris.commons3.web.it.utils.DummyPermission.CREATE_REGULAR_USER;
import static net.avcompris.commons3.web.it.utils.DummyPermission.DELETE_CALORIES_ENTRY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.DELETE_MY_CALORIES_ENTRY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.DELETE_USER;
import static net.avcompris.commons3.web.it.utils.DummyPermission.GET_APPINFO;
import static net.avcompris.commons3.web.it.utils.DummyPermission.GET_CALORIES_ENTRY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.GET_MY_CALORIES_ENTRY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.GET_ROLES;
import static net.avcompris.commons3.web.it.utils.DummyPermission.GET_USER;
import static net.avcompris.commons3.web.it.utils.DummyPermission.GET_USER_ME;
import static net.avcompris.commons3.web.it.utils.DummyPermission.QUERY_CALORIES_ENTRIES;
import static net.avcompris.commons3.web.it.utils.DummyPermission.QUERY_MY_CALORIES_ENTRIES;
import static net.avcompris.commons3.web.it.utils.DummyPermission.QUERY_USERS;
import static net.avcompris.commons3.web.it.utils.DummyPermission.UPDATE_CALORIES_ENTRY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.UPDATE_MY_CALORIES_ENTRY;
import static net.avcompris.commons3.web.it.utils.DummyPermission.UPDATE_USER;
import static net.avcompris.commons3.web.it.utils.DummyPermission.UPDATE_USER_ME;
import static net.avcompris.commons3.web.it.utils.DummyPermission.UPDATE_USER_ROLE;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;

import net.avcompris.commons3.api.EnumPermission;
import net.avcompris.commons3.api.EnumRole;

public enum DummyRole implements EnumRole {

	ANONYMOUS(null,

			ANY, //
			GET_APPINFO, //
			CREATE_REGULAR_USER //
	),

	REGULAR(new EnumRole[] { ANONYMOUS },

			GET_USER_ME, UPDATE_USER_ME, //
			CREATE_CALORIES_ENTRY, //
			QUERY_MY_CALORIES_ENTRIES, //
			GET_MY_CALORIES_ENTRY, UPDATE_MY_CALORIES_ENTRY, DELETE_MY_CALORIES_ENTRY //
	),

	USERMGR(new EnumRole[] { REGULAR },

			GET_ROLES, //
			QUERY_USERS, //
			CREATE_FULL_USER, GET_USER, UPDATE_USER, DELETE_USER, //
			UPDATE_USER_ROLE //
	),

	SUPERVISOR(new EnumRole[] { REGULAR },

			QUERY_USERS, //
			GET_USER, //
			QUERY_CALORIES_ENTRIES, //
			GET_CALORIES_ENTRY),

	ADMIN(new EnumRole[] { USERMGR, SUPERVISOR },

			UPDATE_CALORIES_ENTRY, DELETE_CALORIES_ENTRY //
	),

	SUPERADMIN(null,

			DummyPermission.SUPERADMIN //
	);

	DummyRole(@Nullable final EnumRole[] superRoles, final EnumPermission... permissions) {

		this.superRoles = (superRoles != null) ? superRoles : new EnumRole[0];
		this.permissions = checkNotNull(permissions, "permissions");
	}

	private final EnumRole[] superRoles;
	private final EnumPermission[] permissions;

	@Override
	public String getRolename() {

		return name();
	}

	@Override
	public EnumRole[] getSuperRoles() {

		return ArrayUtils.clone(superRoles);
	}

	@Override
	public EnumPermission[] getPermissions() {

		return ArrayUtils.clone(permissions);
	}
}