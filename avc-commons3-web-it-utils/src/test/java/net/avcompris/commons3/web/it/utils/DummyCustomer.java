package net.avcompris.commons3.web.it.utils;

import net.avcompris.commons3.api.Entity;

public interface DummyCustomer extends Entity {

	String getCustomerId();

	String getDisplayName();

	boolean isEnabled();

	DummyNode[] getNodes();
}
