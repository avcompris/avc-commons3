package net.avcompris.commons3.web.it.utils;

import javax.annotation.Nullable;

public interface DummyEnclosing {

	int getAge();

	@Nullable
	Embedded getEmbedded();

	interface Embedded {

		String getName();
	}
}
