package net.avcompris.commons3.web.it.utils;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.Entity;
import net.avcompris.commons3.types.DateTimeHolder;

public interface DummyUserInfo extends Entity {

	String getUsername();

	String getCustomerId();

	DummyRole getRole();

	boolean isEnabled();

	@Nullable
	DateTimeHolder getLastActiveAt();
}
