package net.avcompris.commons3.web.it.utils;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

public class JSONUtilsTest {

	@Test
	public void testParse_customers_2019_07_28() throws Exception {

		final DummyCustomers customers = JSONUtils.parseJSON(
				FileUtils.readFileToString(new File("src/test/json", "customers_2019_07_28.json"), UTF_8),

				DummyCustomers.class);

		assertEquals(1, customers.getSize());
		assertEquals(0, customers.getStart());
		assertEquals(10, customers.getLimit());
		assertEquals(1, customers.getTotal());
		assertEquals(null, customers.getSqlWhereClause());
		assertEquals(0, customers.getTookMs());

		assertEquals(1, customers.getResults().length);

		final DummyCustomer customer0 = customers.getResults()[0];

		assertEquals("My customer", customer0.getDisplayName());
		assertEquals("C-1564294632241-4cA5", customer0.getCustomerId());
		assertEquals(1, customer0.getRevision());
		assertEquals(true, customer0.isEnabled());
		assertEquals("2019-07-28T06:17:13.224Z", customer0.getCreatedAt().getDateTime());
		assertEquals(1564294633224L, customer0.getCreatedAt().getTimestamp());
		assertEquals("2019-07-28T06:17:13.224Z", customer0.getUpdatedAt().getDateTime());
		assertEquals(1564294633224L, customer0.getUpdatedAt().getTimestamp());
		assertEquals(0, customer0.getNodes().length);
	}

	@Test
	public void testParse_customers_2019_07_28_toString() throws Exception {

		final DummyCustomers customers = JSONUtils.parseJSON(
				FileUtils.readFileToString(new File("src/test/json", "customers_2019_07_28.json"), UTF_8),

				DummyCustomers.class);

		customers.toString();

		customers.hashCode();

		assertEquals(customers, customers);
	}

	private interface MySimple {

		String getName();

		int getAge();
	}

	@Test
	public void testParseSimple_toString() throws Exception {

		final MySimple dataBean = JSONUtils.parseJSON(

				"{\"name\": \"Doris\", \"age\": 24}",

				MySimple.class);

		assertEquals("Doris", dataBean.getName());
		assertEquals(24, dataBean.getAge());

		assertEquals("{\"name\":\"Doris\",\"age\":24}", dataBean.toString());

		dataBean.hashCode();

		assertEquals(dataBean, dataBean);
	}

	@Test
	public void testParseSimple_equals() throws Exception {

		final MySimple dataBean1 = JSONUtils.parseJSON(

				"{\"name\": \"Doris\", \"age\": 24}",

				MySimple.class);

		final MySimple dataBean2 = JSONUtils.parseJSON(

				"{\"name\": \"Herbert\", \"age\": 20}",

				MySimple.class);

		assertEquals(dataBean1, dataBean1);
		assertNotEquals(dataBean1, dataBean2);
	}

	@Test
	public void testParse_users_2019_07_28() throws Exception {

		final DummyUsersInfo users = JSONUtils.parseJSON(
				FileUtils.readFileToString(new File("src/test/json", "users_2019_07_28.json"), UTF_8),

				DummyUsersInfo.class);

		assertEquals(1, users.getSize());
		assertEquals(0, users.getStart());
		assertEquals(10, users.getLimit());
		assertEquals(1, users.getTotal());
		assertEquals(null, users.getSqlWhereClause());
		assertEquals(0, users.getTookMs());

		assertEquals(1, users.getResults().length);

		final DummyUserInfo user0 = users.getResults()[0];

		assertSame(DummyRole.REGULAR, user0.getRole());
		assertEquals("U-1564305191367-4fLbQHMYrYE0MQPEuMjaHEkZ", user0.getUsername());
	}

	@Test
	public void testParseEnclosing() throws Exception {

		final DummyEnclosing dataBean = JSONUtils.parseJSON(

				"{\"age\": 80, \"embedded\": { \"name\": \"Toto\" } }",

				DummyEnclosing.class);

		assertEquals(80, dataBean.getAge());
		assertEquals("Toto", dataBean.getEmbedded().getName());

		assertEquals(dataBean, dataBean);
	}

	@Test
	public void testParseEnclosing_null() throws Exception {

		final DummyEnclosing dataBean = JSONUtils.parseJSON(

				"{\"age\": 80, \"embedded\": null }",

				DummyEnclosing.class);

		assertEquals(80, dataBean.getAge());
		assertNull(dataBean.getEmbedded());

		assertEquals(dataBean, dataBean);
	}
}
