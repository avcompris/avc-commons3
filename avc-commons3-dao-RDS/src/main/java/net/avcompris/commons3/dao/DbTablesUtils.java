package net.avcompris.commons3.dao;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Locale.ENGLISH;
import static org.apache.commons.lang3.StringUtils.isAllLowerCase;
import static org.apache.commons.lang3.StringUtils.isAsciiPrintable;
import static org.apache.commons.lang3.StringUtils.join;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.tuple.Triple;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import net.avcompris.commons3.dao.DbTable.Type;

public abstract class DbTablesUtils {

	public static ColumnBuilder column(final String columnName) {

		return new ColumnBuilder(columnName);
	}

	public interface Column {

		String getColumnName();

		Type getType();

		@Nullable
		Integer getSize();

		boolean isNotNull();

		boolean isSerial();

		boolean isPrimaryKey();

		boolean isUniqueIndex();

		boolean isIndex();

		@Nullable
		String getRegexp();

		@Nullable
		DbTable getRefTable();

		@Nullable
		String getRefColumnName();

		boolean getOnDeleteCascade();
	}

	private static final class ColumnImpl implements Column {

		private final String columnName;
		private final Type type;
		@Nullable
		private final Integer size;
		private final boolean notNull;
		private final boolean serial;
		private final boolean primaryKey;
		private final boolean unique;
		private final boolean index;
		@Nullable
		private final String regexp;
		@Nullable
		private final DbTable refTable;
		@Nullable
		private final String refColumnName;
		private final boolean onDeleteCascade;

		public ColumnImpl(final ColumnBuilder builder) {

			checkNotNull(builder, "builder");

			this.columnName = builder.columnName;
			this.type = checkNotNull(builder.type, "type");
			this.size = builder.size;
			this.notNull = builder.notNull;
			this.serial = builder.serial;
			this.primaryKey = builder.primaryKey;
			this.index = builder.index;
			this.unique = builder.unique;
			this.regexp = builder.regexp;
			this.refTable = builder.refTable;
			this.refColumnName = builder.refColumnName;
			this.onDeleteCascade = builder.onDeleteCascade;
		}

		@Override
		public String getColumnName() {

			return columnName;
		}

		@Override
		public Type getType() {

			return type;
		}

		@Override
		@Nullable
		public Integer getSize() {

			return size;
		}

		@Override
		public boolean isNotNull() {

			return notNull;
		}

		@Override
		public boolean isSerial() {

			return serial;
		}

		@Override
		public boolean isPrimaryKey() {

			return primaryKey;
		}

		@Override
		public boolean isUniqueIndex() {

			return index && unique;
		}

		@Override
		public boolean isIndex() {

			return index;
		}

		@Override
		@Nullable
		public String getRegexp() {

			return regexp;
		}

		@Override
		@Nullable
		public DbTable getRefTable() {

			return refTable;
		}

		@Override
		@Nullable
		public String getRefColumnName() {

			return refColumnName;
		}

		@Override
		public boolean getOnDeleteCascade() {

			return onDeleteCascade;
		}
	}

	public static class ColumnBuilder {

		private final String columnName;
		private Type type;
		private Integer size;
		private String regexp;
		private boolean serial;
		private boolean primaryKey;
		private boolean index;
		private boolean unique;
		private DbTable refTable;
		private String refColumnName;
		private boolean onDeleteCascade;
		private boolean notNull;

		private ColumnBuilder(final String columnName) {

			this.columnName = checkNotNull(columnName, "columnName");

			checkArgument(isAsciiPrintable(columnName), "columnName should be US-ASCII, but was: %s", columnName);

			checkArgument(isAllLowerCase(columnName.replace("_", "")), "columnName should be lowercase, but was: %s",
					columnName);
		}

		public Column build() {

			return new ColumnImpl(this);
		}

		public ColumnBuilder type(final Type type) {

			this.type = checkNotNull(type, "type");

			return this;
		}

		public ColumnBuilder size(final int size) {

			this.size = size;

			return this;
		}

		public ColumnBuilder notNull() {

			this.notNull = true;

			return this;
		}

		public ColumnBuilder nullable() {

			this.notNull = false;

			return this;
		}

		public ColumnBuilder regexp(final String regexp) {

			this.regexp = checkNotNull(regexp, "regexp");

			return this;
		}

		public ColumnBuilder serial() {

			this.serial = true;

			return this;
		}

		public ColumnBuilder primaryKey() {

			this.primaryKey = true;

			return this;
		}

		public ColumnBuilder index() {

			this.index = true;

			return this;
		}

		public ColumnBuilder unique() {

			this.index = true;
			this.unique = true;

			return this;
		}

		public ColumnBuilder refKey(final DbTable refTable, final String refColumnName) {

			return refKey(refTable, refColumnName, false);
		}

		public ColumnBuilder refKey(final DbTable refTable, final String refColumnName, final boolean onDeleteCascade) {

			this.refTable = checkNotNull(refTable, "refTable");
			this.refColumnName = checkNotNull(refColumnName, "refColumnName");
			this.onDeleteCascade = onDeleteCascade;

			return this;
		}
	}

	private static List<DbTable> getSubDbTables(final DbTable dbTable) {

		checkNotNull(dbTable, "dbTable");

		final List<DbTable> alikeDbTables = new ArrayList<>();

		final DbTable[] enumConstants = dbTable.getClass().getEnumConstants();

		if (enumConstants == null) {

			alikeDbTables.add(dbTable);

		} else {

			for (final DbTable enumConstant : enumConstants) {

				if (enumConstant == dbTable || enumConstant.name().startsWith(dbTable.name() + "_")) {

					alikeDbTables.add(enumConstant);
				}
			}
		}

		return alikeDbTables;
	}

	public static String[] composeSQLDropCommands(final String dbTableNamePrefix, final DbTable dbTable)
			throws SQLException {

		final List<String> sqls = new ArrayList<>();

		for (final DbTable dt : Lists.reverse(getSubDbTables(dbTable))) {

			final String actualDbTableName = dt.getRuntimeDbTableNameWithPrefix(dbTableNamePrefix);

			parseIndexes(dt, columns -> {

				// if (columns.length == 1) {

				// final Column column = columns[0];

				for (final Column column : columns) {

					final String columnName = column.getColumnName();

					final String indexName = actualDbTableName + "_" + columnName + "_idx";

					sqls.add("DROP INDEX " + indexName);

//				} else {

//					throw new NotImplementedException("columns.length: " + columns.length);
				}
			});

			sqls.add(composeDropTableSQL(dbTableNamePrefix, dt));
		}

		return Iterables.toArray(sqls, String.class);
	}

	private static String composeDropTableSQL(final String dbTableNamePrefix, final DbTable dbTable) {

		final String runtimeDbTableName = dbTable.getRuntimeDbTableNameWithPrefix(dbTableNamePrefix);

		return "DROP TABLE " + runtimeDbTableName + " CASCADE";
	}

	public static String[] composeSQLCreateCommands(final String dbTableNamePrefix, final DbTable dbTable)
			throws SQLException {

		final List<String> sqls = new ArrayList<>();

		for (final DbTable dt : getSubDbTables(dbTable)) {

			final String actualDbTableName = dt.getRuntimeDbTableNameWithPrefix(dbTableNamePrefix);

			sqls.add(composeCreateTableSQL(dbTableNamePrefix, dt));

			parseIndexes(dt, columns -> {

//				if (columns.length == 1) {

//					final Column column = columns[0];

				for (final Column column : columns) {

					final String columnName = column.getColumnName();

					final String indexName = actualDbTableName + "_" + columnName + "_idx";

					sqls.add((column.isUniqueIndex() ? "CREATE UNIQUE INDEX " : "CREATE INDEX ") //

							+ indexName + " ON " + actualDbTableName + " (" + columnName + ")");

//				} else {

//					throw new NotImplementedException("columns.length: " + columns.length);
				}
			});
		}

		return Iterables.toArray(sqls, String.class);
	}

	private static void parseIndexes(final DbTable table, final Consumer<Column[]> consumer) {

		final List<Column> uniqueIndexes = new ArrayList<Column>();

		for (final Column column : table.columns()) {

			final String columnName = column.getColumnName();
			final boolean primaryKey = column.isPrimaryKey();
			final boolean uniqueIndex = column.isUniqueIndex();
			final boolean index = column.isIndex();

			if (columnName != null && index && !primaryKey) {

				if (uniqueIndex) {

					uniqueIndexes.add(column);

				} else {

					consumer.accept(new Column[] { column });
				}
			}
		}

		if (!uniqueIndexes.isEmpty()) {
			consumer.accept(Iterables.toArray(uniqueIndexes, Column.class));
		}
	}

	private static String composeCreateTableSQL(final String dbTableNamePrefix, final DbTable dbTable) {

		final String runtimeDbTableName = dbTable.getRuntimeDbTableNameWithPrefix(dbTableNamePrefix);

		final StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE " + runtimeDbTableName + " (");

		final List<String> primaryKeys = new ArrayList<>();

		boolean start = true;

		for (final Column column : dbTable.columns()) {

			if (start) {
				start = false;
			} else {
				sb.append(", ");
			}

			final String columnName = column.getColumnName();

			sb.append(columnName + " ");

			switch (column.getType()) {

			case VARCHAR:
				sb.append("VARCHAR(" + column.getSize() + ")");
				break;

			case INTEGER:

				if (column.isSerial()) {
					sb.append("SERIAL");
				} else {
					sb.append("INTEGER");
				}

				break;

			case LONG:
				sb.append("BIGINT");
				break;

			case BOOLEAN:
				sb.append("BOOLEAN");
				break;

			case TIMESTAMP_WITH_TIMEZONE:
				sb.append("TIMESTAMPTZ");
				break;

			case BYTE_ARRAY:
				sb.append("BYTEA");
				break;

			case TEXT:
				sb.append("TEXT");
				break;

			default:
				throw new NotImplementedException("Unknown type: " + column.getType());
			}

			if (column.isNotNull()) {

				sb.append(" NOT NULL");

			} else {

				sb.append(" NULL");
			}

			if (column.isPrimaryKey()) {

				primaryKeys.add(columnName);
			}
		}

		if (!primaryKeys.isEmpty()) {

			sb.append(", PRIMARY KEY (" + join(primaryKeys, ", ") + ")");
		}

		final Map<DbTable, Triple<List<String>, List<String>, MutableBoolean>> foreignColumnNames = newHashMap();

		final List<DbTable> refTables = new ArrayList<>();

		for (final Column column : dbTable.columns()) {

			final DbTable refTable = column.getRefTable();

			if (refTable != null) {

				final Triple<List<String>, List<String>, MutableBoolean> columnNames;

				if (foreignColumnNames.containsKey(refTable)) {

					columnNames = foreignColumnNames.get(refTable);

				} else {

					columnNames = Triple.of(new ArrayList<>(), new ArrayList<>(), new MutableBoolean(false));

					foreignColumnNames.put(refTable, columnNames);

					refTables.add(refTable);
				}

				columnNames.getLeft().add(column.getColumnName());
				columnNames.getMiddle().add(column.getRefColumnName());

				if (column.getOnDeleteCascade()) {
					columnNames.getRight().setTrue();
				}
			}
		}

		for (final DbTable refTable : refTables) {

			final Triple<List<String>, List<String>, MutableBoolean> columnNames = foreignColumnNames.get(refTable);

			final boolean onDeleteCascade = columnNames.getRight().booleanValue();

			sb.append(", FOREIGN KEY (" + join(columnNames.getLeft(), ", ") + ") REFERENCES "
					+ refTable.getRuntimeDbTableNameWithPrefix(dbTableNamePrefix) //
					+ " (" + join(columnNames.getMiddle(), ", ") + ")" //
					+ (onDeleteCascade ? " ON DELETE CASCADE" : ""));

		}

		sb.append(")");

		return sb.toString();
	}

	public static String composeActualDbTableName(final String tableNamePrefix, final DbTable dbTable) {

		checkNotNull(tableNamePrefix, "tableNamePrefix");
		checkNotNull(dbTable, "dbTable");

		return (tableNamePrefix + dbTable.name()).toLowerCase(ENGLISH);
	}
}
