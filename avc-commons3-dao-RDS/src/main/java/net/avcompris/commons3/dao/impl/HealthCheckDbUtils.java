package net.avcompris.commons3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons3.dao.DbTable;
import net.avcompris.commons3.dao.DbTablesUtils.Column;

public abstract class HealthCheckDbUtils {

	public static void populateRuntimeDbStatus( //
			final MutableHealthCheckDb healthCheck, //
			final DataSource dataSource, //
			final String dbTableNamePrefix, //
			final DbTable... dbTables) {

		checkNotNull(healthCheck, "healthCheck");
		checkNotNull(dbTables, "dbTables");

		final Context context = new Context(dataSource, dbTableNamePrefix);

		final MutableHealthCheckDb.RuntimeDbStatus runtimeDbStatus = instantiate(
				MutableHealthCheckDb.RuntimeDbStatus.class) //
						.setOk(true);

		healthCheck.setRuntimeDbStatus(runtimeDbStatus);

		for (final DbTable dbTable : dbTables) {

			populateRuntimeDbStatus(healthCheck, runtimeDbStatus, context, dbTable);

			for (final DbTable subTable : dbTable.getClass().getEnumConstants()) {

				if (dbTable.equals(subTable) || !subTable.name().startsWith(dbTable.name() + "_")) {
					continue;
				}

				populateRuntimeDbStatus(healthCheck, runtimeDbStatus, context, subTable);
			}
		}
	}

	private static void populateRuntimeDbStatus( //
			final MutableHealthCheckDb healthCheck, //
			final MutableHealthCheckDb.RuntimeDbStatus runtimeDbStatus, //
			final Context context, //
			final DbTable dbTable) {

		final String runtimeDbTableName = dbTable.getRuntimeDbTableNameWithPrefix(context.dbTableNamePrefix);

		final MutableHealthCheckDb.RuntimeDbTable runtimeDbTable = instantiate(
				MutableHealthCheckDb.RuntimeDbTable.class) //
						.setOk(true) //
						.setRuntimeName(runtimeDbTableName) //
						.setCompileName(dbTable.name()); //

		runtimeDbStatus.addToTables(runtimeDbTable);

		final boolean existsInRuntimeDb;

		try {

			existsInRuntimeDb = context.existsInRuntimeDb(runtimeDbTableName);

		} catch (final SQLException e) {

			runtimeDbTable //
					.setExistsInRuntimeDb(false) //
					.setOk(false);

			runtimeDbStatus //
					.setOk(false);

			healthCheck //
					.addToErrors(e.toString()) //
					.setOk(false);

			return;
		}

		runtimeDbTable.setExistsInRuntimeDb(existsInRuntimeDb);

		if (!existsInRuntimeDb) {

			runtimeDbTable //
					.setOk(false);

			runtimeDbStatus //
					.setOk(false);

			healthCheck //
					.addToErrors("Could not find table in runtime DB: " + runtimeDbTableName) //
					.setOk(false);
		}

		for (final Column column : dbTable.columns()) {

			final String columnName = column.getColumnName();

			final String compileLiteral = toCompileLiteral(column);

			final MutableHealthCheckDb.RuntimeDbColumn runtimeDbColumn = instantiate(
					MutableHealthCheckDb.RuntimeDbColumn.class) //
							.setOk(true) //
							.setName(columnName) //
							.setCompileLiteral(compileLiteral);

			runtimeDbTable.addToColumns(runtimeDbColumn);

			final String runtimeLiteral;

			try {

				runtimeLiteral = context.getRuntimeLiteral(runtimeDbTableName, columnName);

			} catch (final SQLException e) {

				runtimeDbColumn //
						.setOk(false);

				runtimeDbTable //
						.setOk(false);

				runtimeDbStatus //
						.setOk(false);

				healthCheck //
						.addToErrors(e.toString() + ", for: " + runtimeDbTableName + "." + columnName) //
						.setOk(false);

				continue;
			}

			runtimeDbColumn.setRuntimeLiteral(runtimeLiteral);

			if (!compileLiteral.equals(runtimeLiteral)) {

				runtimeDbColumn //
						.setOk(false);

				if (existsInRuntimeDb) {

					runtimeDbTable //
							.setOk(false);

					runtimeDbStatus //
							.setOk(false);

					healthCheck //
							.addToErrors("Column runtime differs from spec: " //
									+ runtimeDbTableName + "." + columnName + ": " //
									+ runtimeLiteral + " ≠ " + compileLiteral) //
							.setOk(false);
				}
			}
		}
	}

	private static String toCompileLiteral(final Column column) {

		checkNotNull(column, "column");

		final StringBuilder sb = new StringBuilder();

		switch (column.getType()) {

		case VARCHAR:
			sb.append("VARCHAR(").append(column.getSize()).append(")");
			break;

		case BOOLEAN:
			sb.append("BOOLEAN");
			break;

		case INTEGER:
			sb.append("INTEGER");
			break;

		case TIMESTAMP_WITH_TIMEZONE:
			sb.append("TIMESTAMPTZ");
			break;

		case TEXT:
			sb.append("TEXT");
			break;

		case BYTE_ARRAY:
			sb.append("BYTE_ARRAY");
			break;

		default:
			throw new NotImplementedException("colum.type: " + column.getType() //
					+ ", for: " + column.getColumnName());
		}

		if (column.isNotNull()) {
			sb.append(" NOT NULL");
		}

		if (column.isPrimaryKey()) {
			sb.append(" PRIMARY KEY");
		}

		return sb.toString();
	}

	private static final class Context {

		private final DataSource dataSource;
		public final String dbTableNamePrefix;

		public Context( //
				final DataSource dataSource, //
				final String dbTableNamePrefix) {

			this.dataSource = checkNotNull(dataSource, "dataSource");
			this.dbTableNamePrefix = checkNotNull(dbTableNamePrefix, "dbTableNamePrefix");
		}

		public boolean existsInRuntimeDb(final String runtimeDbTableName) throws SQLException {

			checkNotNull(runtimeDbTableName, "runtimeDbTableName");

			try (Connection cxn = dataSource.getConnection()) {

				try (ResultSet rs = cxn.getMetaData().getTables(null, null, runtimeDbTableName, null)) {

					return rs.next();
				}
			}
		}

		public String getRuntimeLiteral(final String runtimeDbTableName, final String columnName) throws SQLException {

			checkNotNull(runtimeDbTableName, "runtimeDbTableName");
			checkNotNull(columnName, "columnName");

			final StringBuilder sb = new StringBuilder();

			try (Connection cxn = dataSource.getConnection()) {

				try (ResultSet rs = cxn.getMetaData().getColumns(null, null, runtimeDbTableName, columnName)) {

					if (!rs.next()) {
						return null;
					}

					final int dataType = rs.getInt("DATA_TYPE");
					final String typeName = rs.getString("TYPE_NAME");
					final int size = rs.getInt("COLUMN_SIZE");
					final boolean nullable = rs.getInt("NULLABLE") == DatabaseMetaData.columnNullable;

					switch (dataType) {

					case Types.VARCHAR:
						if ("text".equals(typeName)) {
							sb.append("TEXT");
						} else {
							sb.append("VARCHAR(").append(size).append(")");
						}
						break;

					case Types.BOOLEAN:
					case Types.BIT:
						sb.append("BOOLEAN");
						break;

					case Types.INTEGER:
						sb.append("INTEGER");
						break;

					case Types.TIMESTAMP_WITH_TIMEZONE:
					case Types.TIMESTAMP:
						sb.append("TIMESTAMPTZ");
						break;

					case Types.BINARY:
						sb.append("BYTE_ARRAY");
						break;

					default:
						throw new NotImplementedException("dataType: " + dataType //
								+ " (typeName: \"" + typeName + "\")" //
								+ ", for: " + runtimeDbTableName + "." + columnName);
					}

					if (!nullable) {
						sb.append(" NOT NULL");
					}
				}

				boolean isPrimaryKey = false;

				try (ResultSet rs = cxn.getMetaData().getPrimaryKeys(null, null, runtimeDbTableName)) {

					while (rs.next()) {

						final String runtimeDbColumnName = rs.getString("COLUMN_NAME");

						if (columnName.contentEquals(runtimeDbColumnName)) {

							isPrimaryKey = true;

							break;
						}
					}
				}

				if (isPrimaryKey) {

					sb.append(" PRIMARY KEY");
				}
			}

			return sb.toString();
		}
	}
}
