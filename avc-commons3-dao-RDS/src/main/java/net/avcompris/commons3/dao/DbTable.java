package net.avcompris.commons3.dao;

import static java.util.Locale.ENGLISH;

import net.avcompris.commons3.dao.DbTablesUtils.Column;

public interface DbTable {

	String name();

	Column[] columns();

	enum Type {

		VARCHAR, //
		INTEGER, //
		LONG, //
		BOOLEAN, //
		TIMESTAMP_WITH_TIMEZONE, //
		BYTE_ARRAY, //
		TEXT, //
		JSONB, //
	}

	default String getRuntimeDbTableNameWithPrefix(final String dbTableNamePrefix) {

		return dbTableNamePrefix + name().toLowerCase(ENGLISH);
	}
}
