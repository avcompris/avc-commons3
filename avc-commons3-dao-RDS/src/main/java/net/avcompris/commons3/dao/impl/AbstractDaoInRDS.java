package net.avcompris.commons3.dao.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.joda.time.DateTimeZone.UTC;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.Nullable;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import net.avcompris.commons3.dao.EntitiesDtoQuery.SortBy;
import net.avcompris.commons3.utils.Clock;

public abstract class AbstractDaoInRDS extends AbstractDao {

	protected static final String SCHEMA_VERSION_COLUMN_NAME = "SCHEMA_VERSION";

	protected AbstractDaoInRDS(final DataSource dataSource, final String tableName, final Clock clock) {

		super(clock);

		this.dataSource = checkNotNull(dataSource, "dataSource");
		this.tableName = checkNotNull(tableName, "tableName");
	}

	private final DataSource dataSource;
	protected final String tableName;

	protected final Connection getConnection() throws SQLException {

		return dataSource.getConnection();
	}

	protected static final String toSQLOrderByDirective(final SortBy... sortBys) {

		checkNotNull(sortBys, "sortBys");

		if (sortBys.length == 0) {
			return ""; // Note: This should not happen!
		}

		final StringBuilder sb = new StringBuilder(" ORDER BY ");

		boolean start = true;

		for (final SortBy sortBy : sortBys) {

			if (start) {
				start = false;
			} else {
				sb.append(", ");
			}

			sb.append(sortBy.toSqlField());

			if (sortBy.isDesc()) {
				sb.append(" DESC");
			}
		}

		return sb.toString();
	}

	protected static String toSQLLimitClause(final int start, final int limit) {

		return " LIMIT " + limit + " OFFSET " + start;
	}

	@Nullable
	protected static Integer getInteger(final ResultSet rs, final String columnName) throws SQLException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		final int value = rs.getInt(columnName);

		return rs.wasNull() ? null : value;
	}

	@Nullable
	protected static Integer getInteger(final ResultSet rs, final int columnIndex) throws SQLException {

		checkNotNull(rs, "rs");

		final int value = rs.getInt(columnIndex);

		return rs.wasNull() ? null : value;
	}

	@Nullable
	protected static Integer getInt(final ResultSet rs, final String columnName) throws SQLException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		return rs.getInt(columnName);
	}

	@Nullable
	protected static Integer getInt(final ResultSet rs, final int columnIndex) throws SQLException {

		checkNotNull(rs, "rs");

		return rs.getInt(columnIndex);
	}

	@Nullable
	protected static Long getLong(final ResultSet rs, final String columnName) throws SQLException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		final long value = rs.getLong(columnName);

		return rs.wasNull() ? null : value;
	}

	@Nullable
	protected static Long getLong(final ResultSet rs, final int columnIndex) throws SQLException {

		checkNotNull(rs, "rs");

		final long value = rs.getLong(columnIndex);

		return rs.wasNull() ? null : value;
	}

	@Nullable
	protected static DateTime getDateTime(final ResultSet rs, final String columnName) throws SQLException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		return getDateTime(rs.getTimestamp(columnName));
	}

	@Nullable
	private static DateTime getDateTime(@Nullable final Timestamp timestamp) {

		return timestamp == null ? null : new DateTime(timestamp.getTime()).withZone(UTC);
	}

	@Nullable
	protected static DateTime getDateTime(final ResultSet rs, final int columnIndex) throws SQLException {

		checkNotNull(rs, "rs");

		return getDateTime(rs.getTimestamp(columnIndex));
	}

	@Nullable
	protected static byte[] getBytes(final ResultSet rs, final String columnName) throws SQLException, IOException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		return getBytes(rs.getBinaryStream(columnName));
	}

	@Nullable
	private static byte[] getBytes(@Nullable final InputStream is) throws IOException {

		if (is == null) {

			return null;
		}

		try {

			return IOUtils.toByteArray(is);

		} finally {

			is.close();
		}
	}

	@Nullable
	protected static byte[] getBytes(final ResultSet rs, final int columnIndex) throws SQLException, IOException {

		checkNotNull(rs, "rs");

		return getBytes(rs.getBinaryStream(columnIndex));
	}

	@Nullable
	protected static String getString(final ResultSet rs, final String columnName) throws SQLException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		return rs.getString(columnName);
	}

	@Nullable
	protected static <T extends Enum<T>> T getEnum(final ResultSet rs, final int columnIndex, final Class<T> enumClass)
			throws SQLException {

		checkNotNull(rs, "rs");

		return toEnum(rs.getString(columnIndex), enumClass);
	}

	@Nullable
	protected static <T extends Enum<T>> T getEnum(final ResultSet rs, final String columnName,
			final Class<T> enumClass) throws SQLException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		return toEnum(rs.getString(columnName), enumClass);
	}

	@Nullable
	private static <T extends Enum<T>> T toEnum(@Nullable final String value, final Class<T> enumClass)
			throws SQLException {

		if (value == null) {

			return null;
		}

		for (final T enumConstant : enumClass.getEnumConstants()) {

			if (value.equals(enumConstant.name())) {

				return enumConstant;
			}
		}

		throw new IllegalArgumentException("Unknown enumConstant: " + value + ", for: " + enumClass.getName());
	}

	@Nullable
	protected static String getString(final ResultSet rs, final int columnIndex) throws SQLException {

		checkNotNull(rs, "rs");

		return rs.getString(columnIndex);
	}

	@Nullable
	protected static boolean getBoolean(final ResultSet rs, final String columnName) throws SQLException {

		checkNotNull(rs, "rs");
		checkNotNull(columnName, "columnName");

		return rs.getBoolean(columnName);
	}

	@Nullable
	protected static boolean getBoolean(final ResultSet rs, final int columnIndex) throws SQLException {

		checkNotNull(rs, "rs");

		return rs.getBoolean(columnIndex);
	}

	protected final void setString(final PreparedStatement pstmt, final int paramIndex, @Nullable final String s)
			throws SQLException {

		pstmt.setString(paramIndex, s);
	}

	protected final void setObject(final PreparedStatement pstmt, final int paramIndex, @Nullable final Object o)
			throws SQLException {

		pstmt.setObject(paramIndex, o);
	}

	protected final void setBinaryStream(final PreparedStatement pstmt, final int paramIndex,
			@Nullable final InputStream is) throws SQLException {

		pstmt.setBinaryStream(paramIndex, is);
	}

	protected final void setInteger(final PreparedStatement pstmt, final int paramIndex, @Nullable final Integer value)
			throws SQLException {

		if (value == null) {

			pstmt.setNull(paramIndex, Types.INTEGER);

		} else {

			pstmt.setInt(paramIndex, value);
		}
	}

	protected final void setInt(final PreparedStatement pstmt, final int paramIndex, final int value)
			throws SQLException {

		pstmt.setInt(paramIndex, value);
	}

	protected final void setDouble(final PreparedStatement pstmt, final int paramIndex, final double value)
			throws SQLException {

		pstmt.setDouble(paramIndex, value);
	}

	protected final void setBoolean(final PreparedStatement pstmt, final int paramIndex, final boolean value)
			throws SQLException {

		pstmt.setBoolean(paramIndex, value);
	}

	protected final void setBytes(final PreparedStatement pstmt, final int paramIndex, final byte[] bytes)
			throws SQLException {

		pstmt.setBytes(paramIndex, bytes);
	}

	public static final String PSQL_UNIQUE_VIOLATION = "23505";
	public static final String PSQL_FOREIGN_ENTITY_VIOLATION = "23503";

	public static boolean isPSQLUniqueViolation(@Nullable final SQLException e) {

		return e != null && PSQL_UNIQUE_VIOLATION.contentEquals(e.getSQLState());
	}

	public static boolean isPSQLForeignEntityViolation(@Nullable final SQLException e) {

		return e != null && PSQL_FOREIGN_ENTITY_VIOLATION.contentEquals(e.getSQLState());
	}

	protected final void setLong(final PreparedStatement pstmt, final int paramIndex, @Nullable final Long value)
			throws SQLException {

		if (value == null) {

			pstmt.setNull(paramIndex, Types.INTEGER);

		} else {

			pstmt.setInt(paramIndex, value.intValue());
		}
	}

	private static final Calendar UTC_CALENDAR = Calendar.getInstance(TimeZone.getTimeZone(DateTimeZone.UTC.getID()));

	protected final void setDateTime(final PreparedStatement pstmt, final int paramIndex,
			@Nullable final DateTime dateTime) throws SQLException {

		if (dateTime == null) {

			pstmt.setNull(paramIndex, Types.TIMESTAMP);

		} else {

			final DateTime withUTC = dateTime.withZone(UTC);

			pstmt.setTimestamp(paramIndex, new Timestamp(withUTC.getMillis()), UTC_CALENDAR);
		}
	}

	@Nullable
	protected static String joinRefIds(@Nullable final String... refIds) {

		if (refIds == null) {

			return null;
		}

		final StringBuilder sb = new StringBuilder("|");

		for (final String refId : refIds) {

			if (refId == null) {
				continue;
			}

			checkArgument(!refId.contains("|"), //
					"refId should not contain \"|\", but was: %s", refId);

			sb.append(refId).append("|");
		}

		return sb.toString();
	}
}
