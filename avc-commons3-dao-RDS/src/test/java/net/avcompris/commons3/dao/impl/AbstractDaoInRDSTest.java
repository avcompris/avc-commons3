package net.avcompris.commons3.dao.impl;

import static net.avcompris.commons3.dao.impl.AbstractDaoInRDS.toSQLOrderByDirective;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.dao.EntitiesDtoQuery;

public class AbstractDaoInRDSTest {

	@Test
	public void testToSQLOrderByDirective_empty() throws Exception {

		assertEquals("", toSQLOrderByDirective());
	}

	private enum MySortBy implements EntitiesDtoQuery.SortBy {

		SORT_BY_X, SORT_BY_X_DESC, SORT_BY_Y, SORT_BY_Y_DESC;

		@Override
		public String toSqlField() {

			return EntitiesDtoQuery.SortBy.toSqlField(name());
		}

		@Override
		public boolean isDesc() {

			return EntitiesDtoQuery.SortBy.isDesc(name());
		}
	}

	@Test
	public void testToSQLOrderByDirective_1() throws Exception {

		assertEquals(" ORDER BY x",

				toSQLOrderByDirective(MySortBy.SORT_BY_X));
	}

	@Test
	public void testToSQLOrderByDirective_2() throws Exception {

		assertEquals(" ORDER BY x, y",

				toSQLOrderByDirective(MySortBy.SORT_BY_X, MySortBy.SORT_BY_Y));
	}

	@Test
	public void testToSQLOrderByDirective_1_desc() throws Exception {

		assertEquals(" ORDER BY x DESC",

				toSQLOrderByDirective(MySortBy.SORT_BY_X_DESC));
	}

	@Test
	public void testToSQLOrderByDirective_2_asc_desc() throws Exception {

		assertEquals(" ORDER BY x, y DESC",

				toSQLOrderByDirective(MySortBy.SORT_BY_X, MySortBy.SORT_BY_Y_DESC));
	}

	@Test
	public void testToSQLOrderByDirective_2_desc_desc() throws Exception {

		assertEquals(" ORDER BY x DESC, y DESC",

				toSQLOrderByDirective(MySortBy.SORT_BY_X_DESC, MySortBy.SORT_BY_Y_DESC));
	}

	@Test
	public void testToSQLOrderByDirective_2_desc_asc() throws Exception {

		assertEquals(" ORDER BY x DESC, y",

				toSQLOrderByDirective(MySortBy.SORT_BY_X_DESC, MySortBy.SORT_BY_Y));
	}
}
