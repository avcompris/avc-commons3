package net.avcompris.commons3.dao;

import static net.avcompris.commons3.dao.DbTable.Type.BOOLEAN;
import static net.avcompris.commons3.dao.DbTable.Type.INTEGER;
import static net.avcompris.commons3.dao.DbTable.Type.TIMESTAMP_WITH_TIMEZONE;
import static net.avcompris.commons3.dao.DbTable.Type.VARCHAR;
import static net.avcompris.commons3.dao.DbTablesUtils.column;
import static net.avcompris.commons3.dao.DbTablesUtils.composeSQLCreateCommands;
import static net.avcompris.commons3.dao.DbTablesUtils.composeSQLDropCommands;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.dao.DbTablesUtils.Column;

public class DbTablesUtilsTest {

	@Test
	public void testGetSQLDropCommands_emptyTable() throws Exception {

		final String[] sqls = composeSQLDropCommands("xxx", forgeDbTable("toto"));

		assertEquals(1, sqls.length);

		assertEquals("DROP TABLE xxxtoto CASCADE", sqls[0]);
	}

	@Test
	public void testGetSQLDropCommands_emptyTable_uppercase() throws Exception {

		final String[] sqls = composeSQLDropCommands("xxx", forgeDbTable("TOTO"));

		assertEquals(1, sqls.length);

		assertEquals("DROP TABLE xxxtoto CASCADE", sqls[0]);
	}

	@Test
	public void testGetSQLCreateCommands_emptyTable() throws Exception {

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("toto"));

		assertEquals(1, sqls.length);

		assertEquals("CREATE TABLE xxxtoto ()", sqls[0]);
	}

	@Test
	public void testGetSQLCreateCommands_emptyTable_uppercase() throws Exception {

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TOTO"));

		assertEquals(1, sqls.length);

		assertEquals("CREATE TABLE xxxtoto ()", sqls[0]);
	}

	@Test
	public void testGetSQLCreateCommands_oneColumn_VARCHAR_255() throws Exception {

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TOTO", //
				column("name") //
						.type(VARCHAR) //
						.size(255) //
						.build()));

		assertEquals(1, sqls.length);

		assertEquals("CREATE TABLE xxxtoto" //
				+ " (name VARCHAR(255) NULL)", sqls[0]);
	}

	@Test
	public void testGetSQLCreateCommands_index() throws Exception {

		final DbTable dbTable = forgeDbTable("TOTO", //
				column("name") //
						.type(VARCHAR) //
						.size(255) //
						.index() //
						.build());

		final String[] sqlsCreate = composeSQLCreateCommands("xxx", dbTable);

		assertEquals(2, sqlsCreate.length);

		assertEquals("CREATE TABLE xxxtoto" //
				+ " (name VARCHAR(255) NULL)", sqlsCreate[0]);

		assertEquals("CREATE INDEX xxxtoto_name_idx" //
				+ " ON xxxtoto (name)", sqlsCreate[1]);

		final String[] sqlsDrop = composeSQLDropCommands("xxx", dbTable);

		assertEquals(2, sqlsDrop.length);

		assertEquals("DROP INDEX xxxtoto_name_idx", sqlsDrop[0]);

		assertEquals("DROP TABLE xxxtoto CASCADE", sqlsDrop[1]);
	}

	@Test
	public void testGetSQLCreateCommands_unique() throws Exception {

		final DbTable dbTable = forgeDbTable("TOTO", //
				column("name") //
						.type(VARCHAR) //
						.size(255) //
						.index() //
						.unique() //
						.build());

		final String[] sqlsCreate = composeSQLCreateCommands("xxx", dbTable);

		assertEquals(2, sqlsCreate.length);

		assertEquals("CREATE TABLE xxxtoto" //
				+ " (name VARCHAR(255) NULL)", sqlsCreate[0]);

		assertEquals("CREATE UNIQUE INDEX xxxtoto_name_idx" //
				+ " ON xxxtoto (name)", sqlsCreate[1]);

		final String[] sqlsDrop = composeSQLDropCommands("xxx", dbTable);

		assertEquals(2, sqlsDrop.length);

		assertEquals("DROP INDEX xxxtoto_name_idx", sqlsDrop[0]);

		assertEquals("DROP TABLE xxxtoto CASCADE", sqlsDrop[1]);
	}

	@Test
	public void testGetSQLCreateCommands_twoColumns_INTEGER_BOOLEAN() throws Exception {

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TOTO", //
				column("age") //
						.type(INTEGER) //
						.build(), //
				column("old") //
						.type(BOOLEAN) //
						.notNull() //
						.build()));

		assertEquals(1, sqls.length);

		assertEquals("CREATE TABLE xxxtoto" //
				+ " (age INTEGER NULL," //
				+ " old BOOLEAN NOT NULL)", sqls[0]);
	}

	@Test
	public void testGetSQLCreateCommands_twoColumns_PK_TZ() throws Exception {

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TOTO", //
				column("id") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.build(), //
				column("started_at") //
						.type(TIMESTAMP_WITH_TIMEZONE) //
						.build()));

		assertEquals(1, sqls.length);

		assertEquals("CREATE TABLE xxxtoto" //
				+ " (id VARCHAR(255) NOT NULL," //
				+ " started_at TIMESTAMPTZ NULL," //
				+ " PRIMARY KEY (id))", sqls[0]);
	}

	@Test
	public void testGetSQLCreateCommands_serial() throws Exception {

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TOTO", //
				column("id") //
						.type(INTEGER) //
						.serial() //
						.primaryKey() //
						.notNull() //
						.build()));

		assertEquals(1, sqls.length);

		assertEquals("CREATE TABLE xxxtoto" //
				+ " (id SERIAL NOT NULL," //
				+ " PRIMARY KEY (id))", sqls[0]);
	}

	@Test
	public void testGetSQLCreateCommands_twoColumns_FK() throws Exception {

		final DbTable totoTable = forgeDbTable("TOTO", //
				column("id") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.build());

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TATA", //
				column("toto_id") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.refKey(totoTable, "id") //
						.build(), //
				column("age") //
						.type(INTEGER) //
						.build()));

		assertEquals("CREATE TABLE xxxtata" //
				+ " (toto_id VARCHAR(255) NOT NULL," //
				+ " age INTEGER NULL," //
				+ " PRIMARY KEY (toto_id)," //
				+ " FOREIGN KEY (toto_id) REFERENCES xxxtoto (id))", sqls[0]);

		assertEquals(1, sqls.length);
	}

	@Test
	public void testGetSQLCreateCommands_2FKs() throws Exception {

		final DbTable totoTable = forgeDbTable("TOTO", //
				column("id") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.build());

		final DbTable tutuTable = forgeDbTable("TUTU", //
				column("id") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.build());

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TATA", //
				column("toto_id") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.refKey(totoTable, "id") //
						.build(), //
				column("tutu_id") //
						.type(VARCHAR) //
						.size(255) //
						.notNull() //
						.refKey(tutuTable, "id") //
						.build(), //
				column("age") //
						.type(INTEGER) //
						.build()));

		assertEquals("CREATE TABLE xxxtata" //
				+ " (toto_id VARCHAR(255) NOT NULL," //
				+ " tutu_id VARCHAR(255) NOT NULL," //
				+ " age INTEGER NULL," //
				+ " PRIMARY KEY (toto_id)," //
				+ " FOREIGN KEY (toto_id) REFERENCES xxxtoto (id)," //
				+ " FOREIGN KEY (tutu_id) REFERENCES xxxtutu (id))", sqls[0]);

		assertEquals(1, sqls.length);
	}

	@Test
	public void testGetSQLCreateCommands_twoColumns_biFK() throws Exception {

		final DbTable totoTable = forgeDbTable("TOTO", //
				column("first_name") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.build(), //
				column("last_name") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.build());

		final String[] sqls = composeSQLCreateCommands("xxx", forgeDbTable("TATA", //
				column("first_name") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.refKey(totoTable, "first_name") //
						.build(), //
				column("last_name") //
						.type(VARCHAR) //
						.size(255) //
						.primaryKey() //
						.notNull() //
						.refKey(totoTable, "last_name") //
						.build(), //
				column("age") //
						.type(INTEGER) //
						.notNull() //
						.build()));

		assertEquals("CREATE TABLE xxxtata" //
				+ " (first_name VARCHAR(255) NOT NULL," //
				+ " last_name VARCHAR(255) NOT NULL," //
				+ " age INTEGER NOT NULL," //
				+ " PRIMARY KEY (first_name, last_name)," //
				+ " FOREIGN KEY (first_name, last_name) REFERENCES xxxtoto (first_name, last_name))", sqls[0]);

		assertEquals(1, sqls.length);
	}

	private static DbTable forgeDbTable(final String tableName, final Column... columns) {

		return new DbTable() {

			@Override
			public String name() {

				return tableName;
			}

			@Override
			public Column[] columns() {

				return columns;
			}
		};
	}

	@Test
	public void testNonAsciiColumnName() {

		assertThrows(IllegalArgumentException.class, ()

		-> column("très_bien"));
	}

	@Test
	public void testNonLowercaseColumnName() {

		assertThrows(IllegalArgumentException.class, ()

		-> column("serviceId"));
	}
}
