package net.avcompris.commons3.notifier.utils;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.joda.time.DateTimeZone.UTC;

import java.io.UnsupportedEncodingException;
// import static org.joda.time.format.DateTimeFormatter;
//import static java.time.format.DateTimeFormatter.

import javax.annotation.Nullable;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.avcompris.commons3.notifier.api.ErrorNotification;
import net.avcompris.commons3.notifier.api.Notification;
import net.avcompris.commons3.notifier.api.Notification.ActionType;

public abstract class NotificationUtils {

	private static final DateTimeFormatter ISO_FORMATTER = ISODateTimeFormat.dateTime();

	public static byte[] serialize(final Notification notification) {

		final String json = serializeToJSON(notification);

		try {

			return json.getBytes(UTF_8);

		} catch (final UnsupportedEncodingException e) {

			throw new RuntimeException(e);
		}
	}

	private static String serializeToJSON(final Notification notification) {

		checkNotNull(notification, "notification");

		final ActionType actionType = notification.getActionType();
		final String username = notification.getUsername();

		final StringBuilder sb = new StringBuilder("{");

		sb.append("\"actionType\": ");

		sb.append("\"").append(actionType.name()).append("\"");

		sb.append(", \"username\": ");

		if (username == null) {

			sb.append("null");

		} else {

			sb.append("\"").append(username).append("\"");
		}

		return sb.append("}").toString();
	}

	public static Notification deserialize( //
			final Class<? extends ActionType> actionTypeClass, //
			final byte[] bytes) {

		checkNotNull(bytes, "bytes");
		checkNotNull(actionTypeClass, "actionTypeClass");

		final String s;

		try {

			s = new String(bytes, UTF_8);

		} catch (final UnsupportedEncodingException e) {

			throw new RuntimeException(e);
		}

		final JSONObject json;

		try {

			json = (JSONObject) new JSONParser().parse(s);

		} catch (final ParseException e) {

			throw new RuntimeException("JSON: " + s, e);
		}

		final Object actionTypeAsString = json.get("actionType");
		final Object username = json.get("username");

		if (actionTypeAsString == null) {
			throw new RuntimeException("actionType should not be null in JSON: " + s);
		}

		final ActionType actionType = extractActionType(actionTypeClass, actionTypeAsString.toString());

		final Notification notification = instantiate(Notification.class) //
				.setActionType(actionType);

		if (username != null) {

			notification.setUsername(username.toString());
		}

		return notification;
	}

	private static ActionType extractActionType(final Class<? extends ActionType> actionTypeClass, final String name) {

		checkNotNull(actionTypeClass, "actionTypeClass");
		checkNotNull(name, "name");

		checkArgument(actionTypeClass.isEnum(), //
				"actionTypeClass should be Enum: %s", actionTypeClass.getName());

		for (final ActionType actionType : actionTypeClass.getEnumConstants()) {

			if (name.contentEquals(actionType.name())) {

				return actionType;
			}
		}

		throw new RuntimeException("actionType in JSON cannot be parsed: " + name);
	}

	public static byte[] serialize(final Throwable throwable) {

		final String json = serializeToJSON(throwable);

		try {

			return json.getBytes(UTF_8);

		} catch (final UnsupportedEncodingException e) {

			throw new RuntimeException(e);
		}
	}

	private static String escape(final String s) {

		checkNotNull(s, "s");

		return s.replace("\\", "\\\\").replace("\"", "\\\"");
	}

	private static String serializeToJSON(final Throwable throwable) {

		checkNotNull(throwable, "throwable");

		final DateTime dateTime = new DateTime().withZone(UTC);

		final Class<? extends Throwable> clazz = throwable.getClass();

		@Nullable
		final String message = throwable.getMessage();

		@Nullable
		final Throwable cause = throwable.getCause();

		final StringBuilder sb = new StringBuilder("{");

		sb.append("\"dateTime\": ");

		sb.append("\"").append(dateTime.toString(ISO_FORMATTER)).append("\"");

		sb.append(", \"className\": ");

		sb.append("\"").append(clazz.getName()).append("\"");

		sb.append(", \"message\": ");

		appendQuotedStringOrNull(sb, message);

		sb.append(", \"cause\": ");

		if (cause == null) {

			sb.append("null");

		} else {

			sb.append(serializeToJSON(cause));
		}

		sb.append(", \"stackTrace\": [");

		boolean start = true;

		for (final StackTraceElement ste : throwable.getStackTrace()) {

			if (!start) {
				sb.append(", ");
			} else {
				start = false;
			}

			sb.append("{");

			sb.append("\"className\": ");

			appendQuotedStringOrNull(sb, ste.getClassName());

			sb.append(", \"methodName\": ");

			appendQuotedStringOrNull(sb, ste.getMethodName());

			sb.append(", \"fileName\": ");

			appendQuotedStringOrNull(sb, ste.getFileName());

			sb.append(", \"lineNumber\": ");

			sb.append(ste.getLineNumber());

			sb.append("}");
		}

		sb.append("]");

		return sb.append("}").toString();
	}

	private static void appendQuotedStringOrNull(final StringBuilder sb, @Nullable final String s) {

		if (s == null) {

			sb.append("null");

		} else {

			sb.append("\"").append(escape(s)).append("\"");
		}
	}

	public static ErrorNotification deserializeError(final byte[] bytes) {

		checkNotNull(bytes, "bytes");

		final String s;

		try {

			s = new String(bytes, UTF_8);

		} catch (final UnsupportedEncodingException e) {

			throw new RuntimeException(e);
		}

		final JSONObject json;

		try {

			json = (JSONObject) new JSONParser().parse(s);

		} catch (final ParseException e) {

			throw new RuntimeException("JSON: " + s, e);
		}

		return deserializeError(json);
	}

	private static ErrorNotification deserializeError(final JSONObject json) {

		final DateTime dateTime = ISO_FORMATTER.parseDateTime(json.get("dateTime").toString());

		final String className = json.get("className").toString();

		@Nullable
		final Object cause = json.get("cause");

		final JSONArray stackTrace = (JSONArray) json.get("stackTrace");

		final ErrorNotification notification = instantiate(ErrorNotification.class) //
				.setDateTime(dateTime) //
				.setClassName(className) //
				.setMessage(toStringOrNull(json.get("message")));

		if (cause != null) {

			notification.setCause(deserializeError((JSONObject) cause));
		}

		for (final Object item : stackTrace) {

			final JSONObject ste = (JSONObject) item;

			notification.addToStackTrace(instantiate(ErrorNotification.StackTraceElement.class) //
					.setClassName(toStringOrNull(ste.get("className"))) //
					.setMethodName(toStringOrNull(ste.get("methodName"))) //
					.setFileName(toStringOrNull(ste.get("fileName"))) //
					.setLineNumber((int) (long) ste.get("lineNumber")));
		}

		return notification;
	}

	@Nullable
	private static String toStringOrNull(@Nullable final Object o) {

		return o == null ? null : o.toString();
	}

	public static ErrorNotification toErrorNotification(final Throwable throwable) {

		checkNotNull(throwable, "throwable");

		final DateTime dateTime = new DateTime().withZone(UTC);

		final String className = throwable.getClass().getName();

		@Nullable
		final String message = throwable.getMessage();

		@Nullable
		final Throwable cause = throwable.getCause();

		final ErrorNotification notification = instantiate(ErrorNotification.class) //
				.setDateTime(dateTime) //
				.setClassName(className) //
				.setMessage(message);

		if (cause != null) {

			notification.setCause(toErrorNotification(cause));
		}

		for (final StackTraceElement ste : throwable.getStackTrace()) {

			notification.addToStackTrace(instantiate(ErrorNotification.StackTraceElement.class) //
					.setClassName(ste.getClassName()) //
					.setMethodName(ste.getMethodName()) //
					.setFileName(ste.getFileName()) //
					.setLineNumber(ste.getLineNumber()));
		}

		return notification;
	}
}
