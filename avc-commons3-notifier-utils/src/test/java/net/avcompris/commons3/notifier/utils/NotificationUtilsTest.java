package net.avcompris.commons3.notifier.utils;

import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.commons3.notifier.utils.DummyActionType.TRACE_BOARDED;
import static net.avcompris.commons3.notifier.utils.DummyActionType.TRACE_POSTED;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.notifier.api.ErrorNotification;
import net.avcompris.commons3.notifier.api.Notification;

public class NotificationUtilsTest {

	@Test
	public void testSerialize_deserialize() throws Exception {

		final byte[] bytes = NotificationUtils.serialize(instantiate(Notification.class) //
				.setActionType(TRACE_BOARDED) //
				.setUsername("blah"));

		assertEquals("{" //
				+ "\"actionType\": \"TRACE_BOARDED\", " //
				+ "\"username\": \"blah\"}",

				new String(bytes, UTF_8));

		final Notification notification = NotificationUtils.deserialize(DummyActionType.class, bytes);

		assertSame(TRACE_BOARDED, notification.getActionType());
		assertEquals("blah", notification.getUsername());
	}

	@Test
	public void testSerialize_deserialize_nullValues() throws Exception {

		final byte[] bytes = NotificationUtils.serialize(instantiate(Notification.class) //
				.setActionType(TRACE_POSTED));

		assertEquals("{" //
				+ "\"actionType\": \"TRACE_POSTED\", " //
				+ "\"username\": null}",

				new String(bytes, UTF_8));

		final Notification notification = NotificationUtils.deserialize(DummyActionType.class, bytes);

		assertSame(TRACE_POSTED, notification.getActionType());
		assertEquals(null, notification.getUsername());
	}

	@Test
	public void testSerialize_deserialize_Throwable() throws Exception {

		final DateTime now = new DateTime();

		final Throwable throwable = new RuntimeException("Please \" see that \\ quote.");

		final byte[] bytes = NotificationUtils.serialize(throwable);

		assertTrue(new String(bytes, UTF_8).startsWith("{" //
				+ "\"dateTime\": \""));

		assertTrue(new String(bytes, UTF_8).contains(""//
				+ "\"className\": \"java.lang.RuntimeException\", " //
				+ "\"message\": \"Please \\\" see that \\\\ quote.\", " //
				+ "\"cause\": null, " //
				+ "\"stackTrace\": ["));

		assertTrue(new String(bytes, UTF_8).endsWith("]}"));

		final ErrorNotification notification = NotificationUtils.deserializeError(bytes);

		assertFalse(now.isAfter(notification.getDateTime()));
		assertEquals("java.lang.RuntimeException", notification.getClassName());
		assertEquals("Please \" see that \\ quote.", notification.getMessage());
		assertEquals(null, notification.getCause());
		assertEquals(NotificationUtilsTest.class.getName(), notification.getStackTrace()[0].getClassName());
		assertEquals("testSerialize_deserialize_Throwable", notification.getStackTrace()[0].getMethodName());
		assertEquals("NotificationUtilsTest.java", notification.getStackTrace()[0].getFileName());
		assertTrue(notification.getStackTrace()[0].getLineNumber() > 0);
	}

	@Test
	public void testDeserializeError() throws Exception {

		final ErrorNotification error = NotificationUtils.deserializeError(("{" //
				+ "\"dateTime\": \"2019-09-15T13:44:33.000Z\", " //
				+ "\"className\": \"toto\", " //
				+ "\"message\": \"momo\", " //
				+ "\"stackTrace\": [" //
				+ "{\"className\": \"a\", \"methodName\": \"b\", \"fileName\": \"c\", \"lineNumber\": 4}, " //
				+ "{\"className\": \"d\", \"methodName\": \"e\", \"lineNumber\": 5}" //
				+ "]}" //
		).getBytes(UTF_8));

		assertEquals("toto", error.getClassName());
		assertEquals("momo", error.getMessage());
		assertEquals(null, error.getCause());

		assertEquals("a", error.getStackTrace()[0].getClassName());
		assertEquals("b", error.getStackTrace()[0].getMethodName());
		assertEquals("c", error.getStackTrace()[0].getFileName());
		assertEquals(4, error.getStackTrace()[0].getLineNumber());

		assertEquals("d", error.getStackTrace()[1].getClassName());
		assertEquals("e", error.getStackTrace()[1].getMethodName());
		assertEquals(null, error.getStackTrace()[1].getFileName());
		assertEquals(5, error.getStackTrace()[1].getLineNumber());
	}
}
