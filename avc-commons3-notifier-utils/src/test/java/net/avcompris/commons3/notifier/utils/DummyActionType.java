package net.avcompris.commons3.notifier.utils;

import net.avcompris.commons3.notifier.api.Notification.ActionType;

enum DummyActionType implements ActionType {

	TRACE_POSTED, TRACE_VALIDATED, TRACE_BOARDED, TRACE_MINED,
}
